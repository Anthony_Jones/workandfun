/*
* Copyright © 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#include <Strut's Resource/DeNode.h>
#include <cstddef>
#include <cmath>
#include <stdexcept>

#pragma once
#ifndef CPP_OOP_ARRAYLIST_H
#define CPP_OOP_ARRAYLIST_H

namespace cw {
    template<class T>
    class ArrayList {
    private:
        DeNode<T> *head;
        DeNode<T> *tail;
        DeNode<T> *current;

        size_t Size = 0;
        size_t CurrentPos = 1;

        DeNode<T> *at(const ptrdiff_t &elementPos) const;

    public:
        ArrayList();

        ArrayList(const ArrayList<T> &other);

        ~ArrayList();

        void add(const T &item);

        void clear();

        void remove(const size_t &elementPos);

        const bool operator==(const ArrayList<T> &other) const;

        const bool operator!=(const ArrayList<T> &other) const;

        const size_t size() const;

        T &operator[](const size_t &elementPos);


        void insert(const size_t &position, const T &item);


        class iterator {
            typedef iterator self_type;
            typedef T value_type;
            typedef DeNode<T> *pointer;

            pointer ptr_;
        public:
//            typedef iterator self_type;
//            typedef DeNode<T> value_type;
//            typedef DeNode<T>* pointer;


            explicit iterator(pointer ptr) : ptr_(ptr) {

            }

            self_type &operator++() {
                ptr_ = ptr_->next;
                return *this;
            }

            self_type &operator--() {
                ptr_ = ptr_->previous;
                return *this;
            }

            iterator &operator=(const self_type &other) {
                ptr_ = other.ptr_;
                return *this;
            }

            const self_type operator++(int) {
                self_type i = *this;
                ptr_ = ptr_->next;
                return i;
            }

            const self_type operator--(int) {
                self_type i = *this;
                ptr_ = ptr_->previous;
                return i;
            }

            value_type &operator*() const { return ptr_->item; }

            pointer operator->() const { return ptr_; }

            const bool operator==(const self_type &rhs) const { return ptr_ == rhs.ptr_; }

            const bool operator!=(const self_type &rhs) const { return ptr_ != rhs.ptr_; }
        };

        iterator begin() {
            return iterator(head);
        }

        iterator end() {
            return iterator(tail);
        }


    };

    template<class T>
    ArrayList<T>::ArrayList() {
        Size = 0;
        CurrentPos = 1;
        head = nullptr;
        tail = nullptr;
        current = nullptr;
    }

    template<class T>
    ArrayList<T>::ArrayList(const ArrayList<T> &other) {
        CurrentPos = other.CurrentPos;
        current = other.current;
        Size = 0;
        DeNode<T> *temp = other.head;
        for (size_t i = 1; i <= other.Size; ++i) {
            add(temp->get());
            temp = temp->getNext();
        }
    }

    template<class T>
    ArrayList<T>::~ArrayList() { // TODO Сделать правильное удаление
        if (Size == 0) return;
        while (head->getNext() != head) {
            auto p = head->getNext();
            p->getNext()->setPrevious(head);
            head->setNext(p->getNext());
            delete p;
        }
        delete head;
    }

    template<class T>
    void ArrayList<T>::add(const T &item) {
        auto *data = new DeNode<T>(item);
        data->set(item);
        if (Size == 0) {
            head = data;
            head->setNext(head);
            head->setPrevious(head);
            tail = head;
            current = head;
            CurrentPos = 1;
        } else {
            tail->setNext(data);
            data->setPrevious(tail);
            head->setPrevious(data);
            data->setNext(head);
            tail = data;
        }
        ++Size;
    }

    template<class T>
    void ArrayList<T>::clear() { // TODO Сделать правильное удаление
        if (Size != 0) {
            while (head->getNext() != head) {
                auto TempNode = head->getNext();
                TempNode->getNext()->setPrevious(head);
                head->setNext(TempNode->getNext());
                delete TempNode;
            }
            delete head;
            head = nullptr;
//            head->setNext(head);
            current = nullptr;
            tail = nullptr;
            Size = 0;
            CurrentPos = 1;
        }
    }

    template<class T>
    void ArrayList<T>::remove(const size_t &elementPos) {
        DeNode<T> *removable;
        if (elementPos > Size) {
            throw std::out_of_range("Index is out of bound");
        } else if (Size == 0) {
            throw std::logic_error("There is no items to remove in empty lists");
        }
        removable = at(elementPos);
        if (Size != 1) {
            current = removable->getNext();
            CurrentPos = elementPos;
            removable->getNext()->setPrevious(removable->getPrevious());
            removable->getPrevious()->setNext(removable->getNext());
            if (elementPos == 1) {
                head = removable->getNext();
            } else if (elementPos == Size) {
                tail = removable->getPrevious();
            }
        }
        --Size;
        delete removable;
    }

    template<class T>
    const size_t ArrayList<T>::size() const {
        return Size;
    }

    template<class T>
    T &ArrayList<T>::operator[](const size_t &elementPos) {
        if (elementPos > Size) {
            throw std::out_of_range("Index is out of bound");
        } else if (Size == 0) {
            throw std::logic_error("Empty lists does not have items");
        }
        current = at(elementPos);
        CurrentPos = elementPos;
        return current->item;
    }

    template<class T>
    const bool ArrayList<T>::operator==(const ArrayList<T> &other) const {
        if (Size == other.Size) {
            auto tempNode = head;
            auto tempNodeOther = other.head;
            for (size_t i = 0; i < Size; ++i) {
                if (tempNode->item != tempNodeOther->item) {
                    return false;
                }
                tempNode = tempNode->getNext();
                tempNodeOther = tempNodeOther->getNext();
            }
            return true;
        } else {
            return false;
        }
    }

    template<class T>
    const bool ArrayList<T>::operator!=(const ArrayList<T> &other) const {
        if (Size != other.Size) {
            auto tempNode = head;
            auto tempNodeOther = other.head;
            for (size_t i = 0; i < Size; ++i) {
                if (tempNode->item == tempNodeOther->item) {
                    return false;
                }
                tempNode = tempNode->getNext();
                tempNodeOther = tempNodeOther->getNext();
            }
            return true;
        } else {
            return false;
        }
    }

    template<class T>
    void ArrayList<T>::insert(const size_t &position, const T &item) {
        auto newItem = new DeNode<T>(item);
        if (position > Size + 1) {
            throw std::out_of_range("Index is out of bound");
        }
        if (Size != 0) {
            DeNode<T> *temp;
            temp = at(position);
            temp->getPrevious()->setNext(newItem);
            newItem->setPrevious(temp->getPrevious());
            newItem->setNext(temp);
            temp->setPrevious(newItem);
            current = newItem;
            CurrentPos = position;
            if (position == 1) {
                head = current;
                tail->setNext(head);
            } else if (position == Size) {
                tail = current;
            }
            ++Size;
        } else if (Size == 0 or position == Size + 1) {
            add(item);
        }
    }

    template<class T>
    DeNode<T> *ArrayList<T>::at(const ptrdiff_t &elementPos) const {
        DeNode<T> *currNode;
            auto distance =
                    static_cast<long long>(elementPos - 1) -
                    static_cast<long long>(CurrentPos - 1);// Знак будет ли сохраняться
            if (abs(static_cast<int>(distance)) < Size / 2) {
                if (distance >= 0) { // Current правее чем удаляемый
                    currNode = current;
                    for (ptrdiff_t i = CurrentPos - 1; i < elementPos - 1; ++i) {
                        currNode = currNode->getNext();
                    }
                } else if (distance < 0) { //Current левее чем удаляемый
                    currNode = current;
                    for (ptrdiff_t i = CurrentPos - 1; i > elementPos - 1; --i) {
                        currNode = currNode->getPrevious();
                    }
                }
            } else if (elementPos - 1 >= static_cast<ptrdiff_t >(Size / 2)) {
                currNode = tail;
                for (ptrdiff_t i = Size - 1; i > elementPos - 1; --i) {
                    currNode = currNode->getPrevious();
                }
            } else {
                currNode = head;
                for (ptrdiff_t i = 0; i < elementPos - 1; ++i) {
                    currNode = currNode->getNext();
                }
            }
            return currNode;
    }


}

#endif //CPP_OOP_ARRAYLIST_H
