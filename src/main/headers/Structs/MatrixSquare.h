/*
* Copyright © 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef CPP_OOP_MATRIXSQUARE_H
#define CPP_OOP_MATRIXSQUARE_H

#include "Structs/MatrixRectangle.h"

namespace cw {

    template<class T = double>
    class MatrixSquare : public MatrixRectangle<T> {
    public:
        MatrixSquare();

        explicit MatrixSquare(size_t Rows);

        MatrixSquare(size_t Rows, T number);

        MatrixSquare(const MatrixSquare<T> &other) = default;

        explicit MatrixSquare(const MatrixRectangle<T> &other);

        virtual ~MatrixSquare() = default;

        const MatrixSquare<T> inverse() const;


        const T determinant() const;

        MatrixSquare<T> &operator=(const MatrixSquare<T> &other);

        const MatrixSquare<T> operator/(const MatrixSquare<T> &other) const;

        const MatrixSquare<T> &operator/=(const MatrixSquare<T> &other);

        const T norm() const;

    };


    /**
 * @throw std::domain_error when matrix is degenerated(Rows = 0)
 * @throw std::domain_error When zero at main diagonal
 * @tparam T
 * @return
 */
    template<class T>
    const T MatrixSquare<T>::determinant() const {
        if (this->Rows == 0) {
            throw std::domain_error("Degenerated matrix has no determinant");
        }
        auto **result = reinterpret_cast<T **>(new void *[this->Rows + this->Rows * this->Rows]);
        result[0] = reinterpret_cast<T *>(result + this->Rows);
        memcpy(result[0], this->Array[0], sizeof(T) * this->Rows * this->Rows);
        for (size_t i = 1; i < this->Rows; ++i) {
            *(result + i) = *this->Array + this->Rows * i;
        }
        if (this->Rows == 1) {
            return result[0][0];
        }
        T res = 1;
        for (size_t k = 0; k < this->Rows; k++) {
            size_t count = 0;
            bool flag = false;
            do {
                if (result[k][k] == T()) {
                    if (count == this->Rows - 1) return T();
                    this->swapLine(result, k, count + 1);
                    count++;
                    res *= -1;
                    flag = true;
                } else flag = false;
            } while (flag);
            for (size_t i = k + 1; i < this->Rows; i++) {
                T b = result[i][k] / result[k][k];
                for (size_t j = k; j < this->Rows; j++) {
                    result[i][j] -= result[k][j] * b;
                    if (result[k][k] == T()) {
                        throw std::domain_error("Zero at main diagonal");
                    }
                }
            }
        }
        for (size_t i = 0; i < this->Rows; i++) {
            res *= result[i][i];
        }
        delete[] result;
        return res;
    }

    /**
 *
 * @throw std::domain_error when matrix is degenerated(Rows = 0)
 * @throw std::domain_error when matrix has zero Determinant
 * @tparam T  Algebraic Type
 * @return MatrixAlgebraic<T> Inverse matrix
 */
    template<class T>
    const MatrixSquare<T> MatrixSquare<T>::inverse() const {
        T detT;
        try {
            detT = this->determinant();
        } catch (std::domain_error &e) {
            string ErrorMessage(e.what());
            ErrorMessage += "\n called by inverse()\n";
            throw std::domain_error(ErrorMessage.c_str());
        }
        if (detT == T()) {
            throw std::domain_error("Determinant is Zero. Has no inverse.");
        }

        if (this->Rows == 1) {
            return MatrixSquare<T>(1, this->Array[0][0]);
        } else {

            MatrixSquare<T> inverse(this->Rows, this->Rows);
            for (size_t i = 0; i < this->Rows; ++i) {
                for (size_t j = 0; j < this->Rows; ++j) {
                    MatrixSquare<T> temp;
                    T temp1;
                    try {
                        temp = this->getMinor(i, j);
                        temp1 = temp.determinant();
                    } catch (std::domain_error &e) {
                        string ErrorMessage(e.what());
                        ErrorMessage += "\n called by inverse()\n";
                        throw std::domain_error(ErrorMessage.c_str());
                    }
                    inverse.Array[i][j] = temp1 * (((i + j) % 2 == 0) ? 1 : -1);
                }
            }
            return inverse.transpose() / this->determinant();
        }
    }

    /**
     * @throw std::bad_alloc
     * @tparam T
     * @param other
     * @return
     */
    template<class T>
    MatrixSquare<T> &MatrixSquare<T>::operator=(const MatrixSquare<T> &other) {
        if (*this == other) {
            return *this;
        } else if (this->Rows == other.Rows) {
            memcpy(this->Array[0], other.Array[0], sizeof(T) * this->Rows * this->Rows);
            for (size_t i = 1; i < this->Rows; ++i) {
                *(this->Array + i) = *(this->Array) + this->Rows * i;
            }
        } else {
            delete[] this->Array;
            this->Rows = other.Rows;
            this->Columns = other.Rows;
            try {
                this->Array = this->createArray2d(this->Rows, this->Rows);
            } catch (std::bad_alloc &e) {
                throw e;
            }
            memcpy(this->Array[0], other.Array[0], sizeof(T) * this->Rows * this->Rows);
            for (size_t i = 1; i < this->Rows; ++i) {
                *(this->Array + i) = *(this->Array) + this->Rows * i;
            }
        }
        return *this;
    }


    /**
    *
    * @throw std::domain_error when one of the matrices is degenerated(Rows = 0)
    * @throw std::domain_error when Right matrix has zero Determinant
    * @throw std::domain_error when Columns of left Matrix does not equal Rows of right Matrix
    * @tparam T  Algebraic Type
    * @return result of division
    */
    template<class T>
    const MatrixSquare<T> MatrixSquare<T>::operator/(const MatrixSquare<T> &other) const {
        MatrixSquare<T> inverseRight(other.Rows, other.Rows);
        MatrixSquare<T> result;
        try {
            inverseRight = other.inverse();
            result = *this * inverseRight;
        } catch (std::domain_error &e) {
            string ErrorMessage(e.what());
            ErrorMessage += "\n called by operator /\n";
            throw std::domain_error(ErrorMessage.c_str());
        }
        return result;
    }

    /**
    *
    * @throw std::domain_error when matrix is degenerated(Rows = 0)
    * @throw std::domain_error when matrix has zero Determinant
    * @throw std::domain_error when Columns of left Matrix does not equal Rows of right Matrix
    * @tparam T  Algebraic Type
    * @return result of division
    */
    template<class T>
    const MatrixSquare<T> &MatrixSquare<T>::operator/=(const MatrixSquare<T> &other) {
        MatrixSquare<T> inverseRight(other.Rows, other.Rows);
        try {
            inverseRight = other.inverse();
            *this *= inverseRight;
        } catch (std::domain_error &e) {
            string ErrorMessage(e.what());
            ErrorMessage += "\n called by operator /=\n";
            throw std::domain_error(ErrorMessage.c_str());
        }
        return *this;
    }

    template<class T>
    MatrixSquare<T>::MatrixSquare(size_t Rows) : MatrixRectangle<T>(Rows, Rows) {}

    template<class T>
    MatrixSquare<T>::MatrixSquare(size_t Rows, T number) : MatrixRectangle<T>(Rows, Rows, number) {}

//    template<class T>
//    MatrixSquare<T>::MatrixSquare(const MatrixSquare<T> &other) : MatrixRectangle<T>(other) {}

    template<class T>
    MatrixSquare<T>::MatrixSquare() : MatrixRectangle<T>() {}

    template<class T>
    MatrixSquare<T>::MatrixSquare(const MatrixRectangle<T> &other) {
        if (other.getRows() != other.getColumns()) {
            throw std::domain_error("Can not convert Rectangle matrix to Square matrix");
        } else {
            this->Rows = other.getRows();
            this->Columns = other.getColumns();
            this->Array = this->createArray2d(this->Rows, this->Rows);
            for (size_t i = 1; i < this->Rows; ++i) {
                *(this->Array + i) = *(this->Array) + this->Rows * i;
            }

            for (size_t j = 0; j < this->Rows; ++j) {
                for (size_t i = 0; i < this->Columns; ++i) {
                    this->Array[j][i] = other(j, i);
                }
            }
        }
    }

    template<class T>
    const T MatrixSquare<T>::norm() const {
        T result = 0;
        for (size_t i = 0; i < this->Rows; ++i) {
            for (size_t j = 0; j < this->Columns; ++j) {
                result += this->Array[i][j] * this->Array[i][j];
            }
        }
        return sqrt(result);
    }


}


#endif //CPP_OOP_MATRIXSQUARE_H
