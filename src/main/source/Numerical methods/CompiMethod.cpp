/*
* Copyright © 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once
#ifndef COMBIMETHOD_CPP
#define COMBIMETHOD_CPP

#include <stdexcept>
#include <cmath>

template<class T>
const T CombinationMethod(const T &a, const T &b, const T (*func)(const T &), const T (*firstDiv)(const T &),
                          const T (*secondDiv)(const T &), const T &eps) {
    if (func(a) * func(b) > 0) {
        throw std::domain_error("There is no root");
    } else {
        T x0 = a;
        T _x0 = b;
        if (secondDiv(a) * func(a) > 0) {
            while (fabs(_x0 - x0) > eps) {
                _x0 = _x0 + (x0 - _x0) / (1 - func(x0) / func(_x0));
                x0 = x0 - func(x0) / firstDiv(x0);
            }
            return (x0 + _x0) / 2.0;
        } else if (secondDiv(b) * func(b) > 0) {
            while (fabs(_x0 - x0) > eps) {
                x0 = x0 + (_x0 - x0) / (1 - func(_x0) / func(x0));
                _x0 = _x0 - func(_x0) / firstDiv(_x0);
            }
            return (x0 + _x0) / 2.0;
        } else {
            throw std::domain_error("Function is not monotonous on this segment");
        }
    }
}

#endif //COMBIMETHOD_CPP