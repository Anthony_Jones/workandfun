#include <gtest/gtest.h>
#include <Structs/ArrayList.h>

using namespace cw;
// TODO Test All ArrayList

TEST(ArrayList, operatorEqualNonEqual) {
    ArrayList<double> Test1, Test2;
    ASSERT_TRUE(Test1 == Test2);
    ASSERT_FALSE(Test1 != Test2);
    for (size_t i = 1; i <= 20; ++i) {
        Test1.add(i);
        Test2.add(i);
    }
    ASSERT_TRUE(Test1 == Test2);
    ASSERT_FALSE(Test1 != Test2);
    Test1.remove(1);
    ASSERT_FALSE(Test1 == Test2);
    ASSERT_TRUE(Test1 != Test2);
}

TEST(ArrayList, Constructors) {
    ArrayList<double> Test1;
    for (size_t i = 1; i <= 20; ++i) {
        Test1.add(i);
    }
    ArrayList<double> Test2(Test1);
    ASSERT_EQ(Test1, Test2);
}

TEST(ArratList, operatorSquare) {
    ArrayList<double> TestList;
    ASSERT_THROW(TestList[0], std::logic_error);
    TestList.add(1);
    ASSERT_THROW(TestList[2], std::out_of_range);
    ASSERT_EQ(TestList[1], 1);
}

TEST(ArrayList, add) {
    ArrayList<double> Test1;
    Test1.add(10);
    ASSERT_EQ(Test1[1], 10);
    Test1.add(1);
    ASSERT_EQ(Test1[2], 1);
}

TEST(ArrayList, clear) {
    ArrayList<double> Test1;
    for (size_t i = 1; i <= 20; ++i) {
        Test1.add(i);
    }
    Test1.clear();
    ArrayList<double> emptyList;
    ASSERT_EQ(Test1, emptyList);
}

TEST(ArrayList, remove) {
    ArrayList<double> Test1;
    Test1.add(1);
    Test1.remove(1);
    ArrayList<double> emptyList;
    ASSERT_EQ(Test1, emptyList);

    ASSERT_THROW(Test1.remove(0), ::std::logic_error);
    Test1.add(1);
    ASSERT_THROW(Test1.remove(2), ::std::out_of_range);
}

TEST(ArrayList, insert) {
    ArrayList<double> Test1;
    Test1.insert(1, 1);
    ASSERT_EQ(1, 1);
    Test1.insert(1, 2);
    ASSERT_EQ(Test1[1], 2);
    Test1.insert(3, 3);
    ASSERT_EQ(Test1[3], 3);
    ASSERT_THROW(Test1[4], std::out_of_range);

}