/*
* Copyright © 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#include "../../../lib/benchmark/include/benchmark/benchmark.h"

#include <Structs/Array.hxx>
#include <../source/Sorts/Sorts.cpp>
#include <../utils/ArrayUtils.cpp>

using namespace cw;

static void BM_SomeFunction(benchmark::State &state) {
    Array<int> A;
    ptrdiff_t n = 10000;
    for (ptrdiff_t i = 1; i < 50; ++i) {
        A = Array<int>(n * i);
        for (ptrdiff_t j = 0; j < n * i; ++j) {
            A[j] = j;
        }
        utils::shuffle(A, n * i, random::lcg::rand);
        for (auto _ : state) {
            // This code gets timed
            QuickSort(A, static_cast<ptrdiff_t >(0), n * i - 1, true);
        }
    }
}
// Register the function as a benchmark
BENCHMARK(BM_SomeFunction);
// Run the benchmark
BENCHMARK_MAIN();
