/*
* Copyright © 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef ADAMS_METHOD_CPP
#define ADAMS_METHOD_CPP

#include <cmath>
#include <iostream>

#include "RungeCutta.cpp"
#include "../../utils/Binpow.cpp"

namespace cw {
    template<class T>
    const T
    AdamsMethod(const T &initialX, const T &initialY, const T &endOfSegment,
                const T(*rightPart)(const T &, const T &)) {
        size_t tempStep = 4;
        T *solutions, *tempArray;
        T step = (endOfSegment - initialX) / static_cast<T>(4);
        do {
            solutions = RungeCutta(initialX, initialY, endOfSegment - step, static_cast<size_t>(4), tempStep,
                                   rightPart);
            tempArray = RungeCutta(initialX, initialY, endOfSegment - step, static_cast<size_t>(4), tempStep * 2,
                                   rightPart);
            tempStep *= 2;
        } while (fabs(tempArray[3] - solutions[3]) >= 10e-7);
        for (size_t j = 0; j < 4; ++j) {
            cout << solutions[j] << " ";
        }
        T extrapolInc = 0, interpolInc = 1;
        T temp = 24;
        ptrdiff_t iterations = 1;
        size_t l = 2;
        step *= 2;
        while (fabs(interpolInc - extrapolInc) >= 10e-3) {
            step /= static_cast<T>(2);
            for (size_t i = 0; i < 4; ++i) {
                tempArray[i] = solutions[i];
            }
            for (ptrdiff_t j = 0;; ++j) {
                extrapolInc = step *
                              (55 * rightPart(initialX + (j + 3) * step, tempArray[3])
                               - 59 * rightPart(initialX + (j + 2) * step, tempArray[2])
                               + 37 * rightPart(initialX + (j + 1) * step, tempArray[1])
                               - 9 * rightPart(initialX + j * step, tempArray[0]))
                              / temp;
                for (size_t i = 0; i < 3; ++i) {
                    tempArray[i] = tempArray[i + 1];
                }
                tempArray[3] = tempArray[2] + extrapolInc;
                if (j > iterations - 5) break;
            }
            interpolInc = step *
                          (9 * rightPart(endOfSegment, tempArray[3])
                           + 19 * rightPart(endOfSegment - step, tempArray[2])
                           - 5 * rightPart(endOfSegment - 2 * step, tempArray[1])
                           + rightPart(endOfSegment - 3 * step, tempArray[0]))
                          / temp;
            ++l;
            iterations = utils::binpow(static_cast<size_t >(2), l);
        }
        tempArray[3] = tempArray[2] + interpolInc;
        interpolInc = step *
                      (9 * rightPart(endOfSegment, tempArray[3])
                       + 19 * rightPart(endOfSegment - step, tempArray[2])
                       - 5 * rightPart(endOfSegment - 2 * step, tempArray[1])
                       + rightPart(endOfSegment - 3 * step, tempArray[0]))
                      / temp;
        temp = tempArray[2] + interpolInc;
        delete[] solutions, delete[] tempArray;
        return temp;
    }
}
#endif //ADAMS_METHOD_CPP