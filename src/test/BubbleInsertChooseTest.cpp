#include <gtest/gtest.h>
#include "../main/source/Structs/DoubleArray.h"
#include "../main/source/ASD/Sorts/Sorts.cpp"


TEST(SortingAlgorithms, InsertionSort) {
    DoubleArray Array;
    Array.RandArray(10);
    auto Temp1 = Array;
    BubbleSort(Array);
    InsertionSort(Temp1);
    ASSERT_EQ(Array, Temp1);
}

TEST(SortingAlgorithms, ChooseElementSort) {
    DoubleArray Array;
    Array.RandArray(10);
    auto Temp1 = Array;
    BubbleSort(Array);
    ChooseElementSort(Temp1);
    ASSERT_EQ(Array, Temp1);
}