/*
* Copyright © 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef _COMPLEX_CPP
#define _COMPLEX_CPP

#include <cmath>
#include <iostream>
//#include "Complex.h"
using namespace std;


const Complex Complex::operator+(const Complex &z) const {
    return Complex(Real + z.Real, Imagine + z.Imagine);
}

const Complex Complex::operator/(const Complex &z) const {
    Complex result;
    double znam = z.getImagine() * z.getImagine() + z.getReal() * z.getReal();
    double temp = (Real * z.getReal() + Imagine * z.getImagine()) / znam;
    result.setReal(temp);
    temp = (z.getReal() * Imagine - Real * z.getImagine()) / znam;
    result.setImagine(temp);
    return result;
}

const Complex Complex::operator*(const Complex &z) const {
    return Complex(Real * z.Real - Imagine * z.Imagine, Real * z.Imagine + Imagine * z.Real);
}

const Complex Complex::operator-(const Complex &z) const {
    return Complex(Real - z.Real, Imagine - z.Imagine);
}

std::ostream &operator<<(ostream &os, const Complex &complex) {
    double real = complex.getReal();
    double imagine = complex.getImagine();

    if (real == 0 and imagine == 0) {
        os << "0";
    } else {
        if (real != 0) {
            os << real;
            if (imagine > 0) os << '+';
        }
        if (imagine == -1) os << '-';
        if (imagine != 0) {
            if (imagine != 1 and imagine != -1) os << imagine;
            os << 'i';
        }
    }
    return os;
}

std::istream &operator>>(std::istream &in, Complex &complex) {
    double temp = 0;
    in >> temp;
    complex.setReal(temp);
    in >> temp;
    complex.setImagine(temp);
    return in;
}

double Complex::getReal() const {
    return Real;
}

void Complex::setReal(double real) {
    Complex::Real = real;
}

double Complex::getImagine() const {
    return Imagine;
}

void Complex::setImagine(double imagine) {
    Complex::Imagine = imagine;
}

Complex::Complex() : Real(0), Imagine(0) {

}

Complex::Complex(const double &real, const double &imagine) : Real(real), Imagine(imagine) {

}

Complex::~Complex() = default;

Complex::Complex(const Complex &complex) : Real(complex.getReal()), Imagine(complex.getImagine()) {

}


const Complex Complex::operator/(const double &x) const {
    return Complex(Real / x, Imagine / x);
}

const Complex Complex::operator*(const double &x) const {
    return Complex(Real * x, Imagine * x);
}

const Complex Complex::operator+(const double &x) const {
    return Complex(Real + x, Imagine);
}

const Complex Complex::operator-(const double &x) const {
    return Complex(Real - x, Imagine);
}

const Complex operator+(const double &x, const Complex &z) {
    return Complex(z.getReal() + x, z.getImagine());
}

const Complex operator-(const double &x, const Complex &z) {
    return Complex(z.getReal() - x, z.getImagine());
}

const Complex operator/(const double &x, const Complex &z) {
    return Complex(x, 0) / z;
}

const Complex operator*(const double &x, const Complex &z) {
    return Complex(z.getReal() * x, z.getImagine() * x);
}

const bool Complex::operator!=(const Complex &z) const {
    return (Real != z.getReal()) or (Imagine != z.getImagine());
}

const bool Complex::operator==(const Complex &z) const {
    return (Real == z.getReal()) or (Imagine == z.getImagine());
}

void Complex::trig() const {
    double r = sqrt(Real * Real + Imagine * Imagine);
    double fi = 0;
    if (Real == 0) {
        fi = atan2(0, Imagine);
    } else if (Imagine == 0) {
        fi = 0;
    } else {
        fi = atan2(Imagine, Real);
    }
    if (r == -1) {
        cout << "-";
    } else if (r != 1) {
        cout << r << "(cos(" << fi << ") + " << "isin(" << fi << "))";
    } else {
        cout << "cos(" << fi << ") + " << "isin(" << fi << ")";
    }
}

const Complex *Complex::nroot(unsigned n) const {
    if (Real == 0 and Imagine == 0) {
        return new Complex[n];
    };
    double R = sqrt(Real * Real + Imagine * Imagine);
    double Fi = acos(Real / R);
    double psi = 0;
    double temp = 0;
    R = pow(R, 1 / (double) n);
    Complex *mass = new Complex[n];
    for (int i = 0; i <= n; ++i) {
        psi = (Fi + 2 * M_PI * i) / (double) n;
        if (fabs(psi) == M_PI_2) {
            temp = 0;
        } else {
            temp = cos(psi);
        }
        mass[i].setReal(R * temp);
        if (fabs(psi) == M_PI) {
            temp = 0;
        } else {
            temp = sin(psi);
        }
        mass[i].setImagine(R * temp);
    }
    return mass;
}

const Complex Complex::powZ(const Complex &z2, const int &k = 0) const {
    Complex LnThis = this->LogZ(k);
    LnThis = LnThis * z2;
    double temp = LnThis.getImagine();
    double sin1 = 0;
    double cos1 = 0;
    while (temp > 2 * M_PI) {
        temp -= 2 * M_PI;
    }
    if (temp == M_PI_2 or temp == 3 * M_PI_2) {
        cos1 = 0;
        sin1 = sin(temp);
    } else if (temp == M_PI) {
        sin1 = 0;
        cos1 = cos(temp);
    } else {
        sin1 = sin(temp);
        cos1 = cos(temp);
    }
    return Complex(pow(M_E, LnThis.getReal()) * cos1,
                   pow(M_E, LnThis.getReal()) * sin1);
}

Complex &Complex::operator=(const Complex &complex) {
    this->setReal(complex.Real);
    this->setImagine(complex.Imagine);
    return *this;
}

const Complex Complex::LogZ(const int &k) const {
    double temp;
    double R = sqrt(Real * Real + Imagine * Imagine);
    if (Real == 0 and Imagine == 0) {
        temp = 0;
    } else {
        temp = acos(Real / R);
    }
    return Complex(log(R), temp + 2 * M_PI * k);
}

const Complex &Complex::operator+=(const Complex &x) {
    this->setReal(this->getReal() + x.getReal());
    this->setImagine(this->getImagine() + x.getImagine());
    return *this;
}

const Complex &Complex::operator-=(const Complex &x) {
    this->setReal(this->getReal() - x.getReal());
    this->setImagine(this->getImagine() - x.getImagine());
    return *this;
}

const Complex &Complex::operator/=(const Complex &z) {
    double znam = z.getImagine() * z.getImagine() + z.getReal() * z.getReal();
    double temp1 = (Real * z.getReal() + Imagine * z.getImagine()) / znam;
    double temp = (z.getReal() * Imagine - Real * z.getImagine()) / znam;
    this->setReal(temp1);
    this->setImagine(temp);
    return *this;
}

const Complex &Complex::operator*=(const Complex &x) {
    double real = Real;
    double imagine = Imagine;
    this->setReal(real * x.Real - imagine * x.Imagine);
    this->setImagine(real * x.Imagine + imagine * x.Real);
    return *this;
}

const Complex &Complex::operator+=(const double &x) {
    Real += x;
    return *this;
}

const Complex &Complex::operator-=(const double &x) {
    Real -= x;
    return *this;
}

const Complex &Complex::operator*=(const double &x) {
    Real *= x;
    Imagine *= x;
    return *this;
}

const Complex &Complex::operator/=(const double &x) {
    Real /= x;
    Imagine /= x;
    return *this;
}

#endif //_COMPLEX_CPP
