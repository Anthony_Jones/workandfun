/*
* Copyright © 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef ARRAY_UTILS_CPP
#define ARRAY_UTILS_CPP

#include <stdexcept>

#include "Random.hxx"

namespace cw::utils {
    size_t SwapCount = 0; // TODO DELETE
    /**
     * Shuffle items in "random" order.
     * @tparam T Type of Class which items you want to shuffle.
     * @tparam I Index type
     * @param [in] array Items
     * @param size Count os Items
     * @param random Function returns random Number between min and max
     * @param [out] array Shuffled Items
     */
    template<class T, class I, class Z>
    void shuffle(T &array, const I &size, Z (*random)(Z min, Z max) = random::lcg::rand) {
        for (I i = 0; i < size; ++i) {
            I pos2 = random(i, size - 1);
            Swap(array, i, pos2);
        }
    }

    /**
     * Each item is randomly Присваивается
     * @tparam T Class which contain Items
     * @tparam E Items Type
     * @tparam I Size Type
     * @param Array[in] Items
     * @param left[in] Left border of segment of Items which you want to Randomize
     * @param right[in] Left border of segment of Items which you want to Randomize
     * @param min[in] Infinum of random values
     * @param max[in] Supremum of random values
     * @param random[in] Function returning random Values of Type E between min and max
     * @param Array[out] Items with randomized segment of values
     */
    template<class T, typename E, class I>
    void RandArray(T &Array, const I &left, const I &right, E min, E max) {
        I size = right - left;
        uint64_t Min = min;
        uint64_t Max = max;
        for (I i = 0; i < size; ++i) {
            Array[left + i] = static_cast<E>(random::chrtrand(Min, Max));
        }
    }

    template<class T, class I>
    void Swap(T &Array, const I &left, const I &right) {
        auto temp = Array[left];
        Array[left] = Array[right];
        Array[right] = temp;
    }
}
#endif //ARRAY_UTILS_CPP