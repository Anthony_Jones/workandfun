/*
* Copyright © 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#include <iostream>
#include "OOP Examples/Complex.h"

using namespace std;

int main() {
    Complex(-10, -2).trig();
    cout << endl;
    double x = 2;
    Complex z1(-1, 0), z2(2, 1);
    int k = 5;
    const Complex *result = Complex(-1, 0).nroot(5);
    for (int j = 0; j < 5; ++j) {
        result[j].trig();
        cout << " = " << result[j] << endl;
    }

    cout << endl;
    cout << z1 + z2 << endl;
    cout << z1 * z2 << endl;
    cout << z1 / z2 << endl;
    cout << z1 - z2 << endl;
    cout << endl;
    cout << x + z1 << endl;
    cout << x - z1 << endl;
    cout << x * z2 << endl;
    cout << x / z2 << endl;
    cout << endl;
    cout << z1 << " += " << z2 << ": ";
    z1 += z2;
    cout << z1 << endl;
    cout << z1 << " -= " << z2 << ": ";
    z1 -= z2;
    cout << z1 << endl;
    cout << z1 << " *= " << z2 << ": ";
    z1 *= z2;
    cout << z1 << endl;
    cout << z1 << " /= " << z2 << ": ";
    z1 /= z2;
    cout << z1 << endl;
    cout << endl;
    cout << z1 << " +=" << x << ": ";
    z1 += x;
    cout << endl;
    cout << z1 << " -=" << x << ": ";
    z1 -= x;
    cout << endl;
    cout << z1 << " /=" << x << ": ";
    z1 /= x;
    cout << endl;
    cout << z1 << " *=" << x << ": ";
    z1 *= x;
    cout << endl << endl;
    cout << Complex(0, 0) << endl;
    cout << Complex(1, 0) << endl;
    cout << Complex(1, 1) << endl;
    cout << Complex(1, -1) << endl;
    cout << Complex(-1, -1) << endl;
    cout << Complex(0, 1) << endl;
    cout << Complex(0, -1) << endl;
    cout << Complex(-1, 0) << endl;
    cout << Complex(1, -1) << endl << endl;
    z1 = Complex(0, -1);
    z2 = Complex(2, 0);
    cout << z1 << "^" << z2 << ": ";
    cout << z1.powZ(z2, 0);
}