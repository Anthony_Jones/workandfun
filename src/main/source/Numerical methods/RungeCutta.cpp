/*
* Copyright © 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef RUNGE_CUTTA_CPP
#define RUNGE_CUTTA_CPP

#include <cstddef>
#include <iostream>

using std::cout;
using std::endl;

namespace cw {
/**
 * Runge-Cutta's method of 4th order
 * @tparam T
 * @param initialX
 * @param initialY
 * @param endOfSegment
 * @param initialPointsQuantity Чтобы сохранять только те точки, которые нужны для метода
 * @param pointsQuantity
 * @param rightPart
 * @return
 */
    template<typename T>
    T *RungeCutta(const T &initialX, const T &initialY, const T &endOfSegment, const size_t &initialPointsQuantity,
                  const size_t &pointsQuantity,
                  const T(*rightPart)(const T &, const T &)) {
        T step = (endOfSegment - initialX) / (pointsQuantity - 1);
        T initialStep = (endOfSegment - initialX) / (initialPointsQuantity - 1);
        T cursor = initialX;
        T initialCursor = initialX;
        T *solution = new T[initialPointsQuantity];
        T tempoRary = 0;
        T previousValue = initialY;
        for (size_t i = 0, j = 0; i < pointsQuantity; ++i) {
            T zeroPhi = step * rightPart(cursor, previousValue);
            T firstPhi =
                    step * rightPart(cursor + step / static_cast<T>(2), previousValue + zeroPhi / static_cast<T>(2));
            T secondPhi =
                    step * rightPart(cursor + step / static_cast<T>(2), previousValue + firstPhi / static_cast<T>(2));
            T thirdPhi = step * rightPart(cursor + step, previousValue + secondPhi);
            tempoRary = (zeroPhi + static_cast<T>(2) * firstPhi + static_cast<T>(2) * secondPhi + thirdPhi) /
                        static_cast<T>(6);
            previousValue += tempoRary;
            if (fabs(cursor - initialCursor) <= 10e-5) {
                solution[j] = previousValue;
                initialCursor += initialStep;
                ++j;
            }
            cursor += step;
        }
        return solution;
    }
}
#endif //RUNGE_CUTTA_CPP
