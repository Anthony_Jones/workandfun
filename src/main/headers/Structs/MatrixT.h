/*
* Copyright © 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef CPP_OOP_1_MATRIXT_H
#define CPP_OOP_1_MATRIXT_H

#include "../../utils/Trim.cpp"

#include <cstddef>
#include <iostream>
#include <stdexcept>
#include <sstream>
#include <cmath>

namespace cw {
    /**
     * @warning DOES NOT WORK AT WINDOWS. ONLY AT LINUX
     * @tparam T
     */
    template<class T>
    class MatrixT {
    protected:
        size_t Rows;
        size_t Columns;
        T **Array;

        virtual T **createArray2d(size_t rows, size_t columns) const noexcept(true) final {
            if (rows * columns == 0) {
                return nullptr;
            }
            auto **array = reinterpret_cast<T **>(new void *[rows + columns * rows]);
            array[0] = reinterpret_cast<T *>(array + rows);
            for (size_t i = 1; i < rows; i++) {
                array[i] = array[0] + i * columns;
            }
            return array;
        }

    public:


        MatrixT();

        MatrixT(size_t rows, size_t columns);

        MatrixT(size_t Rows, size_t Columns, T object);

        MatrixT(const MatrixT<T> &other);

        virtual ~MatrixT();

        size_t getRows() const;

        size_t getColumns() const;

        T &operator()(long row, long column) const;

        const bool operator==(const MatrixT<T> &matrixT) const;

        const bool operator!=(const MatrixT<T> &matrixT) const;

        MatrixT<T> &operator=(const MatrixT<T> &other);

        template<class V>
        friend std::ostream &operator<<(std::ostream &os, const MatrixT<V> &t);

        template<class V>
        friend std::istream &operator>>(std::istream &is, MatrixT<V> &t);

        virtual void swapLine(T **array, size_t Src, size_t Dest) const final;

        MatrixT<T> swapLine(size_t src, size_t destination) const;
    };
}


#endif //CPP_OOP_1_MATRIXT_H

#include "../../source/OPP Examples/MatrixT.cpp"
