/*
 *    Copyright © 2019 Allex Atamanenko. All rights reserved.
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#ifndef CPP_OOP_1_TIMER_HXX
#define CPP_OOP_1_TIMER_HXX

#include <chrono>
#include <ostream>

using std::chrono::time_point;
using std::chrono::high_resolution_clock;
using std::chrono::nanoseconds;
using std::chrono::duration_cast;
namespace cw {
    extern __inline__ uint64_t rdtsc() {
        uint64_t x;
        __asm__ __volatile__ ("rdtsc" : "=A" (x));
        return x;
    }

    class TickTimer {
    public:
        class StartedTickTimer {
            friend class TickTimer;

            typedef uint64_t tick;
            tick timePoint;
        public:
            friend std::ostream &operator<<(std::ostream &os, const StartedTickTimer &timer);

        private:

            explicit StartedTickTimer() : timePoint(rdtsc()) {}


        private:
        public:
            tick started() const {
                return timePoint;
            }

            tick finish() const {
                return rdtsc() - timePoint;
            }
        };

        static StartedTickTimer start() {
            return StartedTickTimer();
        }

    };

    std::ostream &operator<<(std::ostream &os, const TickTimer::StartedTickTimer &timer) {
        os << timer.finish();
        return os;
    }

    class Timer {
    public:
        class StartedTimer {
            time_point<high_resolution_clock, nanoseconds> timePoint;

            explicit StartedTimer() : timePoint(high_resolution_clock::now()) {}

            friend class Timer;


        private:
        public:
            uint64_t finish() const {
                auto finish = std::chrono::high_resolution_clock::now();
                uint64_t i = duration_cast<std::chrono::nanoseconds>(finish - timePoint).count();
                return i;
            }
        };

        static StartedTimer start() {
            return StartedTimer();
        }

    };
}

#endif //CPP_OOP_TIMER_HXX
