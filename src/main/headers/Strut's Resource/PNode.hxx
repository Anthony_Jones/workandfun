/*
* Copyright © 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef CPP_OOP_PNODE_HXX
#define CPP_OOP_PNODE_HXX

template<class T>
class PNode {
    T item;

    PNode *previous = nullptr;


public:
    explicit PNode(const T &item) : item(item), previous(nullptr) {}

    explicit PNode(PNode *PNode) : previous(PNode) {}

    PNode(T item, PNode *PNode) : item(item), previous(PNode) {}

    PNode *getPrevious() const {
        return previous;
    }

    void setPrevious(PNode *other) {
        PNode::previous = other;
    }

    const T get() const {
        return item;
    }

    void set(const T otherItem) {
        PNode::item = otherItem;
    }

    virtual ~PNode() {
        previous = nullptr;
    }

};


#endif //CPP_OOP_PNODE_HXX
