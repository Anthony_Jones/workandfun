/*
* Copyright © 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#include <iostream>

#include "../source/Sorts/Sorts.cpp"
#include "Structs/Array.hxx"
#include "Structs/ArrayList.h"

using std::cout;
using std::endl;

using namespace cw;
using utils::RandArray;
using utils::shuffle;

int compare(const int &one, const int &another) {
    if (one == another) {
        return 0;
    } else if (one < another) {
        return -1;
    } else {
        return 1;
    }
}


int main() {
    Timer ar1;
    ptrdiff_t n = 10000, n2;
    Array<int> A;
    ArrayList<int> B;
    cout << " Quick Sort \n";
    for (ptrdiff_t i = 1; i <= 50; ++i) {
        n2 = n * i;
        A = Array<int>(n2);
        for (ptrdiff_t j = 0; j < n2; ++j) {
            A[j] = j;
        }
        shuffle(A, n2, random::lcg::rand);
        auto ar = ar1.start();
        QuickSort(A, static_cast<ptrdiff_t>(0), n2 - 1, true);
        cout << ar.finish() << endl;
    }
//    for (ptrdiff_t i = 0; i < n2; ++i) {
//        cout << A[i] << " ";
//    }
    cout << "\n Merge Sort:" << endl;
    for (ptrdiff_t i = 1; i <= 50; ++i) {
        n2 = n * i;
        A = Array<int>(n2);
        for (ptrdiff_t j = 0; j < n2; ++j) {
            A[j] = j;
        }
        shuffle(A, n2, random::lcg::rand);
        auto ar = ar1.start();
        MergeSort(A, static_cast<ptrdiff_t>(0), n2 - 1, compare);
        cout << ar.finish() << endl;
    }
//    for (ptrdiff_t i = 0; i < n2; ++i) {
//        cout << A[i] << " ";
//    }
    cout << "\n Heap Sort" << endl;
//    n2 = n;
//    A = Array<int>(n2);
//    for (ptrdiff_t i = 0; i < n2; ++i) {
//        A[i] = i;
//    }
//    shuffle(A, n2, random::lcg::rand);
//    auto ar = ar1.start();
//    HeapSort(A, n2, compare);
//    cout << ar.finish() << " nsec " << endl;
    for (ptrdiff_t i = 1; i <= 50; ++i) {
        n2 = n * i;
        A = Array<int>(n2);
        for (ptrdiff_t j = 0; j < n2; ++j) {
            A[j] = j;
        }
        shuffle(A, n2, random::lcg::rand);
        auto ar = ar1.start();
        HeapSort(A, n2, compare);
        auto temp = ar.finish();
        cout << temp << endl;
    }
//    for (ptrdiff_t i = 0; i < n2; ++i) {
//        cout << A[i] << " ";
//    }
}