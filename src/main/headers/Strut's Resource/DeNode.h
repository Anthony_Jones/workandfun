/*
* Copyright © 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once
#ifndef CPP_OOP_NODE_H
#define CPP_OOP_NODE_H
namespace cw {
/**
 * Class for Lists,DeQueue, Trees etc.
 * @tparam T
 */
    template<class T>
    class DeNode {


    public:
        T item;

        DeNode<T> *next;
        DeNode<T> *previous;

        explicit DeNode(const T &item, DeNode<T> *next = nullptr, DeNode<T> *previous = nullptr) :
                item(item),
                next(next),
                previous(previous) {}

        ~DeNode() {
            next = nullptr;
            previous = nullptr;
        }

        T get() const {
            return item;
        }

        void set(T Item) {
            DeNode::item = Item;
        }

        DeNode<T> *getNext() const {
            return next;
        }

        void setNext(DeNode<T> *Next) {
            DeNode::next = Next;
        }

        DeNode<T> *getPrevious() const {
            return previous;
        }

        void setPrevious(DeNode<T> *Previous) {
            DeNode::previous = Previous;
        }
    };
}

#endif //CPP_OOP_NODE_H
