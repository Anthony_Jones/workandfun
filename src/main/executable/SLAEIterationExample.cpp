/*
* Copyright © 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#include <iostream>
#include <Structs/MatrixRectangle.h>
#include <Structs/MatrixSquare.h>
#include "../source/Numerical methods/LinerAlgebra.cpp"

using std::cout;
using std::cin;
using std::endl;
typedef double CompType;

using namespace cw;

int main() {
    constexpr CompType EPSILON = 1e-50;

    MatrixRectangle<CompType> dano(3, 4);
    dano(0, 0) = 3;
    dano(0, 1) = 2;
    dano(0, 2) = -1;
    dano(0, 3) = 4;  //1
    dano(1, 0) = 2;
    dano(1, 1) = 5;
    dano(1, 2) = -1;
    dano(1, 3) = 9;  //2
    dano(2, 0) = 1;
    dano(2, 1) = 1;
    dano(2, 2) = 6;
    dano(2, 3) = 21;  //3

    cout << "Сиcтема: \n" << dano << endl;
    auto roots = SeidelsMethod(dano, EPSILON);
    cout << "\nКорни этой системы линейных уравнений: {";
    for (CompType &i: roots) {
        cout << i;
        if (&i != &roots.back()) cout << ", ";
    }
    cout << "} \n";

//    roots = IterationRoots(dano, EPSILON);
//    cout << "\nКорни этой системы линейных уравнений: {";
//    for (CompType &i: roots) {
//        cout << i;
//        if (&i != &roots.back()) cout << ", ";
//    }
//    cout << "} \n";
//    CompType temp1;
//    for (size_t l = 0; l < result.getRows(); ++l) {
//        temp1 = 0;
//        for (size_t i = 0; i < result.getColumns() - 1; ++i) {
//            cout << roots[i] * dano(l, i);
//            if (i != result.getColumns() - 2) cout << " + ";
//            temp1 += roots[i] * dano(l, i);
//        }
//        cout << " = " << dano(l, result.getColumns() - 1) << endl;
//    }
    return 0;
}
