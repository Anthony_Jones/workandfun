/*
* Copyright � 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include <cstdio>
#include <cstdlib>
#include <stdexcept>

<<<<<<< HEAD
#include "lcg_rand.h"

=======

#include "../resource/Random.hxx"


//#include
>>>>>>> feature/MergeSort
using namespace std;
using namespace cw;

// �����: ��������������� ������ ��������� ���� double
// ��������� ��������� ���������� � 1
class DoubleArray {
private:
// ���� ������
    double *Array;        // ��������: ��������� �� ������ ��������� ���� double
    size_t count;    // ��������: ���������� ��������� (�����) �������

    size_t swapCount;
    size_t compareCount;
// �������� ������
private:
    // �������� ����� ��� ���������� �������
    int resize(size_t N);

public:
    size_t getSwapCount() const;

    size_t getCompareCount() const;
// �������� ������
public:
// ������������
    ~DoubleArray();

    DoubleArray();

    explicit DoubleArray(size_t N);

    DoubleArray(DoubleArray &D);

// ���������
    DoubleArray &operator=(const DoubleArray &D);

    double &operator[](size_t i) const;

    const bool operator==(const DoubleArray &other) const;

// ������, ��������� � �������������� ������ � ����������� �������
    void BuildSet(size_t N);

    void Clear();

    size_t Count() const;

    void RandArray(size_t N);

    void Resize(size_t N, double f = 0.0);

// ������, ������������ ��� ����������
    bool Compare(size_t i, size_t j);

    bool CompareM(size_t i, size_t j);

    void InvOrder();

    void Mix();

    void Swap(size_t i, size_t j);

// ������ ������ � ������ � ����
    int LoadFromFile(const char *file);

    int SaveToFile(const char *file, const char *format);


};/////////////////////////////////////////////////////////////////////

int DoubleArray::resize(size_t N) {
    // �������� ����� ������.
    // ���� ����� ������ ������� (N) �� ����� ������� (size),
    // ����� ������� ������ ���� ������ � �������� �����
    // ���� ����� ������ ������� (N) ����� ������� (size),
    // ����� �� ���������� ������� ����������� � �������
    // ��� ���������� ���������� ������ ������� ���������� ���������� ���������,
    // ��� ������� �������� ������
    // ��� ��������� ���������� ������� ���������� �������� -1
    if (N != count) {
        if (Array != NULL) {// ���� ��������� �� NULL, ������� ������
            delete[]Array;
            Array = NULL;
        }
        if (N > 0) {// ���� N>0 �������� ����� ���� ������
            count = N;
            try { Array = new double[count]; }
            catch (...) {// ���� ��� ��������� ������ �������� ������:
                Array = NULL;
                count = 0;
                return -1;
            }
        } else count = 0;
    }
    return static_cast<int>(count);
}

// ���������� ������ DoubleArray
DoubleArray::~DoubleArray() { resize(0); }

// ����������� ������ DoubleArray,
// ������ "������" ������
DoubleArray::DoubleArray() : swapCount(0), compareCount(0) {
    Array = NULL;
    count = 0;
}

// ����������� ������ DoubleArray,
// ������ ������ �� N ��������� � ��������� ��� ������
DoubleArray::DoubleArray(size_t N) : swapCount(0), compareCount(0) {
    Array = NULL;
    count = 0;
    if (resize(N) != -1)
        for (size_t i = 0; i < count; ++i) Array[i] = 0.0;
    else throw "memory realloc error in DoubleArray";
}

// ����������� ����������� ������ DoubleArray
DoubleArray::DoubleArray(DoubleArray &D) : swapCount(0), compareCount(0) {
    Array = nullptr;
    count = 0;
    if (resize(D.count) != -1)
        for (size_t i = 0; i < count; ++i) Array[i] = D.Array[i];
    else throw "memory realloc error in DoubleArray";
}

// �����: �������� ��������� �����������
DoubleArray &DoubleArray::operator=(const DoubleArray &D) {
    if (&D != this) {
        // ���� ������ �������� �� ������ -- ������������ ����������
        if (resize(D.count) != -1)
            for (size_t i = 0; i < count; ++i) Array[i] = D.Array[i];
        else throw "memory realloc error in DoubleArray";
    }
    return *this;
}

// �����: �������� ��������� � �������� ������� �� �������
// ��������� ��������� ������� ���������� � 1
double &DoubleArray::operator[](size_t i) const {
    // ���� ������ ������� �� ���������� ������� --
    // ������������ �������������� ��������
    if (i < 0 || i >= count) throw std::out_of_range("��� �������");
    return Array[i];
}

// ����� ���������� ��������� (������, ��� �������� �������� ��������)
// �� N ���������, ������� � ��������� �� 0 �� 1
void DoubleArray::BuildSet(size_t N) {
    RandArray(N);
    size_t i;
    for (i = 0; i < count; ++i) Array[i - 1] += i;
    for (i = 0; i < count; ++i) Array[i - 1] /= count + 1;
}

// ����� ������� ������, ���������� ��� �������� ��������� �������
void DoubleArray::Clear() { resize(0); }

// ����� ���������� ���������� ��������� �������
size_t DoubleArray::Count() const { return count; }

// ����� �������� ������ ������� �� N � ��������� ��� ��������
// ���������� ������� � ��������� [0..1]
void DoubleArray::RandArray(size_t N) {
    if (resize(N) > 0) {
//		 ���������� ��������� ����� ���� double,
//		 ���������� �������������� �� ������� inf..sup
        double inf = 0.0;
        double sup = 1.0;
        double K = (sup - inf) / (900);
        for (size_t i = 0; i < count; ++i) {
            Array[i] = cw::random::chrtrand(0, 900) * K;
        };
    }
}

// ����� �������� ������ ������� �� N � ��������� ��� ��������
// ��������� ������� ��������� f (�� ��������� f=0)
void DoubleArray::Resize(size_t N, double f) {
    if (resize(N) != -1)
        for (size_t i = 0; i < count; ++i) Array[i] = f;
    else throw "memory realloc error in DoubleArray";
}

// ����� Comp(size_t i, size_t j) ���������� i-� � j-� ��������.
// ���������� true, ���� A[i] <= A[j]
bool DoubleArray::Compare(size_t i, size_t j) {
    // ���� ������ ���� ��� ���� ������� �������
    // �� ������� ������� -- ���������� false
    ++compareCount;
    <<<<<<< HEAD
    if (count == 0 || i < 1 || i > count || j < 1 || j > count)
    =======
    if (count == 0 || i < 0 || i >= count || j < 0 || j >= count)
    >>>>>>> feature / MergeSort
    return false;
    return Array[i] < Array[j];
}

// ����� Comp(size_t i, size_t j) ���������� i-� � j-� ��������.
// ���������� true, ���� A[i] > A[j]
bool DoubleArray::CompareM(size_t i, size_t j) {
    // ���� ������ ���� ��� ���� ������� �������
    // �� ������� ������� -- ���������� false
    if (count == 0 || i < 0 || i > count || j < 0 || j > count)
        return false;
    return Array[i - 1] >= Array[j - 1];
}

// ��������� �������� ������� � ������� �������� �������
void DoubleArray::InvOrder() {
    // ��������� �������� ������� � ������� �������� �������
    for (size_t i = 0; i < count / 2; ++i)
        Swap(i, count + 1 - i);
}

// ����� ������������ �������� ������� � ��������� �������
void DoubleArray::Mix() {
    size_t i, j, k;
    // ������� ���� ����� ��� ����� ������������ �������������
    // �������� ���������� ����������� ��� k = �� 2 �� 5
    for (k = 0; k < 3; ++k) {
        for (i = 0; i < count; ++i) {
            // �������� ��������� ������ �� 0 �� cnt
            j = rand() % count + 1;
            // ������ ������� i-� � j-� ��������
            Swap(i, j);
        }
    }
}

// ����� ������ ������� �������� � ���������� ��������
void DoubleArray::Swap(size_t i, size_t j) {
    // ���� ������� ������� �� ������� ������� --
    // ������������ ����������
    ++swapCount;
    if (i < 0 || i >= count || j < 0 || j >= count)
        throw std::out_of_range("Out of range");
    else {
        double T = Array[i];
        Array[i] = Array[j];
        Array[j] = T;
    }
}

// ������� ������ ������ �� ���������� ����� file
int DoubleArray::LoadFromFile(const char *file) {
//	FILE *f;
//	if ( fopen_s(&f, file, "r") == 0 ) {
//		size_t k;
//		fscanf_s(f, "%i", &k);
//		if ( resize(k) > 0 ) {
//			for ( k = 0; k < size; ++k ) {
//				try { fscanf_s(f, "%lf", &(A[k])); }
//				catch(...){
//					fclose(f);
//					return k;
//				}
//			}
//		}
//		fclose(f);
//		return size;
//	}
    return -1;
}

// ������� ��������� ���������� ������ ��������� ������� A � ��������� ���� file
// � ������ ������ ����� ����������� ���������� ��������� ������� size
// �������� format "%9.8lf"
int DoubleArray::SaveToFile(const char *file, const char *format = "%9.8lf") {
//	 ���� ������ ���� -- ������ � ���� �� ������������
//	if ( A == NULL ) return -1;
//	FILE *f;
//	if ( fopen_s(&f, file, "w") == 0 ) {
//		fprintf_s(f, "%i", size);
//		fprintf_s(f, "\n");
//		for ( size_t i = 0; i < size; ++i ) {
//			fprintf_s(f, format, A[i]); // ������ ���!!!
//			fprintf_s(f, "\n");
//		}
//		fclose(f);
//		return size;
//	}
    return 0;
}

size_t DoubleArray::getSwapCount() const {
    return swapCount;
}

size_t DoubleArray::getCompareCount() const {
    return compareCount;
}

const bool DoubleArray::operator==(const DoubleArray &other) const {
    if (Count() == other.Count()) {
        for (size_t i = 0; i < Count(); ++i) {
            if (Array[i] != other.Array[i])
                return false;
        }
        return true;
    } else {
        return false;
    }
}
