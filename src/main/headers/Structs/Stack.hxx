/*
* Copyright © 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef CPP_OOP_STACK_HXX
#define CPP_OOP_STACK_HXX

#include <iostream>

#include "exception/EmptyStackException.hxx"
#include "exception/NullPointerException.hxx"
#include "Strut's Resource/PNode.hxx"


namespace cw {
    template<class T>
    class Stack {
        PNode<T> *currentPNode;
    public:
        Stack() : currentPNode(nullptr) {}

        Stack(Stack &stack) {
            if (stack.currentPNode != nullptr) {
                Stack tmp;
                PNode<T> *node = stack.currentPNode;
                while (node != nullptr) {
                    tmp.push(node->get());
                    node = node->getPrevious();
                }
                node = tmp.currentPNode;
                while (node != nullptr) {
                    push(node->get());
                    node = node->getPrevious();
                }
            }
        }

        Stack &push(const T &t) {
            currentPNode = new PNode<T>(t, currentPNode);
            return *this;
        }

        T pop() {
            auto node = currentPNode;
            T value = currentPNode->get();
            currentPNode = (*currentPNode).getPrevious();
            delete node;
            return value;
        }

        T peek() const {
            if (currentPNode == nullptr) throw exception::NullPointerException();
            return currentPNode->get();
        }

        bool isEmpty() const {
            return currentPNode == nullptr;
        }

        int search(const T &value) const {
            PNode<T> *node = currentPNode;
            for (int i = 0; node != nullptr; ++i) {
                if (node->get() == value) return i;
            }
            return -1;
        }

        friend std::ostream &operator<<(std::ostream &os, const Stack &stack) {
            if (stack.currentPNode != nullptr) {
                PNode<T> *node = stack.currentPNode;
                while (node != nullptr) {
                    os << *node;
                    node = node->getPrevious();
                }

            }
            return os;
        }

        std::string toString() {
//            std::stringstream str;
//            str<<"[ ";
//            PNode<T> * node = currentPNode;
//            while(node != nullptr){
//                str << (node->get());
//                node = node->getPrevious()();
//                if(node != nullptr){
//                    str << ", ";
//                }
//            }
//            str<<" ]";
//            return str.str();
            return "";
        }

        virtual ~Stack() {
            PNode<T> *node = currentPNode;
            while (currentPNode != nullptr) {
                currentPNode = (*currentPNode).getPrevious();
                delete node;
                node = currentPNode;
            }
        }
    };
}

#endif //CPP_OOP_STACK_HXX
