#include <Structs/MatrixT.h>
#include <gtest/gtest.h>

using namespace cw;

TEST(MatrixT, equality) {
    MatrixT<double> temp1(3, 4);
    MatrixT<double> temp2(3, 4, 0.0);
    ASSERT_TRUE(temp1 == temp2);
    ASSERT_FALSE(temp1 == MatrixT<double>(1, 2));
    ASSERT_FALSE(temp1 != temp2);
    ASSERT_TRUE(temp1 != MatrixT<double>(1, 2));
}

TEST(MatrixT, access) {
    MatrixT<double> temp1(5, 6, 10);
    ASSERT_EQ(temp1(4, 5), 10);
    temp1(0, 0) += 1;
    ASSERT_EQ(temp1(0, 0), 11);
}

TEST(MatrixT, construct) {
    MatrixT<double> temp1(5, 6, 10);
    ASSERT_EQ(temp1, MatrixT(temp1));
    MatrixT temp2(temp1);
    ASSERT_EQ(temp2 = temp1, temp1);
}



