/*
* Copyright © 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef CPP_OPP_MATRIXT_CPP
#define CPP_OPP_MATRIXT_CPP

#include "../../utils/Trim.cpp"
#include <Structs/MatrixT.h>


#include <stdexcept>
#include <sstream>
#include <cmath>
#include <cstring>

namespace cw {

    template<class T>
    MatrixT<T>::MatrixT(): Rows(0), Columns(0) {
        Rows = 0;
        Columns = 0;
        Array = nullptr;
    }

    /**
     * @throw std::bad_alloc When memory allocation is failed
     * @tparam T
     * @param rows
     * @param columns
     */
    template<class T>
    MatrixT<T>::MatrixT(size_t rows, size_t columns): Rows(rows), Columns(columns) {
        try {
            Array = MatrixT < T > ::createArray2d(Rows, Columns);
        } catch (std::bad_alloc &e) {
            throw e;
        }
    }

    /**
     * @throw std::bad_alloc When memory allocation is failed
     * @tparam T
     * @param Rows
     * @param Columns
     * @param object
     */
    template<class T>
    MatrixT<T>::MatrixT(size_t Rows, size_t Columns, T object): Rows(Rows), Columns(Columns) {
        try {
            Array = MatrixT < T > ::createArray2d(Rows, Columns);
        } catch (std::bad_alloc &e) {
            throw e;
        }
        for (size_t i = 0; i < Rows; ++i) {
            for (size_t j = 0; j < Columns; ++j) {
                Array[i][j] = object;
            }
        }
    }

    template<class T>
    MatrixT<T>::MatrixT(const MatrixT <T> &other): Rows(other.Rows), Columns(other.Columns) {
        if (Rows == 0 or Columns == 0) {
            Array = nullptr;
        } else {
            try {
                Array = MatrixT < T > ::createArray2d(Rows, Columns);
            } catch (std::bad_alloc &e) {
                throw e;
            }
            memcpy(Array[0], other.Array[0], sizeof(T) * Rows * Columns);
            for (size_t i = 1; i < Rows; ++i) {
                *(Array + i) = *Array + Columns * i;
            }
        }
    }

    /**
     * @throw std::out_of_range When index is out of range or negative
     * @tparam T
     * @param row
     * @param column
     * @return
     */
    template<class T>
    T &MatrixT<T>::operator()(long row, long column) const {
        if (row < 0 or column < 0) {
            throw std::out_of_range("Index can not be negative");
        } else if ((size_t) row >= Rows or (size_t) column >= Columns) {
            throw std::out_of_range("Index out of range");
        }
        return Array[row][column];
    }

    template<class T>
    MatrixT<T>::~MatrixT() {
        delete[] Array;
    }

    template<class T>
    const bool MatrixT<T>::operator==(const MatrixT <T> &matrixT) const {
        if (Rows != matrixT.Rows or Columns != matrixT.Columns) {
            return false;
        } else {
            for (size_t i = 0; i < Rows; ++i) {
                for (size_t j = 0; j < Columns; ++j) {
                    if (Array[i][j] != matrixT.Array[i][j]) {
                        return false;
                    }
                }
            }
            return true;
        }
    }

    template<class T>
    const bool MatrixT<T>::operator!=(const MatrixT <T> &matrixT) const {
        if (Rows != matrixT.Rows or Columns != matrixT.Columns) {
            return true;
        } else {
            for (size_t i = 0; i < Rows; ++i) {
                for (size_t j = 0; j < Columns; ++j) {
                    if (Array[i][j] != matrixT.Array[i][j]) {
                        return true;
                    }
                }
            }
            return false;
        }
    }

    /**
     * @throw std::bad_alloc When memory allocation is failed
     * @tparam T
     * @param other
     * @return
     */
    template<class T>
    MatrixT <T> &MatrixT<T>::operator=(const MatrixT <T> &other) {
        if (*this == other) {
            return *this;
        } else if (Rows == other.Rows and Columns == other.Columns) {
            memcpy(Array[0], other.Array[0], sizeof(T) * Rows * Columns);
            for (size_t i = 1; i < Rows; ++i) {
                *(Array + i) = *Array + Columns * i;
            }
        } else {
            delete[] Array;
            Rows = other.Rows;
            Columns = other.Columns;
            try {
                Array = MatrixT<T>::createArray2d(Rows, Columns);
            } catch (std::bad_alloc &e) {
                string ErrorMessage(e.what());
                ErrorMessage += "\n called by operator =\n";
                throw std::bad_alloc();
            }
            memcpy(Array[0], other.Array[0], sizeof(T) * Rows * Columns);
            for (size_t i = 1; i < Rows; ++i) {
                *(Array + i) = *Array + Columns * i;
            }

        }
        return *this;
    }

    template<class T>
    std::ostream &operator<<(std::ostream &os, const MatrixT <T> &t) {
        for (size_t i = 0; i < t.Rows; ++i) {
            for (size_t j = 0; j < t.Columns; ++j) {
                os << t.Array[i][j] << ' ';
            }
            os << std::endl;
        }
        os << std::endl;
        return os;
    }

    template<class T>
    std::istream &operator>>(std::istream &is, MatrixT <T> &t) {
        std::string string1;
        std::getline(is, string1);                   //getting Size
        string1 = trim(string1);
        size_t RowQuantity = 1;
        if (string1.empty()) {
            if (t.getRows() != 0) {
                t.Rows = 0;
                t.Columns = 0;
                delete[] t.Array;
            }
        } else {
            double temp;
            std::istringstream ss(string1);
            ss >> temp;
            if (fabs(fmod(temp, 1)) > 0) {
                throw std::domain_error("Row quantity can not be fractional");
            } else if (temp < 0) {
                throw std::domain_error("Row quantity can not be negative");
            } else {
                RowQuantity = (size_t) temp;
                if (RowQuantity != t.Rows) {
                    t.Rows = RowQuantity;
                }
            }
        }
        size_t ColumnsQuantity = 1;
        std::getline(is, string1);
        std::string tempString;
        std::unique_copy(string1.begin(), string1.end(),
                         std::back_inserter(tempString), // deleting repeating whitespaces
                         [](char c1, char c2) { return isspace(c1) and isspace(c2); });
        cout << tempString << endl;
        tempString = trim(
                tempString);                                         //deleting whitespaces at begin, and end of string
        if (tempString.empty()) {
            delete[] t.Array;
            t.Rows = 0;
            return is;
        }
        for (char i : tempString) {
            if (isspace(i)) {
                ++ColumnsQuantity;
            }
        }
        t.Columns = ColumnsQuantity;
        delete[] t.Array;
        t.Array = t.createArray2d(t.Rows, t.Columns);
        std::istringstream ss(tempString);
        for (size_t j = 0; j < t.Columns; ++j) {
            ss >> t.Array[0][j];
        }
        string1.erase();
        tempString.erase();
        for (size_t k = 1; k < t.Rows; ++k) {
            std::getline(is, string1);
            std::unique_copy(string1.begin(), string1.end(),
                             std::back_inserter(tempString), // deleting repeating whitespaces
                             [](char c1, char c2) { return isspace(c1) and isspace(c2); });
            tempString = trim(
                    tempString);                                         //deleting whitespaces at begin, and end of string
            if (tempString.empty()) {
                for (size_t j = 0; j < t.Columns; ++j) {
                    t.Array[k][j] = T();
                }
            }
            std::istringstream ss1(tempString);
            for (size_t j = 0; j < t.Columns; ++j) {
                ss1 >> t.Array[k][j];
            }
            string1.erase();
            tempString.erase();
        }
        return is;
    }

    template<class T>
    size_t MatrixT<T>::getRows() const {
        return Rows;
    }

    template<class T>
    size_t MatrixT<T>::getColumns() const {
        return Columns;
    }

    /**
     * @brief additional function to swap lines in matrix. Used in MatrixAlgebraic<T>::determinant()
     * @tparam T
     * @param[in] array Matrix to swap
     * @param[out] array Matrix with swaped lines
     * @param pos Which line swap
     * @param inPos To which position swap
     */
    template<class T>
    void MatrixT<T>::swapLine(T **array, size_t src, size_t dest) const {
        T *temp = array[src];
        array[src] = array[dest];
        array[dest] = temp;
    }

    template<class T>
    MatrixT <T> MatrixT<T>::swapLine(size_t src, size_t destination) const {
        MatrixT result(*this);
        T *temp = result.Array[src];
        result.Array[src] = result.Array[destination];
        result.Array[destination] = temp;
        delete temp;
    }

}

#endif //CPP_OPP_MATRIXT_CPP