/*
* Copyright © 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef SORT_UTILS_CPP
#define SORT_UTILS_CPP

#include <cstddef>

#include <Structs/Stack.hxx>
#include "../headers/exception/NullPointerException.hxx"
#include "ArrayUtils.cpp"
#include "Compare.cpp"

namespace cw::utils {
    template<class T, class E, class I>
    I PartitionWithComp(T &Array, I left, I right, int (*Compare)(const E &, const E &)) {
        E pivot = Array[right];
        I i = left - 1;
        for (I j = left; j < right; ++j) {
            if (Compare(Array[j], pivot) <= 0) {
                ++i;
                Swap(Array, i, j);
            }
        }
        Swap(Array, i + 1, right);
        return i + 1;
    }

    template<class T, class I>
    I Partition(T &Array, I left, I right, bool type = true) {
        auto pivot = Array[right];
        I i = left - 1;
        for (I j = left; j < right; ++j) {
            if ((Compare(Array[j], pivot) <= 0) ^ !type) {
                ++i;
                Swap(Array, i, j);
            }
        }
        Swap(Array, i + 1, right);
        return i + 1;
    }

    template<class T, class I, class E>
    void Merge(T &Array, const I &left, const I &central, const I &right, int (*compare)(const E &, const E &)) {
        Stack<E> leftStack, rightStack;
        for (I i = central + 1; i > left; --i) {
            leftStack.push(Array[i - 1]);
        }
        for (I j = right - 1; j >= central; --j) {
            rightStack.push(Array[j + 1]);
        }
        for (I k = left; k <= right; ++k) {
            if (leftStack.isEmpty() or rightStack.isEmpty()) {
                while (!rightStack.isEmpty()) {
                    Array[k] = rightStack.pop();
                    ++k;
                }
                while (!leftStack.isEmpty()) {
                    Array[k] = leftStack.pop();
                    ++k;
                }
                return;
            } else {
                if (compare(leftStack.peek(), rightStack.peek()) <= 0) {
                    Array[k] = leftStack.pop();
                } else {
                    Array[k] = rightStack.pop();
                }
            }
        }
    }

    template<class T, class I, class E>
    void Heapify(const T &Array, const I &element, const I &heap_size, int(*compare)(const E &, const E &)) {
        I max = element;
        I left = 2 * element + 1;
        I right = left + 1;
        if (left < heap_size and Compare(Array[left], Array[max]) > 0) {
            max = left;
        }
        if (right < heap_size and Compare(Array[right], Array[max]) > 0) {
            max = right;
        }
        if (max != element) {
            Swap(Array, max, element);
            Heapify(Array, max, heap_size, compare);
        }
    }

    template<class T, class I, class E>
    void BuildHeap(const T &Array, const I &heap_size, int(*compare)(const E &, const E &)) {
        for (I i = heap_size / 2; i >= 0; --i) {
            Heapify(Array, i, heap_size, compare);
        }
    }


    template<class T, class E, class I>
    I medWithComparator(const T &Array, I left, I right, int(*Compare)(const E &, const E &)) {
        if (Compare(Array[left], Array[(right + left) / 2]) > 0 || Compare(Array[left], Array[right]) > 0) {
            if (Compare(Array[(right + left) / 2], Array[right]) > 0)
                return (right + left) / 2;
            else
                return right;
        } else
            return left;
    }

    template<class T, class I>
    I med(const T &Array, I left, I right) {
        if (Array[left] > Array[(right + left) / 2] || Array[left] > Array[right]) {
            if (Array[(right + left) / 2] > Array[right])
                return (right + left) / 2;
            else
                return right;
        } else
            return left;
    }
}
#endif //SORT_UTILS_CPP