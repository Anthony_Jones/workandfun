
/*
* Copyright © 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef CPP_OOP_ARRAY_HXX
#define CPP_OOP_ARRAY_HXX


#include <stdexcept>

namespace cw {
/**
 * Class Array for Sorting Algorithms
 * @date March 2019
 * @author Anton Ivaniv
 * @throws std::bad_alloc, std::out_of_range
 * @tparam T type of Items
 */
    template<class T>
    class Array {
        T *Items;
        size_t Size;
    public:
        Array() : Items(nullptr), Size(0) {
        }

        explicit Array(size_t size) : Size(size) {
            Items = new T[Size];
            if (Items == nullptr) {
                throw ::std::bad_alloc();
            }
        }

        Array(T *items, size_t size) : Size(size) {
            Items = new T[Size];
            for (size_t i = 0; i < Size; ++i) {
                Items[i] = items[i];
            }
        }

        Array(const Array &other) : Size(other.Size) {
            Items = new T[Size];
            if (Items == nullptr) {
                throw ::std::bad_alloc();
            }
        }

        ~Array() {
            delete[] Items;
        }

        /**
         * @throw std::out_of_range
         * @param at Index of Items you want to read/write
         * @return Reference to Ttem
         */
        T &operator[](const size_t &at) const {
            if (at > Size) {
                throw ::std::out_of_range("Index is out of bound");
            }
            return Items[at];
        }

        const bool operator==(const Array &other) const {
            if (Size == other.Size) {
                for (size_t i = 0; i < Size; ++i) {
                    if (Items[i] != other.Items[i]) {
                        return false;
                    }
                }
                return true;
            } else {
                return false;
            }
        }

        const bool operator!=(const Array &other) const {
            if (Size == other.Size) {
                for (size_t i = 0; i < Size; ++i) {
                    if (Items[i] != other.Items[i]) {
                        return true;
                    }
                }
                return false;
            } else {
                return true;
            }
        }

        Array &operator=(const Array &other) {
            Items = new T[Size = other.Size];
            for (size_t i = 0; i < Size; ++i) {
                Items[i] = other.Items[i];
            }
            return *this;
        }

        size_t size() const {
            return Size;
        }
    };

}
#endif //CPP_OOP_ARRAY_HXX
