#include <gtest/gtest.h>

TEST(VectorT, copy) {
    VectorT<double> temp(2, 1);
    VectorT<int> temp2(temp);
    ASSERT_EQ(temp2, temp);
    ASSERT_EQ(VectorT<int>(2, 1) = temp, temp);
    ASSERT_EQ(VectorT<int>(2, 1) = VectorT<int>(10), VectorT<int>(10));
}

TEST(VectorT, eq_nonEq) {
    VectorT<int> cv(2, 1);
    VectorT<double> cvw(2);
    ASSERT_TRUE(cvw == VectorT<double>(45));
    ASSERT_TRUE(cvw == VectorT<double>(45, 0));
    ASSERT_TRUE(VectorT<double>(3, 0) == VectorT<double>(4, 0));

    ASSERT_FALSE(cvw != VectorT<double>(45, 0));
    ASSERT_FALSE(cvw != VectorT<double>(45));
    ASSERT_FALSE(VectorT<double>(3, 0) != VectorT<double>(4, 0));

    ASSERT_TRUE(cv == cv);
    ASSERT_FALSE(cv != cv);
    ASSERT_TRUE(cv != VectorT<int>());
    ASSERT_FALSE(cv == VectorT<int>());
    ASSERT_TRUE(VectorT<int>(10) == VectorT<int>(20));
    ASSERT_FALSE(VectorT<int>(10) != VectorT<int>(3));

}

TEST(VectorT, getByIndex) {
    VectorT<int> temp(3, 2);
    int tempi = 2;
    ASSERT_EQ(temp[0], tempi);
    temp[0] += 1;
    ++tempi;
    ASSERT_EQ(temp[0], tempi);
    ASSERT_EQ(VectorT<int>(10)[2], 0);
}
