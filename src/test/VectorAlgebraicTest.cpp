
#include <gtest/gtest.h>
#include "OOP Examples/VectorAlgebraic.h"

using namespace cw;

TEST(VectorAlgebraic, module) {
    ASSERT_THROW(VectorAlgebraic<>().module(), std::domain_error);

    ASSERT_EQ(VectorAlgebraic<>(2).module(), 0);
    ASSERT_EQ(VectorAlgebraic<>(2, 2).module(), sqrt(8));
}

TEST(VectorAlgebraic, vectorSkalar) {
    ASSERT_THROW(VectorAlgebraic<>(2) / 0, std::domain_error);

    ASSERT_THROW(VectorAlgebraic<>() * 2, std::domain_error);
    ASSERT_EQ(VectorAlgebraic<>(2) * 2, VectorAlgebraic(2));
    ASSERT_EQ(VectorAlgebraic<>(2, 1) * 3, VectorAlgebraic<>(2, 3));

    ASSERT_THROW(2.0 * VectorAlgebraic<>(), std::domain_error);
    ASSERT_EQ(2.0 * VectorAlgebraic(2), VectorAlgebraic(2));
    ASSERT_EQ(2.0 * VectorAlgebraic<>(2, 1), VectorAlgebraic<>(2, 2));

    ASSERT_THROW(VectorAlgebraic<>() / 2, std::domain_error);
    ASSERT_EQ(VectorAlgebraic<>(2) / 2, VectorAlgebraic(2));
    ASSERT_EQ(VectorAlgebraic<>(2, 3) / 3, VectorAlgebraic<>(2, 1));

}

TEST(VectorAlgebraic, scalar_multiplication) {
    ASSERT_THROW(VectorAlgebraic<>() * VectorAlgebraic<>(), std::domain_error);

    ASSERT_EQ(VectorAlgebraic<>(10) * VectorAlgebraic<>(11), 0);

    ASSERT_EQ(VectorAlgebraic<>(10, 2) * VectorAlgebraic(9), 0);
    ASSERT_EQ(VectorAlgebraic<>(10) * VectorAlgebraic<>(9, 2), 0);
    ASSERT_EQ(VectorAlgebraic<>(5, 2) * VectorAlgebraic<>(4, 3), 24);

    ASSERT_EQ(VectorAlgebraic<>(10, 2) * VectorAlgebraic<>(11), 0);
    ASSERT_EQ(VectorAlgebraic<>(10) * VectorAlgebraic<>(11, 2), 0);
    ASSERT_EQ(VectorAlgebraic<>(4, 3) * VectorAlgebraic<>(5, 2), 24);

    ASSERT_EQ(VectorAlgebraic<>(10) * VectorAlgebraic<>(10, 3), 0);
    ASSERT_EQ(VectorAlgebraic<>(10, 4) * VectorAlgebraic<>(10), 0);
    ASSERT_EQ(VectorAlgebraic<>(5, 2) * VectorAlgebraic<>(5, 4), 40);
    VectorAlgebraic<> temp(5, 2);
    ASSERT_EQ(temp * temp, 20);

}

TEST(VectorAlgebraic, Unary) {
    ASSERT_THROW(-VectorAlgebraic<>(), std::domain_error);
    ASSERT_THROW(+VectorAlgebraic<>(), std::domain_error);

    ASSERT_EQ(-VectorAlgebraic<>(2), VectorAlgebraic<>(2, 0));
    ASSERT_EQ(+VectorAlgebraic<>(2), VectorAlgebraic<>(2, 0));

    ASSERT_EQ(-VectorAlgebraic<>(2, 1), VectorAlgebraic<>(2, -1));
    ASSERT_EQ(+VectorAlgebraic<>(2, 1), VectorAlgebraic<>(2, 1));

    VectorAlgebraic<> temp(6, 2);
    VectorAlgebraic<> tmep2(5, 2);
    VectorAlgebraic<> temp3(5, 2);
    temp[5] = 0;

    ASSERT_EQ(tmep2++, temp3);
    ASSERT_EQ(tmep2, temp);
    temp += VectorAlgebraic<>(7, 0);
    ASSERT_EQ(++tmep2, temp);

    ASSERT_EQ(temp3--, VectorAlgebraic<>(5, 2));
    temp = VectorAlgebraic<>(4, 2);
    ASSERT_EQ(temp3, temp);
    ASSERT_EQ(--temp3, VectorAlgebraic<>(3, 2));
}

TEST(VectorAlgebraic, addition) {
    ASSERT_THROW(VectorAlgebraic<>() + VectorAlgebraic<>(), std::domain_error);

    ASSERT_EQ(VectorAlgebraic<>(10) + VectorAlgebraic<>(11), VectorAlgebraic<>(11));
    ASSERT_EQ(VectorAlgebraic<>(10) + VectorAlgebraic<>(10), VectorAlgebraic<>(10));
    ASSERT_EQ(VectorAlgebraic<>(10) + VectorAlgebraic<>(9), VectorAlgebraic<>(10));
    VectorAlgebraic<> temp(11, 2);
    temp[10] = 0;
    ASSERT_EQ(VectorAlgebraic<>(11) + VectorAlgebraic<>(10, 2), temp);
    ASSERT_EQ(VectorAlgebraic<>(11, 2) + VectorAlgebraic<>(10), VectorAlgebraic<>(11, 2));
    ASSERT_EQ(VectorAlgebraic<>(11, 0) + VectorAlgebraic<>(10, 2), temp);

    ASSERT_EQ(VectorAlgebraic<>(10, 2) + VectorAlgebraic<>(11), temp);
    ASSERT_EQ(VectorAlgebraic<>(10) + VectorAlgebraic<>(11, 2), VectorAlgebraic<>(11, 2));
    ASSERT_EQ(VectorAlgebraic<>(10, 2) + VectorAlgebraic<>(11, 0), temp);

    ASSERT_EQ(VectorAlgebraic<>(10, 2) + VectorAlgebraic<>(10, 2), VectorAlgebraic<>(10, 4));
    ASSERT_EQ(VectorAlgebraic<>(10) + VectorAlgebraic<>(10, 2), VectorAlgebraic<>(10, 2));
    ASSERT_EQ(VectorAlgebraic<>(10, 2) + VectorAlgebraic<>(10), VectorAlgebraic<>(10, 2));


}

TEST(VectorAlgebraic, subtraction) {
    ASSERT_THROW(VectorAlgebraic<>() - VectorAlgebraic<>(), std::domain_error);

    ASSERT_EQ(VectorAlgebraic<>(10) - VectorAlgebraic<>(11), VectorAlgebraic<>(11));
    ASSERT_EQ(VectorAlgebraic<>(10) - VectorAlgebraic<>(10), VectorAlgebraic<>(10));
    ASSERT_EQ(VectorAlgebraic<>(10) - VectorAlgebraic<>(9), VectorAlgebraic<>(10));
    VectorAlgebraic<> temp(11, -2);
    temp[10] = 0;
    ASSERT_EQ(VectorAlgebraic<>(11) - VectorAlgebraic<>(10, 2), temp);
    ASSERT_EQ(VectorAlgebraic<>(11, 2) - VectorAlgebraic<>(10), VectorAlgebraic<>(11, 2));
    ASSERT_EQ(VectorAlgebraic<>(11, 0) - VectorAlgebraic<>(10, 2), temp);
    VectorAlgebraic<> temp2(11, 2);
    temp2[10] = 0;
    ASSERT_EQ(VectorAlgebraic<>(10, 2) - VectorAlgebraic<>(11), temp2);
    ASSERT_EQ(VectorAlgebraic<>(10) - VectorAlgebraic<>(11, 2), VectorAlgebraic<>(11, -2));
    ASSERT_EQ(VectorAlgebraic<>(10, 2) - VectorAlgebraic<>(11, 0), temp2);

    ASSERT_EQ(VectorAlgebraic<>(10, 2) - VectorAlgebraic<>(10, 2), VectorAlgebraic<>(10, 0));
    ASSERT_EQ(VectorAlgebraic<>(10) - VectorAlgebraic<>(10, 2), VectorAlgebraic<>(10, -2));
    ASSERT_EQ(VectorAlgebraic<>(10, 2) - VectorAlgebraic<>(10), VectorAlgebraic<>(10, 2));

    ASSERT_EQ(VectorAlgebraic<>(10, -2) - VectorAlgebraic<>(10, -2), VectorAlgebraic<>(10, 0));
}

TEST(VectorAlgebraic, additionInc) {
    ASSERT_THROW(VectorAlgebraic<>() + VectorAlgebraic<>(), std::domain_error);

    ASSERT_EQ(VectorAlgebraic<>(10) += VectorAlgebraic<>(11), VectorAlgebraic<>(11));
    ASSERT_EQ(VectorAlgebraic<>(10) += VectorAlgebraic<>(10), VectorAlgebraic<>(10));
    ASSERT_EQ(VectorAlgebraic<>(10) += VectorAlgebraic<>(9), VectorAlgebraic<>(10));
    VectorAlgebraic<> temp(11, 2);
    temp[10] = 0;
    ASSERT_EQ(VectorAlgebraic<>(11) += VectorAlgebraic<>(10, 2), temp);
    ASSERT_EQ(VectorAlgebraic<>(11, 2) += VectorAlgebraic<>(10), VectorAlgebraic<>(11, 2));
    ASSERT_EQ(VectorAlgebraic<>(11, 0) += VectorAlgebraic<>(10, 2), temp);

    ASSERT_EQ(VectorAlgebraic<>(10, 2) += VectorAlgebraic<>(11), temp);
    ASSERT_EQ(VectorAlgebraic<>(10) += VectorAlgebraic<>(11, 2), VectorAlgebraic<>(11, 2));
    ASSERT_EQ(VectorAlgebraic<>(10, 2) += VectorAlgebraic<>(11, 0), temp);

    ASSERT_EQ(VectorAlgebraic<>(10, 2) += VectorAlgebraic<>(10, 2), VectorAlgebraic<>(10, 4));
    ASSERT_EQ(VectorAlgebraic<>(10) += VectorAlgebraic<>(10, 2), VectorAlgebraic<>(10, 2));
    ASSERT_EQ(VectorAlgebraic<>(10, 2) += VectorAlgebraic<>(10), VectorAlgebraic<>(10, 2));
}

TEST(VectorAlgebraic, subtractionInc) {
    ASSERT_THROW(VectorAlgebraic<>() -= VectorAlgebraic<>(), std::domain_error);

    ASSERT_EQ(VectorAlgebraic<>(10) -= VectorAlgebraic<>(11), VectorAlgebraic<>(11));
    ASSERT_EQ(VectorAlgebraic<>(10) -= VectorAlgebraic<>(10), VectorAlgebraic<>(10));
    ASSERT_EQ(VectorAlgebraic<>(10) -= VectorAlgebraic<>(9), VectorAlgebraic<>(10));
    VectorAlgebraic<> temp(11, -2);
    temp[10] = 0;
    ASSERT_EQ(VectorAlgebraic<>(11) -= VectorAlgebraic<>(10, 2), temp);
    ASSERT_EQ(VectorAlgebraic<>(11, 2) -= VectorAlgebraic<>(10), VectorAlgebraic<>(11, 2));
    ASSERT_EQ(VectorAlgebraic<>(11, 0) -= VectorAlgebraic<>(10, 2), temp);
    VectorAlgebraic<> temp2(11, 2);
    temp2[10] = 0;
    ASSERT_EQ(VectorAlgebraic<>(10, 2) -= VectorAlgebraic<>(11), temp2);
    ASSERT_EQ(VectorAlgebraic<>(10) -= VectorAlgebraic<>(11, 2), VectorAlgebraic<>(11, -2));
    ASSERT_EQ(VectorAlgebraic<>(10, 2) -= VectorAlgebraic<>(11, 0), temp2);

    ASSERT_EQ(VectorAlgebraic<>(10, 2) -= VectorAlgebraic<>(10, 2), VectorAlgebraic<>(10, 0));
    ASSERT_EQ(VectorAlgebraic<>(10) -= VectorAlgebraic<>(10, 2), VectorAlgebraic<>(10, -2));
    ASSERT_EQ(VectorAlgebraic<>(10, 2) -= VectorAlgebraic<>(10), VectorAlgebraic<>(10, 2));

    ASSERT_EQ(VectorAlgebraic<>(10, -2) -= VectorAlgebraic<>(10, -2), VectorAlgebraic<>(10, 0));
}