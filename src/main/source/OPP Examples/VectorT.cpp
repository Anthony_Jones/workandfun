/*
* Copyright © 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef CPP_OOP_VECTOR_CPP
#define CPP_OOP_VECTOR_CPP

#include "OOP Examples/VectorT.h"
#include "../utils/Trim.cpp"
#include <cstdio>
#include <stdexcept>
#include <sstream>

namespace cw {
    template<class T>
    std::string VectorT<T>::ElementSeparator = "\t";

    template<class T>
    size_t VectorT<T>::County = 0;

    template<class T>
    size_t VectorT<T>::size() const {
        return Size;
    }

    template<class T>
    VectorT<T>::VectorT() {
        ++County;
        Size = 0;
        Array = nullptr;
    }

    template<class T>
    [[deprecated]]
    VectorT<T>::VectorT(size_t size): Size(size) {
        ++County;
        Array = new T[Size];
        if (Array == nullptr) {
            throw std::bad_alloc();
        }
    }

    template<class T>
    VectorT<T>::VectorT(size_t size, T number): Size(size) {
        Initialized = true;
        ++County;
        Array = new T[Size];
        if (Array == nullptr) {
            throw std::bad_alloc();
        }
        for (size_t i = 0; i < Size; ++i) {
            Array[i] = number;
        }
    }

    template<class T>
    VectorT<T>::VectorT(const VectorT <T> &vector) {
        Initialized = vector.Initialized;
        ++County;
        if (vector.size() != 0) {
            Array = new T[Size = vector.size()];
            if (Initialized) {
                for (size_t i = 0; i < Size; ++i) {
                    Array[i] = vector.Array[i];
                }
            }
        } else {
            Array = nullptr;
            Size = 0;
        }
    }

    template<class T>
    VectorT<T>::~VectorT() {
        --County;
        delete[] Array;
    }

    template<class T>
    VectorT <T> &VectorT<T>::operator=(const VectorT <T> &vector) {
        if (vector.Initialized) {
            Initialized = true;
        } else if (this->Initialized and !vector.Initialized) {
            Initialized = false;
        }
        if (this->size() != 0 and vector.size() != 0) {
            if (Size != vector.size()) {
                delete[] Array;
                Array = new T[Size = vector.size()];
            }
            if (vector.Initialized) {
                for (size_t i = 0; i < Size; ++i) {
                    Array[i] = vector.Array[i];
                }
            }
        } else if (Size == 0 and vector.size() != 0) {
            Array = new T[Size = vector.size()];
            if (this->Initialized) {
                for (size_t i = 0; i < Size; ++i) {
                    Array[i] = vector.Array[i];
                }
            }
        } else if (Size != 0 && vector.size() == 0) {
            delete[] Array;
        }
        return *this;
    }

    template<class T>
    const bool VectorT<T>::operator==(const VectorT <T> &vector) const {
        if (!this->Initialized and !vector.Initialized) {
            return true;
        }
        if (this->Initialized and !vector.Initialized) {
            for (size_t i = 0; i < Size; ++i) {
                if (Array[i] != T()) {
                    return false;
                }
            }
        } else if (vector.Initialized and !this->Initialized) {
            for (size_t i = 0; i < Size; ++i) {
                if (Array[i] != T()) {
                    return true;
                }
            }
        } else {
            if (Size != vector.Size) {
                size_t temp;
                (Size > vector.Size) ? temp = Size : temp = vector.Size;
                for (size_t i = 0; i < temp - 1; ++i) {
                    if (Array[i] != T() or vector.Array[i] != T()) {
                        return false;
                    }
                }
                if (temp == Size) return Array[temp] == T();
                else return vector.Array[temp] == T();
            } else {
                for (size_t i = 0; i < Size; ++i) {
                    if (Array[i] != vector.Array[i]) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    template<class T>
    const bool VectorT<T>::operator!=(const VectorT <T> &vector) const {
        if (!this->Initialized and !vector.Initialized) {
            return false;
        }
        if (this->Initialized and !vector.Initialized) {
            for (size_t i = 0; i < Size; ++i) {
                if (Array[i] != T()) {
                    return true;
                }
            }
        } else if (vector.Initialized and !this->Initialized) {
            for (size_t i = 0; i < Size; ++i) {
                if (Array[i] != T()) {
                    return false;
                }
            }
        } else {
            if (Size != vector.Size) {
                size_t temp;
                (Size > vector.Size) ? temp = Size : temp = vector.Size;
                for (size_t i = 0; i < temp - 1; ++i) {
                    if (Array[i] != T() or vector.Array[i] != T()) {
                        return true;
                    }
                }
                if (temp == Size) return Array[temp] != T();
                else return vector.Array[temp] != T();
            } else {
                for (size_t i = 0; i < Size; ++i) {
                    if (Array[i] == vector.Array[i]) {
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }

    template<class T>
    T &VectorT<T>::operator[](long long index) {
        if (index < 0) {
            throw std::out_of_range("Index is out of range");
        }
        auto tempIndex = (VectorT<T>::size_type) index;
        if (Size == 0) {
            throw std::domain_error("Degenerated vectors don't have coordinates");
        } else if (tempIndex >= Size) {
            throw std::out_of_range("Index is out of range");
        }
        if (this->Initialized) {
            return Array[index];
        } else {
            Initialized = true;
            return (Array[index] = T());
        }
    }


    template<class T>
    std::ostream &operator<<(std::ostream &os, const VectorT <T> &vector) {
        if (vector.size() != 0) {
            os << "(";
            for (size_t i = 0; i < vector.Size; ++i) {
                os << vector.Array[i];
                if (i != vector.size() - 1) {
                    std::cout << VectorT<T>::ElementSeparator;
                }
            }
            os << ")";
        }
        return os;
    }


    template<class T>
    std::istream &operator>>(std::istream &is, VectorT <T> &vector) {
        std::string string1;
        std::getline(is, string1);                            //getting string which initializer vector
        std::string string2;
        std::unique_copy(string1.begin(), string1.end(), std::back_inserter(string2), // deleting repeating whitespaces
                         [](char c1, char c2) { return isspace(c1) and isspace(c2); });

        string2 = trim(
                string2);                                         //deleting whitespaces at begin, and end of string
        size_t size = 1;
        if (string2.empty()) {
            delete[] vector.Array;
            vector.Size = 0;
            return is;
        }
        for (char i : string2) {
            if (isspace(i)) {
                ++size;
            }
        }
        string1.erase();
        if (size != vector.size()) {
            delete[] vector.Array;
            vector.Array = new T[vector.Size = size];
        }
        std::istringstream istringstream1(string2);
        for (size_t j = 0; j < vector.Size; ++j) {
            istringstream1 >> vector.Array[j];
        }
        vector.Initialized = true;
        return is;
    }

    template<typename T>
    void *VectorT<T>::operator new[](size_t lenght) {
        return malloc(lenght * sizeof(VectorT < T > ));
    }


    template<class T>
    typename VectorT<T>::iterator VectorT<T>::begin() {
        return iterator(this->Array);
    }

    template<class T>
    typename VectorT<T>::iterator VectorT<T>::end() {
        return iterator(this->Array + Size);
    }

    template<class T>
    typename VectorT<T>::const_iterator VectorT<T>::cbegin() const {
        return const_iterator(this->Array);
    }

    template<class T>
    typename VectorT<T>::const_iterator VectorT<T>::cend() const {
        return const_iterator(this->Array + Size);
    }

    template<typename T>
    void VectorT<T>::operator delete[](void *p) {
        free(p);
    }
}
#endif //CPP_OOP_VECTOR_CPP

