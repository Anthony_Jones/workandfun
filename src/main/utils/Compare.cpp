/*
* Copyright © 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once
#ifndef COMPARE_ITEMS_CPP
namespace cw::utils {
    size_t CompareCount = 0;// TODO DELETE
/**
 * Compare of two Values of the same type. If one equals another return 0.
 * If one greater than another return 1.
 * If one less than another return -1.
 * @tparam T Type of Values
 * @param [in] one value
 * @param [in] another value
 * @return int
 */
    template<class T>
    int Compare(const T &one, const T &another) {
        ++CompareCount;// TODO DELETE
        if (one == another) {
            return 0;
        } else if (one < another) {
            return -1;
        } else {
            return 1;
        }
    }

/**
 * Inverted compare of two Values of the same type.
 * Result:
 * If one equals another return 0.
 * If one greater than another return -1.
 * If one less than another return 1.
 * @tparam T Type of Values
 * @param [in] one value
 * @param [in] another value
 * @return int
 */
    template<class T>
    int InvertedCompare(const T &one, const T &another) {
        ++CompareCount;// TODO DELETE
        if (one == another) {
            return 0;
        } else if (one < another) {
            return 1;
        } else {
            return -1;
        }
    }
}

#endif //COMPARE_ITEMS_CPP