/*
* Copyright © 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#include <cmath>
#include <iostream>

#include "../source/Numerical methods/RungeCutta.cpp"

using std::cout;
using std::endl;

using namespace cw;

typedef double CType;

const CType rightPart(const CType &x, const CType &y) {
    return x / CType(2) + y / CType(2) * pow(M_E, -y);
}

const CType rightPart1(const CType &x, const CType &y) {
    return cos(x);
}

CType solu(const CType &x) {
    return sin(x) + 1;
}

int main() {
    CType x0 = M_PI_2;
    CType xN = 3;

    CType y0 = 2;
    CType epsilon = 10e-6;

    size_t initialPointsQuantity = 20;

    size_t pointsQuantity = initialPointsQuantity;
    size_t pointsQuantityDouble = pointsQuantity * 2;
    CType *solution = nullptr;
    CType *solutionNew = nullptr;
    do {
        solution = RungeCutta(x0, y0, xN, initialPointsQuantity, pointsQuantity, rightPart1);
        solutionNew = RungeCutta(x0, y0, xN, initialPointsQuantity, pointsQuantityDouble, rightPart1);
        pointsQuantity = pointsQuantityDouble;
        pointsQuantityDouble *= 2;
    } while (fabs(solution[initialPointsQuantity - 1] - solutionNew[initialPointsQuantity - 1]) >= epsilon);
    cout << "Test Example y' = cos(x), y(Pi/2) = 2" << endl;
    cout << "Numerical solution at points: " << endl;
    for (size_t i = 0; i < initialPointsQuantity; ++i) {
        cout << solutionNew[i] << " ";
    }
    cout << endl;
    CType temp = x0;
    cout << "Analytical solution ay points: " << endl;
    for (size_t j = 0; j < initialPointsQuantity; ++j) {
        cout << solu(temp) << " ";
        temp += (xN - x0) / initialPointsQuantity;
    }
    cout << endl;
    x0 = 2;
    xN = 3;
    y0 = 3;
    initialPointsQuantity = 10;

    pointsQuantity = initialPointsQuantity;
    pointsQuantityDouble = pointsQuantity * 2;
    do {
        solution = RungeCutta(x0, y0, xN, initialPointsQuantity, pointsQuantity, rightPart);
        solutionNew = RungeCutta(x0, y0, xN, initialPointsQuantity, pointsQuantityDouble, rightPart);
        pointsQuantity = pointsQuantityDouble;
        pointsQuantityDouble *= 2;
    } while (fabs(solution[initialPointsQuantity - 1] - solutionNew[initialPointsQuantity - 1]) >= epsilon);
    cout << "y' = x / CType(2) + y / CType(2) * pow(M_E, -y), y(2) = 3" << endl;
    for (size_t i = 0; i < initialPointsQuantity; ++i) {
        cout << solutionNew[i] << " ";
    }
    cout << endl;

    delete[] solution;
    delete[] solutionNew;
}