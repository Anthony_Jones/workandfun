#include <gtest/gtest.h>
#include <Structs/MatrixRectangle.h>
#include <Structs/MatrixSquare.h>

using namespace cw;

TEST(MatrixRectangle, sum) {
    ASSERT_EQ(MatrixRectangle<double>(1, 1, 2) + MatrixRectangle<double>(1, 1, 2), MatrixRectangle<double>(1, 1, 4));
    ASSERT_EQ(MatrixRectangle<>(2, 3, 4) + MatrixRectangle<>(2, 3, 36), MatrixRectangle<>(2, 3, 40));
    ASSERT_EQ(MatrixRectangle<>(2, 3, 4) + MatrixRectangle<>(2, 3, -36), MatrixRectangle<>(2, 3, -32));

    ASSERT_THROW(MatrixRectangle<double>(1, 2) + MatrixRectangle<double>(2, 3), std::domain_error);
    ASSERT_THROW(MatrixRectangle<double>() + MatrixRectangle<double>(), std::domain_error);

    ASSERT_EQ(MatrixRectangle<double>(1, 1, 2) += MatrixRectangle<double>(1, 1, 2), MatrixRectangle<double>(1, 1, 4));
    ASSERT_EQ(MatrixRectangle<>(2, 3, 4) += MatrixRectangle<>(2, 3, 36), MatrixRectangle<>(2, 3, 40));
    ASSERT_EQ(MatrixRectangle<>(2, 3, 4) += MatrixRectangle<>(2, 3, -36), MatrixRectangle<>(2, 3, -32));

    ASSERT_THROW(MatrixRectangle<double>(1, 2) += MatrixRectangle<double>(2, 3), std::domain_error);
    ASSERT_THROW(MatrixRectangle<double>() += MatrixRectangle<double>(), std::domain_error);
}

TEST(MatrixRectangle, sub) {
    ASSERT_EQ(MatrixRectangle<double>(1, 1, 2) - MatrixRectangle<double>(1, 1, 2), MatrixRectangle<double>(1, 1, 0));
    ASSERT_EQ(MatrixRectangle<>(2, 3, 4) - MatrixRectangle<>(2, 3, 36), MatrixRectangle<>(2, 3, -32));
    ASSERT_EQ(MatrixRectangle<>(2, 3, 4) - MatrixRectangle<>(2, 3, -36), MatrixRectangle<>(2, 3, 40));

    ASSERT_THROW(MatrixRectangle<double>(1, 2) - MatrixRectangle<double>(2, 3), std::domain_error);
    ASSERT_THROW(MatrixRectangle<double>() - MatrixRectangle<double>(), std::domain_error);

    ASSERT_EQ(MatrixRectangle<double>(1, 1, 2) -= MatrixRectangle<double>(1, 1, 2), MatrixRectangle<double>(1, 1, 0));
    ASSERT_EQ(MatrixRectangle<>(2, 3, 4) -= MatrixRectangle<>(2, 3, 36), MatrixRectangle<>(2, 3, -32));
    ASSERT_EQ(MatrixRectangle<>(2, 3, 4) -= MatrixRectangle<>(2, 3, -36), MatrixRectangle<>(2, 3, 40));

    ASSERT_THROW(MatrixRectangle<double>(1, 2) -= MatrixRectangle<double>(2, 3), std::domain_error);
    ASSERT_THROW(MatrixRectangle<double>() -= MatrixRectangle<double>(), std::domain_error);
}

TEST(MatrixRectangle, scalar) {
    MatrixRectangle<> temp(4, 4, 29);
    ASSERT_EQ(temp * 2, MatrixRectangle<>(4, 4, 58));
    ASSERT_EQ(temp *= 2, MatrixRectangle<>(4, 4, 58));
    ASSERT_EQ(temp * 1, temp);

    ASSERT_EQ(temp / 0.5, temp * 2);
    ASSERT_EQ(temp /= 0.5, temp * 2);
    ASSERT_EQ(temp / 1, temp);

    ASSERT_THROW(MatrixRectangle<>() * 2, std::domain_error);
    ASSERT_THROW(MatrixRectangle<>() / 2, std::domain_error);

    ASSERT_THROW(MatrixRectangle<>() *= 2, std::domain_error);
    ASSERT_THROW(MatrixRectangle<>() /= 2, std::domain_error);

    ASSERT_THROW(temp / 0, std::domain_error);
    ASSERT_THROW(temp /= 0, std::domain_error);

}

TEST(MatrixRectangle, multiplication) {
    MatrixRectangle<double> E(3, 3);
    for (size_t i = 0; i < E.getRows(); ++i) {
        for (size_t j = 0; j < E.getColumns(); ++j) {
            if (i == j) E(i, j) = 1;
            else E(i, j) = 0;
        }
    }
    MatrixRectangle<double> temp(3, 3);
    for (size_t i = 0; i < temp.getRows(); ++i) {
        for (size_t j = 0; j < temp.getColumns(); ++j) {
            temp(i, j) = i * j;
        }
    }
    ASSERT_EQ(temp * E, temp);
    ASSERT_EQ(E * temp, temp);
    ASSERT_EQ(E * E, E);

    ASSERT_EQ(temp *= E, temp);
    ASSERT_EQ(E *= temp, temp);
    ASSERT_EQ(E *= E, E);

    ASSERT_THROW(E * MatrixRectangle<>(), std::domain_error);
    ASSERT_THROW(E * MatrixRectangle<>(1, 1), std::domain_error);

    ASSERT_THROW(E *= MatrixRectangle<>(), std::domain_error);
    ASSERT_THROW(E *= MatrixRectangle<>(1, 1), std::domain_error);
}

TEST(MatrixSquare, determinant) {
    MatrixSquare<double> E(3);
    for (size_t i = 0; i < E.getRows(); ++i) {
        for (size_t j = 0; j < E.getColumns(); ++j) {
            if (i == j) E(i, j) = 1;
            else E(i, j) = 0;
        }
    }
    ASSERT_EQ(E.determinant(), 1);
    MatrixSquare<double> temp(2);
    temp(0, 0) = 5;
    temp(0, 1) = 6;
    temp(1, 0) = 7;
    temp(1, 1) = 8;
    cout << temp.determinant();
    ASSERT_EQ(MatrixSquare<double>(100, 500.0).determinant(), 0);

    ASSERT_THROW(MatrixSquare<>().determinant(), std::domain_error);
}

TEST(MatrixSquare, inverse) {
    MatrixSquare<double> E(3);
    for (size_t i = 0; i < E.getRows(); ++i) {
        for (size_t j = 0; j < E.getColumns(); ++j) {
            if (i == j) E(i, j) = 1;
            else E(i, j) = 0;
        }
    }
    MatrixSquare<> temp(3);
    temp(0, 0) = 1;
    temp(0, 1) = 2;
    temp(0, 2) = 3;

    temp(1, 0) = 4;
    temp(1, 1) = 5;
    temp(1, 2) = 6;

    temp(2, 0) = 10;
    temp(2, 1) = 3;
    temp(2, 2) = 1;

    ASSERT_EQ(E * E.inverse(), E);
    ASSERT_EQ(temp * temp.inverse(), E);

    ASSERT_THROW(MatrixSquare<>().inverse(), std::domain_error);
    ASSERT_THROW(MatrixSquare<>(2, 0).inverse(), std::domain_error);
}

TEST(MatrixSquare, division) {
    MatrixSquare<double> E(3);
    for (size_t i = 0; i < E.getRows(); ++i) {
        for (size_t j = 0; j < E.getColumns(); ++j) {
            if (i == j) E(i, j) = 1;
            else E(i, j) = 0;
        }
    }
    MatrixSquare<> temp(3);
    temp(0, 0) = 1;
    temp(0, 1) = 2;
    temp(0, 2) = 3;

    temp(1, 0) = 4;
    temp(1, 1) = 5;
    temp(1, 2) = 6;

    temp(2, 0) = 10;
    temp(2, 1) = 3;
    temp(2, 2) = 1;

    ASSERT_EQ(E / E, E);
    ASSERT_EQ(temp / temp, E);

    ASSERT_EQ(E /= E, E);
    ASSERT_EQ(temp /= temp, E);

    ASSERT_THROW(MatrixSquare<>() / MatrixSquare<>(), std::domain_error);
    ASSERT_THROW(MatrixSquare<>(2) / MatrixSquare<>(2, 4), std::domain_error);
    ASSERT_THROW(MatrixSquare<>(2) / MatrixSquare<>(3, 2), std::domain_error);
    ASSERT_THROW(MatrixSquare<>() /= MatrixSquare<>(), std::domain_error);
    ASSERT_THROW(MatrixSquare<>(2) /= MatrixSquare<>(2, 4), std::domain_error);
    ASSERT_THROW(MatrixSquare<>(2) /= MatrixSquare<>(3, 2), std::domain_error);
}

TEST(MatrixRectangle, transpose) {
    MatrixRectangle<double> E(3, 3);
    for (size_t i = 0; i < E.getRows(); ++i) {
        for (size_t j = 0; j < E.getColumns(); ++j) {
            if (i == j) E(i, j) = 1;
            else E(i, j) = 0;
        }
    }

    ASSERT_EQ(E.transpose(), E);
}
