/*
* Copyright © 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once
#ifndef ASD_SORTS_CPP
#define ASD_SORTS_CPP

#include <cstddef>

#include "../../utils/SortUtils.hpp"
#include "../../utils/Compare.cpp"
#include "../../utils/ArrayUtils.cpp"

namespace cw {
    using utils::Compare;
    using utils::Swap;
    using utils::Merge;
    using utils::Partition;

    template<class T>
    void BubbleSort(T &A, bool IncFlag = true) {
        ssize_t i;
        ssize_t j;
        for (i = A.size() - 1; i >= 0; --i) {
            for (j = 0; j < i; ++j) {
                if ((Compare(A[j], A[j + 1]) <= 0) ^ IncFlag)
                    Swap(A, j, j + 1);
            }
        }
        //if (!IncFlag) A.InvOrder();
    }

    template<class T>
    void InsertionSort(T &A, bool IncFlag = true) {
        int i, j;
        for (i = 0; i < static_cast<int>(A.size() - 1); ++i) {
            j = i;
            while ((Compare(A[j], A[j + 1]) <= 0) ^ IncFlag) {
                Swap(A, j, j + 1);
                --j;
                if (j < 0) break;
            }
        }
    }

    template<class T, class E>
    void InsertionSort(T &Array, size_t left, size_t right, int (*Compare)(const E &, const E &)) {
        size_t i, j;
        for (i = left; i < right; ++i) {
            j = i;
            while (Compare(Array[j], Array[j + 1]) >= 0) {
                Swap(Array, j, j + 1);
                ++j;
            }
        }
//    if (type) Array.InvOrder();
    }

    template<class T>
    void ChooseElementSort(T &Array, bool type = true) {
        size_t ElementPos;
        for (size_t i = 0; i < Array.size() - 1; ++i) {
            ElementPos = i;
            for (size_t j = i + 1; j < Array.size(); ++j) {
                if ((Compare(Array[ElementPos], Array[j]) <= 0) ^ type) {
                    ElementPos = j;
                }
            }
            Swap(Array, i, ElementPos);
        }
    }

    template<class T, class I, class E>
    void
    MergeSort(T &Array, I left, I right, int(*compare)(const E &, const E &) = Compare) { //TODO Do Merge Sort
        if (left < right) {
            I center = (right + left) / 2;
            MergeSort(Array, left, center, compare);
            MergeSort(Array, center + 1, right, compare);
            Merge(Array, left, center, right, compare);
        }
    }

    template<class T, class I, class E>
    void HeapSort(const T &Array, const I &heap_size, int(*compare)(const E &, const E &) = Compare) { //TODO Heap Sort
        utils::BuildHeap(Array, heap_size, compare);
        for (I i = heap_size; i >= 2; --i) {
            Swap(Array, I(), i - 1);
            utils::Heapify(Array, I(), i - 1, compare);
        }
    }


    template<class T, class E>
    void QuickSortWithComparator(T &Array, size_t left, size_t right,
                                 int(*Comp)(const E &, const E &)) {
        if (left < right) {
            size_t sep = PartitionWithComp(Array, left, right, Comp);
            QuickSortWithComparator(Array, left, sep - 1, Comp);
            QuickSortWithComparator(Array, sep + 1, right, Comp);
        }
    }

    template<class T, class I>
    void QuickSort(T &Array, I left, I right, bool type = true) { // TODO InvCompare
        if (left < right) {
            I sep = Partition(Array, left, right, type);
            QuickSort(Array, left, sep - 1, type);
            QuickSort(Array, sep + 1, right, type);
        }
    }

    template<class T, class E>
    void ExtQuickSortWithComparator1(T &Array, size_t left, size_t right,
                                     int (*compare)(const E &, const E &)) { // TODO InvCompare
        if (right - left < 10) {
            InsertionSort(Array, left, right, compare);
            return;
        }
        size_t med = selectPivot(Array, left, right, compare);
        Swap(Array, med, right);
        size_t sep = PartitionWithComp(Array, left, right, compare);
        ExtQuickSortWithComparator1(Array, left, sep - 1, compare);
        ExtQuickSortWithComparator1(Array, sep + 1, right, compare);
    }

    template<class T, class E>
    void ExtQuickSortWithComparator2(T &Array, size_t left, size_t right,
                                     int (*compare)(const E &, const E &)) { // TODO InvCompare
        if (right - left < 10) {
            InsertionSort(Array, left, right, compare);
            return;
        }
        E med = selectPivot(Array, left, right);
        Swap(Array, med, right);
        E sep = PartitionWithComp(Array, left, right, compare);
        ExtQuickSortWithComparator2(Array, left, sep - 1, compare);
        ExtQuickSortWithComparator2(Array, sep + 1, right, compare);
    }

}
#endif//ASD_SORTS_CPP