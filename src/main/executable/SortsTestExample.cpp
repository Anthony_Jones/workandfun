/*
* Copyright © 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#include <iostream>

using std::cout;
using std::endl;

#include "../../source/ASD/Sorts/Sorts.cpp"
#include "../utils/ArrayUtils.cpp"
#include "../utils/Compare.cpp"

#include <Structs/Array.hxx>

using namespace cw;
using utils::shuffle;

int main() {
    Array<double> A(10);
    for (size_t j = 1; j <= 10; ++j) {
        A[j - 1] = j;
    }
    shuffle(A, 10, random::lcg::rand);
    cout << "Not sorted " << endl;
    for (size_t i = 0; i < A.size(); ++i) {
        cout << A[i] << " ";
    }
    cout << endl;
    cout << "Sorted : " << endl;
    BubbleSort(A);
    for (size_t i = 0; i < A.size(); ++i) {
        cout << A[i] << " ";
    }
    cout << endl;
    shuffle(A, 10, random::lcg::rand);
    InsertionSort(A);
    for (size_t i = 0; i < A.size(); ++i) {
        cout << A[i] << " ";
    }
    cout << endl;
    shuffle(A, 10, random::lcg::rand);
    ChooseElementSort(A);
    for (size_t i = 0; i < A.size(); ++i) {
        cout << A[i] << " ";
    }
    cout << endl;
}