/*
* Copyright © 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef CPP_EULER_METHOD
#define CPP_EULER_METHOD

/**
 * Euler Method for Solving 1st Order Ordinal Differential Equations at one point with Cauchy problem.
 * The solution is an approximation using tangents
 * @tparam T Computation Type
 * @param ODE Right part of ODE
 * @param Cauchy Problem
 * @param a Begin of segment
 * @param b End of segment
 * @param step
 * @return Value of Y at this Segment with Cauchy problem
 */
template<typename T>
T EulerMethod(T(*ODE)(const T &x, const T &y), const T &Cauchy, const T &a, const T &b, const T &step) { //TODO TEST
    T steps = a; // Var for containing step
    T answer = Cauchy;
    T deriviative; // Tangents
    T deriviativeMulStep; //Tangents * Step
    while (steps < b) {
        deriviative = ODE(steps, answer);
        deriviativeMulStep = deriviative * step;
        answer += deriviativeMulStep;
        steps += step;
    }
    return answer;
}

#endif //CPP_EULER_METHOD