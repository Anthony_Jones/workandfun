/*
* Copyright © 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#include "../source/Numerical methods/LinerAlgebra.cpp"
#include <random>
using std::cout;
using std::cin;
using std::endl;
using std::vector;

using namespace cw;

int main() {
    MatrixSquare<double> A(3);
    A(0, 0) = 1;
    A(0, 1) = 0;
    A(0, 2) = -2;
    A(1, 0) = 2;
    A(1, 1) = 3;
    A(1, 2) = -1;
    A(2, 0) = 0;
    A(2, 1) = 0;
    A(2, 2) = 2;
    VectorAlgebraic<double> C0(3);
    C0[0] = 1;
    C0[1] = 0;
    C0[2] = 1;
    VectorAlgebraic<VectorAlgebraic<double>> vectorsC(A.getRows() + 1);
    MatrixSquare<double> CheckDet(C0.size());
    unsigned Seed = 0;
    cout << "Auto Generated System of Vectors: " << endl;
    do {
        if (Seed > 1000) break;
        vectorsC[0] = VectorAlgebraic<double>(C0.size());
        for (size_t m = 0; m < vectorsC[0].size(); ++m) {
            srand(static_cast<unsigned int>(Seed + m));
            vectorsC[0][m] = (static_cast<double>(rand() / 500000000) + 1);
        }
        cout << vectorsC[0] << endl;
        try {
            for (size_t i = 1; i < A.getRows() + 1; ++i) {
                vectorsC[i] = A * vectorsC[i - 1];
                std::cout << vectorsC[i] << std::endl;
            }
        }
        catch (std::domain_error &e) {
            std::cout << std::endl << e.what() << std::endl;
        }
        for (size_t j = 0; j < CheckDet.getRows(); ++j) {
            for (size_t i = 0; i < CheckDet.getColumns(); ++i) {
                CheckDet(j, i) = vectorsC[j][i];
            }
        }
        ++Seed;

    } while (fabs(CheckDet.determinant()) < 1);
    cout << endl;
//    vectorsC[0] = VectorAlgebraic<double>(
//            C0.Size()); // C0
//    for (size_t m = 0; m < vectorsC[0].Size(); ++m) {
//        srand(static_cast<unsigned int>(Seed + m));
//        vectorsC[0][m] = (static_cast<double>(rand() / 1000000000));
//        ++Seed;
//    }
//    cout << vectorsC[0] << endl;
//    try {
//        for (size_t i = 1; i < A.getRows(); ++i) {
//            vectorsC[i] = A * vectorsC[i - 1];
//            std::cout << vectorsC[i] << std::endl;
//        }
//    }
//    catch (std::domain_error &e) {
//        std::cout << std::endl << e.what() << std::endl;
//    }
    auto eigenValues = EigenValues(A, vectorsC, 1e-30);
    cout << "eigen Values: ";
    for (auto i : eigenValues) {
        cout << i << " ";
    }
    cout << endl;
    double sum1 = 0, sum2 = 0;
    for (size_t k = 0; k < A.getRows(); ++k) {
        sum1 += A(k, k);
        sum2 += eigenValues[k / 2];
    }
    cout << "След матрицы: " << sum1 << "  ==   Cумма собсвтенных чисел: " << sum2 << endl;
    auto eigenVectors = EigenVectors(A, vectorsC, 1e-30);
    cout << "Собственные векторы" << endl;
    cout << eigenVectors;
}