/*
* Copyright © 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef CPP_OOP_VECTORALGEBRAICT_H
#define CPP_OOP_VECTORALGEBRAICT_H

#include "VectorT.h"
#include <Structs/MatrixRectangle.h>

#include <iostream>

namespace cw {
    template<class T = double>
    class VectorAlgebraic : public VectorT<T> {
    public:

        VectorAlgebraic<T>();

        explicit VectorAlgebraic<T>(size_t Size);

        VectorAlgebraic<T>(size_t Size, T number);

        VectorAlgebraic<T>(const VectorAlgebraic<T> &vectorAlgebraicT);

        virtual ~VectorAlgebraic();

        const VectorAlgebraic<T> operator-() const;

        const VectorAlgebraic<T> operator+() const;

        const VectorAlgebraic<T> &operator++();

        const VectorAlgebraic<T> operator++(int);

        const VectorAlgebraic<T> &operator--();

        const VectorAlgebraic<T> operator--(int);

        const VectorAlgebraic operator+(const VectorAlgebraic<T> &vectorAlgebraicT) const;

        const VectorAlgebraic operator-(const VectorAlgebraic<T> &vectorAlgebraicT) const;

        T operator*(const VectorAlgebraic<T> &vectorAlgebraicT);

        const VectorAlgebraic<T> &operator+=(const VectorAlgebraic<T> &vectorAlgebraicT);

        const VectorAlgebraic<T> &operator-=(const VectorAlgebraic<T> &vectorAlgebraicT);

        const VectorAlgebraic<T> operator*(T number) const;

        const VectorAlgebraic<T> operator/(T number) const;

        template<class V>
        friend const VectorAlgebraic<V> operator*(V number, const VectorAlgebraic<V> &);

        template<class V>
        friend std::ostream &operator<<(std::ostream &os, const VectorAlgebraic<V> &vector);

        template<class V>
        friend std::istream &operator>>(std::istream &is, VectorAlgebraic<V> &t);


        T module() const;
    };


}
#endif //CPP_OOP_1_VECTORALGEBRAICT_H

#include "../../source/OPP Examples/VectorAlgebraic.cpp"
