/*
* Copyright © 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef _COMPLEX_H
#define _COMPLEX_H

#include <iostream>

class Complex {

protected:

    double Real;
    double Imagine;

public:

    Complex();

    double getReal() const;

    void setReal(double real);

    double getImagine() const;

    void setImagine(double imagine);

    Complex(const double &real, const double &imagine);

    Complex(const Complex &);

    virtual ~Complex();


    const Complex operator+(const double &x) const;

    const Complex operator-(const double &x) const;

    const Complex operator/(const double &x) const;

    const Complex operator*(const double &x) const;

    const Complex &operator+=(const double &x);

    const Complex &operator-=(const double &x);

    const Complex &operator/=(const double &x);

    const Complex &operator*=(const double &x);

    Complex &operator=(const Complex &);

    friend const Complex operator+(const double &x, const Complex &z);

    friend const Complex operator-(const double &x, const Complex &z);

    friend const Complex operator/(const double &x, const Complex &z);

    friend const Complex operator*(const double &x, const Complex &z);

    const Complex operator+(const Complex &z) const;

    const Complex operator-(const Complex &z) const;

    const Complex operator/(const Complex &z) const;

    const Complex operator*(const Complex &z) const;

    const Complex &operator+=(const Complex &x);

    const Complex &operator-=(const Complex &x);

    const Complex &operator/=(const Complex &z);

    const Complex &operator*=(const Complex &x);

    const bool operator!=(const Complex &) const;

    const bool operator==(const Complex &) const;

    void trig() const;

    const Complex *nroot(unsigned n) const;

    const Complex LogZ(const int &k) const;

    const Complex powZ(const Complex &, const int &k) const;

    friend std::ostream &operator<<(std::ostream &, const Complex &complex);

    friend std::istream &operator>>(std::istream &, Complex &complex);
};

#include "../../../../../ClionProjects/CPP_OOP/src/main/source/Complex.cpp"

#endif //COMPLEX_COMPLEX_H
