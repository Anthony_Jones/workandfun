/*
* Copyright © 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef CPP_OOP_POLYNOMIAL_H
#define CPP_OOP_POLYNOMIAL_H

#include <cmath>
#include "VectorT.h"

namespace cw {
    template<typename T>
    class Polynomial : public VectorT<T> {

        size_t Degree{}; // Степень многочлена.
        char Variable;
    public:
        explicit Polynomial(char variable = 'x');

        /**
         *
         * @param degree Степень многочлена
         */
        explicit Polynomial(size_t degree, char variable = 'x');

        /**
         * Notice: Size = Degree + 1
         * @param degree
         * @param number
         */
        Polynomial(size_t degree, T number, char variable = 'x');

        explicit Polynomial(VectorT <T> &vector, char variable = 'x');

        Polynomial(const Polynomial<T> &polynomial);

        /**
         * Находит промежуток (a,b) где содержаться |z| корни многочлена
         * Замечание: Корни следует искать на (a,b)  и (-a,-b)
         * @return VectorT<T> Промежуток
         */
        const VectorT<T> WhereRoots() const;

        const Polynomial operator+(const Polynomial &other) const;

        const Polynomial operator-(const Polynomial &other) const;

        const Polynomial operator*(const Polynomial &other) const;

        Polynomial &operator+=(const Polynomial &other);

        Polynomial &operator-=(const Polynomial &other);

        Polynomial &operator*=(const Polynomial &other);

        Polynomial &operator=(const Polynomial &other);

        T &operator[](size_t index) const {
            if (index < 0) {
                throw std::out_of_range("Index is out of range");
            }
            auto tempIndex = size_t(index);
            if (this->Size == 0) {
                throw std::domain_error("Degenerated vectors don't have coordinates");
            } else if (tempIndex >= this->Size) {
                throw std::out_of_range("Index is out of range");
            }
            if (this->Initialized) {
                return this->Array[index];
            } else {
                return (this->Array[index] = T());
            }
        }

        size_t degree() const {
            return this->Degree;
        }

        const T AtPoint(const T x) const {

            T result = this->Array[0];
            if (this->Degree != 0) {
                T temp = x;
                for (size_t i = 1; i < this->Size; ++i) {
                    result += temp * this->Array[i];
                    temp *= x;
                }
            }
            return result;
        }

        /**
         * Возвращает производную многочлена по её порядковому номеру
         * @param ordinal Порядок производной
         * @return Polynomial Производная изначального многочлена
         */
        Polynomial GetOrdinalDerivative(long long ordinal) const;

        template<class V>
        friend std::ostream &operator<<(std::ostream &out, Polynomial<V> other);

        template<class V>
        friend std::istream &operator>>(std::istream &in, Polynomial<V> &other);

    };
}
namespace cw {
    template<class T>
    std::ostream &operator<<(std::ostream &out, Polynomial<T> polynomial) {
        if (polynomial.Size != 0) {
            for (int i = int(polynomial.Size - 1); i >= 0; --i) {
                if (polynomial[i] != 0) {
                    if (polynomial[i] != 1 and polynomial[i] != -1) {
                        out << polynomial[i];
                    }
                    if (i >= 1) {
                        out << polynomial.Variable;
                    }
                    if (i > 1) {
                        out << '^' << i;
                    }
                }
                if (i != 0) {
                    if (polynomial[i - 1] > 0) {
                        out << '+';
                    }
                }

            }
            return out;
        } else {
            return out;
        }
    }

    template<class T>
    std::istream &operator>>(std::istream &is, Polynomial<T> &other) {
        std::string string1;
        std::getline(is, string1);                            //getting string which initializer vector
        std::string string2;
        std::unique_copy(string1.begin(), string1.end(), std::back_inserter(string2), // deleting repeating whitespaces
                         [](char c1, char c2) { return isspace(c1) and isspace(c2); });

        string2 = trim(
                string2);                                         //deleting whitespaces at begin, and end of string
        size_t size = 1;
        if (string2.empty()) {
            delete[] other.Array;
            other.Size = 0;
            other.Degree = 0;
            return is;
        }
        for (char i : string2) {
            if (isspace(i)) {
                ++size;
            }
        }
        string1.erase();
        if (size != other.size()) {
            delete[] other.Array;
            other.Array = new T[other.Size = size];
            other.Degree = other.Size - 1;
        }
        std::istringstream istringstream1(string2);
        for (int j = other.Size - 1; j >= 0; --j) {
            istringstream1 >> other.Array[j];
        }
        other.Initialized = true;
        return is;
    }

    template<typename T>
    Polynomial<T>::Polynomial(char variable): Degree(0), Variable(variable) {
        this->Initialized = true;
    }

    template<typename T>
    Polynomial<T>::Polynomial(size_t degree,
                              char variable):
            VectorT<T>(degree + 1),
            Degree(degree),
            Variable(variable) {
        this->Initialized = true;
    }

    template<typename T>
    Polynomial<T>::Polynomial(size_t degree,
                              T number,
                              char variable):
            VectorT<T>(degree + 1, number),
            Degree(degree),
            Variable(variable) {
        this->Initialized = true;
    }

    template<typename T>
    Polynomial<T>::Polynomial(VectorT <T> &vector,
                              char variable):
            Degree(vector.size() - 1),
            Variable(variable) {
        this->Initialized = true;
        this->Array = new T[vector.size()];
        this->Size = vector.size();
        for (auto i = static_cast<int>(this->Size - 1); i >= 0; --i) {
            this->Array[i] = vector[this->Size - 1 - i];
        }
    }

    template<typename T>
    Polynomial<T>::Polynomial(const Polynomial<T> &polynomial) {
        this->Initialized = true;
        this->Size = polynomial.Size;
        Variable = polynomial.Variable;
        this->Array = new T[this->Size];
        this->Degree = polynomial.Degree;
        if (polynomial.Size != 0) {
            for (size_t i = 0; i < this->Size; ++i) {
                this->Array[i] = polynomial.Array[i];
            }
        }
    }

    template<typename T>
    const VectorT <T> Polynomial<T>::WhereRoots() const {
        VectorT<T> Result(2);
        T MaxCoefficientFromA1toAN = fabs(this->Array[0]);
        T MaxCoefficientFromA0toAN_1 = fabs(this->Array[1]);
        for (size_t i = 2; i < this->Size - 1; ++i) {
            if (fabs(MaxCoefficientFromA0toAN_1) < fabs(this->Array[i])) {
                MaxCoefficientFromA0toAN_1 = fabs(this->Array[i]);
            }
        }
        for (size_t j = 1; j < this->Size - 2; ++j) {
            if (fabs(MaxCoefficientFromA1toAN) < fabs(this->Array[j])) {
                MaxCoefficientFromA1toAN = fabs(this->Array[j]);
            }
        }
        Result[0] = fabs(this->Array[0]) / (MaxCoefficientFromA0toAN_1 - fabs(this->Array[0]));
        Result[1] = 1 + MaxCoefficientFromA1toAN / fabs(this->Array[this->Size - 1]);
        return Result;
    }

    template<typename T>
    Polynomial<T> Polynomial<T>::GetOrdinalDerivative(long long ordinal) const {
        if (ordinal < 0) {
            throw std::domain_error("Derivative can not be negative");
        }
        auto ordinalT = size_t(ordinal);
        if (ordinalT == 0) {
            return *this;
        }
        if (degree() < ordinalT) {
            return Polynomial();
        } else {
            Polynomial result(degree() - ordinalT, this->Variable);
            T tempCoefficient = 1;
            for (size_t j = 0; j < result.size(); ++j) {
                for (size_t i = 1; i <= ordinalT; ++i) {
                    tempCoefficient *= T(j) + T(i); // Вылезают коэфициенты из степени при дифференцировании
                }
                result[j] = this->Array[j + ordinalT] * tempCoefficient;
                tempCoefficient = 1;
            }
            return result;
        }
    }

    template<typename T>
    Polynomial<T> &Polynomial<T>::operator=(const Polynomial &other) {
        this->Initialized = true;
        this->Size = other.Size;
        Variable = other.Variable;
        this->Array = new T[this->Size];
        this->Degree = other.Degree;
        if (other.Size != 0) {
            for (size_t i = 0; i < this->Size; ++i) {
                this->Array[i] = other.Array[i];
            }
        }
        return *this;
    }
}
#endif //CPP_OOP_POLYNOMIAL_H
