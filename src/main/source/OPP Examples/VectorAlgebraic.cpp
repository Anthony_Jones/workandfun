/*
* Copyright © 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef CPP_OPP_VECTORALG_H
#define CPP_OPP_VECTORALG_H

#include "OOP Examples/VectorAlgebraic.h"

#include "../utils/Trim.cpp"
#include <algorithm>
#include <cmath>

namespace cw {
    template<class T>
    VectorAlgebraic<T>::VectorAlgebraic() = default;

    template<class T>
    VectorAlgebraic<T>::VectorAlgebraic(size_t Size):VectorT<T>(Size) {

    }

    template<class T>
    VectorAlgebraic<T>::VectorAlgebraic(size_t Size, T number):VectorT<T>(Size, number) {

    }

    template<class T>
    VectorAlgebraic<T>::VectorAlgebraic(const VectorAlgebraic <T> &vectorAlgebraicT):VectorT<T>(vectorAlgebraicT) {
    }

    template<class T>
    VectorAlgebraic<T>::~VectorAlgebraic() = default;

    template<class T>
    const VectorAlgebraic <T> VectorAlgebraic<T>::operator-() const {
        if (this->Size == 0) {
            throw std::domain_error("Vector is degenerated");
        }
        VectorAlgebraic <T> result(*this);
        if (!this->Initialized) {
            for (size_t i = 0; i < this->Size; ++i) {
                result.Array[i] = -T();
            }
            result.Initialized = true;
        } else {
            for (size_t i = 0; i < this->Size; ++i) {
                result.Array[i] = -result.Array[i];
            }
        }
        return result;
    }

    template<class T>
    const VectorAlgebraic <T> VectorAlgebraic<T>::operator+() const {
        if (this->Size == 0) {
            throw std::domain_error("Vector is degenerated");
        }
        return *this;
    }

    template<class T>
    const VectorAlgebraic <T> &VectorAlgebraic<T>::operator++() {
        this->Size += 1;
        T *temp = new T[this->Size];
        for (size_t i = 0; i < this->Size; ++i) {
            temp[i] = this->Array[i];
        }
        temp[this->Size - 1] = T();
        delete[] this->Array;
        this->Array = temp;
        return *this;
    }

    template<class T>
    const VectorAlgebraic <T> VectorAlgebraic<T>::operator++(int) {
        if (this->Size == 0) {
            throw std::domain_error("Vector is degenerated");
        }
        VectorAlgebraic <T> result(*this);
        this->Size += 1;
        T *temp = new T[this->Size];
        for (size_t i = 0; i < this->Size; ++i) {
            temp[i] = this->Array[i];
        }
        temp[this->Size - 1] = T();
        delete[] this->Array;
        this->Array = temp;
        return result;
    }

    template<class T>
    const VectorAlgebraic <T> &VectorAlgebraic<T>::operator--() {
        if (this->Size == 0) {
            throw std::domain_error("Vector is degenerated");
        }
        this->Size -= 1;
        T *temp = new T[this->Size];
        for (size_t i = 0; i < this->Size; ++i) {
            temp[i] = this->Array[i];
        }
        delete[] this->Array;
        this->Array = temp;
        return *this;
    }

    template<class T>
    const VectorAlgebraic <T> VectorAlgebraic<T>::operator--(int) {
        if (this->Size == 0) {
            throw std::domain_error("Vector is degenerated");
        }
        VectorAlgebraic <T> result(*this);
        this->Size -= 1;
        T *temp = new T[this->Size];
        for (size_t i = 0; i < this->Size; ++i) {
            temp[i] = this->Array[i];
        }
        delete[] this->Array;
        this->Array = temp;
        return result;
    }


    template<class T>
    const VectorAlgebraic <T> VectorAlgebraic<T>::operator+(const VectorAlgebraic <T> &vectorAlgebraicT) const {
        if (this->Size == 0 or vectorAlgebraicT.size() == 0) {
            throw std::domain_error("Vector(s) are degenerated");
        }
        VectorAlgebraic <T> result;
        if (!this->Initialized and !vectorAlgebraicT.Initialized) { //Оба не проинициализированны
            if (this->Size >= vectorAlgebraicT.Size) {
                return *this;
            } else {
                return vectorAlgebraicT;
            }
        } else if (this->size() != vectorAlgebraicT.size()) {            // Имеют разные размеры
            if (this->Size > vectorAlgebraicT.Size) {                        //Вектор слева больше
                result = VectorAlgebraic<T>(this->Size);
                if (this->Initialized and
                    !vectorAlgebraicT.Initialized) { //Вектор слева проинициализирован, а справа нет
                    for (size_t i = 0; i < this->Size; ++i) {
                        result.Array[i] = this->Array[i] + T();
                    }
                } else if (!this->Initialized and vectorAlgebraicT.Initialized) { // Вектор справа проинициализирован ,
                    for (size_t i = 0; i < vectorAlgebraicT.Size; ++i) {            //а слева нет
                        result.Array[i] = T() + vectorAlgebraicT.Array[i];
                    }
                    for (size_t j = vectorAlgebraicT.Size; j < this->Size; ++j) {
                        result.Array[j] = T();
                    }
                } else {                                                        //Оба вектора проинициализированны
                    for (size_t i = 0; i < vectorAlgebraicT.Size; ++i) {
                        result.Array[i] = this->Array[i] + vectorAlgebraicT.Array[i];
                    }
                    for (size_t j = vectorAlgebraicT.size(); j < this->Size; ++j) {
                        result.Array[j] = this->Array[j] + T();
                    }
                }
            } else {                                                            //Справа больше чем слева
                result = VectorAlgebraic<T>(vectorAlgebraicT.Size);
                if (this->Initialized and
                    !vectorAlgebraicT.Initialized) { //Вектор слева проинициализирован, а справа нет
                    for (size_t i = 0; i < this->Size; ++i) {
                        result.Array[i] = this->Array[i] + T();
                    }
                    for (size_t j = this->Size; j < vectorAlgebraicT.Size; ++j) {
                        result.Array[j] = T();
                    }
                } else if (!this->Initialized and vectorAlgebraicT.Initialized) { // Вектор справа проинициализирован ,
                    for (size_t i = 0; i < vectorAlgebraicT.Size; ++i) {            //а слева нет
                        result.Array[i] = T() + vectorAlgebraicT.Array[i];
                    }
                } else {                                                        //Оба вектора проинициализированны
                    for (size_t i = 0; i < this->Size; ++i) {
                        result.Array[i] = this->Array[i] + vectorAlgebraicT.Array[i];
                    }
                    for (size_t j = this->Size; j < vectorAlgebraicT.Size; ++j) {
                        result.Array[j] = T();
                    }
                }
            }
        } else {                                                                    //Векторы имеют один размер
            result = VectorAlgebraic<T>(this->Size);
            if (this->Initialized and !vectorAlgebraicT.Initialized) { //Вектор слева проинициализирован, а справа нет
                for (size_t i = 0; i < this->Size; ++i) {
                    result.Array[i] = this->Array[i] + T();
                }
            } else if (!this->Initialized and vectorAlgebraicT.Initialized) { // Вектор справа проинициализирован ,
                for (size_t i = 0; i < this->Size; ++i) {                                   //а слева нет
                    result.Array[i] = T() + vectorAlgebraicT.Array[i];
                }
            } else {                                                            //Оба вектора проинициализированны
                for (size_t i = 0; i < this->Size; ++i) {
                    result.Array[i] = this->Array[i] + vectorAlgebraicT.Array[i];
                }
            }
        }
        result.Initialized = true;
        return result;
    }

    template<class T>
    T VectorAlgebraic<T>::module() const {
        if (this->Size == 0) {
            throw std::domain_error("Degenerated vector don't have module");
        }
        T module = T();
        if (!this->Initialized) {
            return sqrt(module);
        } else {
            for (size_t i = 0; i < this->Size; ++i) {
                module += this->Array[i] * this->Array[i];
            }
            return sqrt(module);
        }
    }

    template<class T>
    const VectorAlgebraic <T> VectorAlgebraic<T>::operator-(const VectorAlgebraic <T> &vectorAlgebraicT) const {
        if (this->Size == 0 or vectorAlgebraicT.size() == 0) {
            throw std::domain_error("Vector(s) are degenerated");
        }
        VectorAlgebraic <T> result;
        if (!this->Initialized and !vectorAlgebraicT.Initialized) { //Оба не проинициализированны
            if (this->Size >= vectorAlgebraicT.Size) {
                return *this;
            } else {
                return vectorAlgebraicT;
            }
        } else if (this->size() != vectorAlgebraicT.size()) {            // Имеют разные размеры
            if (this->Size > vectorAlgebraicT.Size) {                        //Вектор слева больше
                result = VectorAlgebraic<T>(this->Size);
                if (this->Initialized and
                    !vectorAlgebraicT.Initialized) { //Вектор слева проинициализирован, а справа нет
                    for (size_t i = 0; i < this->Size; ++i) {
                        result.Array[i] = this->Array[i] - T();
                    }
                } else if (!this->Initialized and vectorAlgebraicT.Initialized) { // Вектор справа проинициализирован ,
                    for (size_t i = 0; i < vectorAlgebraicT.Size; ++i) {            //а слева нет
                        result.Array[i] = T() - vectorAlgebraicT.Array[i];
                    }
                    for (size_t j = vectorAlgebraicT.Size; j < this->Size; ++j) {
                        result.Array[j] = -T();
                    }
                } else {                                                        //Оба вектора проинициализированны
                    for (size_t i = 0; i < vectorAlgebraicT.Size; ++i) {
                        result.Array[i] = this->Array[i] - vectorAlgebraicT.Array[i];
                    }
                    for (size_t j = vectorAlgebraicT.size(); j < this->Size; ++j) {
                        result.Array[j] = this->Array[j] - T();
                    }
                }
            } else {                                                            //Справа больше чем слева
                result = VectorAlgebraic<T>(vectorAlgebraicT.Size);
                if (this->Initialized and
                    !vectorAlgebraicT.Initialized) { //Вектор слева проинициализирован, а справа нет
                    for (size_t i = 0; i < this->Size; ++i) {
                        result.Array[i] = this->Array[i] - T();
                    }
                    for (size_t j = this->Size; j < vectorAlgebraicT.Size; ++j) {
                        result.Array[j] = -T();
                    }
                } else if (!this->Initialized and vectorAlgebraicT.Initialized) { // Вектор справа проинициализирован ,
                    for (size_t i = 0; i < vectorAlgebraicT.Size; ++i) {            //а слева нет
                        result.Array[i] = T() - vectorAlgebraicT.Array[i];
                    }
                } else {                                                        //Оба вектора проинициализированны
                    for (size_t i = 0; i < this->Size; ++i) {
                        result.Array[i] = this->Array[i] - vectorAlgebraicT.Array[i];
                    }
                    for (size_t j = this->Size; j < vectorAlgebraicT.Size; ++j) {
                        result.Array[j] = -T();
                    }
                }
            }
        } else {                                                                    //Векторы имеют один размер
            result = VectorAlgebraic<T>(this->Size);
            if (this->Initialized and !vectorAlgebraicT.Initialized) { //Вектор слева проинициализирован, а справа нет
                for (size_t i = 0; i < this->Size; ++i) {
                    result.Array[i] = this->Array[i] - T();
                }
            } else if (!this->Initialized and vectorAlgebraicT.Initialized) { // Вектор справа проинициализирован ,
                for (size_t i = 0; i < this->Size; ++i) {                                   //а слева нет
                    result.Array[i] = T() - vectorAlgebraicT.Array[i];
                }
            } else {                                                            //Оба вектора проинициализированны
                for (size_t i = 0; i < this->Size; ++i) {
                    result.Array[i] = this->Array[i] - vectorAlgebraicT.Array[i];
                }
            }
        }
        result.Initialized = true;
        return result;
    }

    template<class T>
    T VectorAlgebraic<T>::operator*(const VectorAlgebraic <T> &vectorAlgebraicT) {
        if (this->Size == 0 and vectorAlgebraicT.Size == 0) {
            throw std::domain_error("Vectors are degenerated");
        }
        T skalarMulti = T();
        if (!this->Initialized and !vectorAlgebraicT.Initialized) { //Both is not initialized
            size_t temp = (this->Size > vectorAlgebraicT.size()) ? this->Size : vectorAlgebraicT.Size;
            for (size_t i = 0; i < temp; ++i) {
                skalarMulti += T() * T();
            }
        } else if (this->Size != vectorAlgebraicT.size()) {                    //Size don't macth
            if (this->Size > vectorAlgebraicT.Size) {                    //Left bigger than right
                if (this->Initialized and !vectorAlgebraicT.Initialized) {       //Lest initialized, right is not
                    for (size_t i = 0; i < this->Size; ++i) {
                        skalarMulti += this->Array[i] * T();
                    }
                } else if (!this->Initialized and
                           vectorAlgebraicT.Initialized) {//Left not initialized, Right is the opposite
                    for (size_t i = 0; i < vectorAlgebraicT.Size; ++i) {
                        skalarMulti += T() * vectorAlgebraicT.Array[i];
                    }
                    for (size_t j = vectorAlgebraicT.Size; j < this->Size; ++j) {
                        skalarMulti += T() * T();
                    }
                } else {                                                            //Both is initialized
                    for (size_t i = 0; i < vectorAlgebraicT.Size; ++i) {
                        skalarMulti += this->Array[i] * vectorAlgebraicT.Array[i];
                    }
                    for (size_t j = vectorAlgebraicT.Size; j < this->Size; ++j) {
                        skalarMulti += this->Array[j] * T();
                    }
                }
            } else {                                                        // Right is bigger than left
                if (this->Initialized and !vectorAlgebraicT.Initialized) { //Lest initialized, right is not
                    for (size_t i = 0; i < this->Size; ++i) {
                        skalarMulti += this->Array[i] * T();
                    }
                    for (size_t j = this->Size; j < vectorAlgebraicT.Size; ++j) {
                        skalarMulti += T() * T();
                    }
                } else if (!this->Initialized and
                           vectorAlgebraicT.Initialized) {//Left not initialized, Right is the opposite
                    for (size_t i = 0; i < vectorAlgebraicT.Size; ++i) {
                        skalarMulti += T() * vectorAlgebraicT.Array[i];
                    }
                } else {                                                      //Both is initialized
                    for (size_t i = 0; i < this->Size; ++i) {
                        skalarMulti += this->Array[i] * vectorAlgebraicT.Array[i];
                    }
                    for (size_t j = this->Size; j < vectorAlgebraicT.Size; ++j) {
                        skalarMulti += T() * vectorAlgebraicT.Array[j];
                    }
                }
            }
        } else {                                                    //Sizes are match
            if (this->Initialized and !vectorAlgebraicT.Initialized) {
                for (size_t i = 0; i < this->Size; ++i) {
                    skalarMulti += this->Array[i] * T();
                }
            } else if (!this->Initialized and vectorAlgebraicT.Initialized) {
                for (size_t i = 0; i < this->Size; ++i) {
                    skalarMulti += T() * vectorAlgebraicT.Array[i];
                }
            } else {
                for (size_t i = 0; i < this->Size; ++i) {
                    skalarMulti += this->Array[i] * vectorAlgebraicT.Array[i];
                }
            }
        }
        return skalarMulti;
    }

    template<class T>
    const VectorAlgebraic <T> &VectorAlgebraic<T>::operator+=(const VectorAlgebraic <T> &vectorAlgebraicT) {
        if (this->Size == 0 or vectorAlgebraicT.size() == 0) {
            throw std::domain_error("Vector(s) are degenerated");
        }
        if (!this->Initialized and !vectorAlgebraicT.Initialized) { //Оба не проинициализированны
            if (this->Size >= vectorAlgebraicT.Size) {
                return *this;
            } else {
                return vectorAlgebraicT;
            }
        } else if (this->size() != vectorAlgebraicT.size()) {            // Имеют разные размеры
            if (this->Size > vectorAlgebraicT.Size) {                        //Вектор слева больше
                if (this->Initialized and
                    !vectorAlgebraicT.Initialized) { //Вектор слева проинициализирован, а справа нет
                    for (size_t i = 0; i < this->Size; ++i) {
                        this->Array[i] += T();
                    }
                } else if (!this->Initialized and vectorAlgebraicT.Initialized) { // Вектор справа проинициализирован ,
                    for (size_t i = 0; i < vectorAlgebraicT.Size; ++i) {            //а слева нет
                        this->Array[i] = T() + vectorAlgebraicT.Array[i];
                    }
                    for (size_t j = vectorAlgebraicT.Size; j < this->Size; ++j) {
                        this->Array[j] = T();
                    }
                } else {                                                        //Оба вектора проинициализированны
                    for (size_t i = 0; i < vectorAlgebraicT.Size; ++i) {
                        this->Array[i] += vectorAlgebraicT.Array[i];
                    }
                    for (size_t j = vectorAlgebraicT.size(); j < this->Size; ++j) {
                        this->Array[j] += T();
                    }
                }
            } else {//Справа больше чем слева
                T *result = new T[vectorAlgebraicT.Size];
                if (this->Initialized and
                    !vectorAlgebraicT.Initialized) { //Вектор слева проинициализирован, а справа нет
                    for (size_t i = 0; i < this->Size; ++i) {
                        result[i] = this->Array[i] + T();
                    }
                    for (size_t j = this->Size; j < vectorAlgebraicT.Size; ++j) {
                        result[j] = T();
                    }
                } else if (!this->Initialized and vectorAlgebraicT.Initialized) { // Вектор справа проинициализирован ,
                    for (size_t i = 0; i < vectorAlgebraicT.Size; ++i) {            //а слева нет
                        result[i] = T() + vectorAlgebraicT.Array[i];
                    }
                } else {                                                        //Оба вектора проинициализированны
                    for (size_t i = 0; i < this->Size; ++i) {
                        result[i] = this->Array[i] + vectorAlgebraicT.Array[i];
                    }
                    for (size_t j = this->Size; j < vectorAlgebraicT.Size; ++j) {
                        result[j] = T();
                    }
                }
                this->Size = vectorAlgebraicT.Size;
                delete[] this->Array;
                this->Array = result;
            }
        } else {                                                                    //Векторы имеют один размер
            if (this->Initialized and !vectorAlgebraicT.Initialized) { //Вектор слева проинициализирован, а справа нет
                for (size_t i = 0; i < this->Size; ++i) {
                    this->Array[i] += T();
                }
            } else if (!this->Initialized and vectorAlgebraicT.Initialized) { // Вектор справа проинициализирован ,
                for (size_t i = 0; i < this->Size; ++i) {                                   //а слева нет
                    this->Array[i] = T() + vectorAlgebraicT.Array[i];
                }
            } else {                                                            //Оба вектора проинициализированны
                for (size_t i = 0; i < this->Size; ++i) {
                    this->Array[i] += vectorAlgebraicT.Array[i];
                }
            }
        }
        this->Initialized = true;
        return *this;
    }

    template<class T>
    const VectorAlgebraic <T> &VectorAlgebraic<T>::operator-=(const VectorAlgebraic <T> &vectorAlgebraicT) {
        if (this->Size == 0 or vectorAlgebraicT.size() == 0) {
            throw std::domain_error("Vector(s) are degenerated");
        }
        if (!this->Initialized and !vectorAlgebraicT.Initialized) { //Оба не проинициализированны
            if (this->Size >= vectorAlgebraicT.Size) {
                return *this;
            } else {
                return vectorAlgebraicT;
            }
        } else if (this->size() != vectorAlgebraicT.size()) {            // Имеют разные размеры
            if (this->Size > vectorAlgebraicT.Size) {                        //Вектор слева больше
                if (this->Initialized and
                    !vectorAlgebraicT.Initialized) { //Вектор слева проинициализирован, а справа нет
                    for (size_t i = 0; i < this->Size; ++i) {
                        this->Array[i] -= T();
                    }
                } else if (!this->Initialized and vectorAlgebraicT.Initialized) { // Вектор справа проинициализирован ,
                    for (size_t i = 0; i < vectorAlgebraicT.Size; ++i) {            //а слева нет
                        this->Array[i] = T() - vectorAlgebraicT.Array[i];
                    }
                    for (size_t j = vectorAlgebraicT.Size; j < this->Size; ++j) {
                        this->Array[j] = -T();
                    }
                } else {                                                        //Оба вектора проинициализированны
                    for (size_t i = 0; i < vectorAlgebraicT.Size; ++i) {
                        this->Array[i] -= vectorAlgebraicT.Array[i];
                    }
                    for (size_t j = vectorAlgebraicT.size(); j < this->Size; ++j) {
                        this->Array[j] -= T();
                    }
                }
            } else {//Справа больше чем слева
                T *result = new T[vectorAlgebraicT.Size];
                if (this->Initialized and
                    !vectorAlgebraicT.Initialized) { //Вектор слева проинициализирован, а справа нет
                    for (size_t i = 0; i < this->Size; ++i) {
                        result[i] = this->Array[i] - T();
                    }
                    for (size_t j = this->Size; j < vectorAlgebraicT.Size; ++j) {
                        result[j] = -T();
                    }
                } else if (!this->Initialized and vectorAlgebraicT.Initialized) { // Вектор справа проинициализирован ,
                    for (size_t i = 0; i < vectorAlgebraicT.Size; ++i) {            //а слева нет
                        result[i] = T() - vectorAlgebraicT.Array[i];
                    }
                } else {                                                        //Оба вектора проинициализированны
                    for (size_t i = 0; i < this->Size; ++i) {
                        result[i] = this->Array[i] - vectorAlgebraicT.Array[i];
                    }
                    for (size_t j = this->Size; j < vectorAlgebraicT.Size; ++j) {
                        result[j] = -T();
                    }
                }
                this->Size = vectorAlgebraicT.Size;
                delete[] this->Array;
                this->Array = result;
            }
        } else {                                                                    //Векторы имеют один размер
            if (this->Initialized and !vectorAlgebraicT.Initialized) { //Вектор слева проинициализирован, а справа нет
                for (size_t i = 0; i < this->Size; ++i) {
                    this->Array[i] -= T();
                }
            } else if (!this->Initialized and vectorAlgebraicT.Initialized) { // Вектор справа проинициализирован ,
                for (size_t i = 0; i < this->Size; ++i) {                                   //а слева нет
                    this->Array[i] = T() - vectorAlgebraicT.Array[i];
                }
            } else {                                                            //Оба вектора проинициализированны
                for (size_t i = 0; i < this->Size; ++i) {
                    this->Array[i] -= vectorAlgebraicT.Array[i];
                }
            }
        }
        this->Initialized = true;
        return *this;
    }

    template<class T>
    const VectorAlgebraic <T> VectorAlgebraic<T>::operator*(T number) const {
        if (this->Size == 0) {
            throw std::domain_error("Vector is degenerated");
        }
        VectorAlgebraic <T> temp(*this);
        if (!this->Initialized) {
            for (size_t i = 0; i < this->Size; ++i) {
                temp.Array[i] = T() * number;
            }
            temp.Initialized = true;
        } else {

            for (size_t i = 0; i < this->Size; ++i) {
                temp.Array[i] *= number;
            }
        }
        return temp;

    }

    template<class T>
    const VectorAlgebraic <T> VectorAlgebraic<T>::operator/(T number) const {
        if (this->Size == 0) {
            throw std::domain_error("Vector is degenerated");
        } else if (number == 0) {
            throw std::domain_error("Division by zero");
        }
        VectorAlgebraic <T> temp(*this);
        if (!this->Initialized) {
            for (size_t i = 0; i < this->Size; ++i) {
                temp.Array[i] = T() / number;
            }
            temp.Initialized = true;
        } else {

            for (size_t i = 0; i < this->Size; ++i) {
                temp.Array[i] /= number;
            }
        }
        return temp;
    }

    template<class T>
    const VectorAlgebraic <T> operator*(T number, const VectorAlgebraic <T> &vectorAlgebraicT) {
        if (vectorAlgebraicT.Size == 0) {
            throw std::domain_error("Vector is degenerated");
        }
        VectorAlgebraic <T> temp(vectorAlgebraicT);
        if (!vectorAlgebraicT.Initialized) {
            for (size_t i = 0; i < vectorAlgebraicT.Size; ++i) {
                temp.Array[i] = number * T();
            }
            temp.Initialized = true;
        } else {
            for (size_t i = 0; i < vectorAlgebraicT.Size; ++i) {
                temp.Array[i] = number * temp.Array[i];
            }
        }
        return temp;
    }


    template<class T>
    std::ostream &operator<<(std::ostream &os, const VectorAlgebraic <T> &vector) {
        if (vector.size() != 0) {
            os << "(";
            if (vector.Initialized) {
                for (size_t i = 0; i < vector.Size; ++i) {
                    os << vector.Array[i];
                    if (i != vector.size() - 1) {
                        cout << VectorAlgebraic<T>::ElementSeparator;
                    }
                }
            }
            os << ")";
        }
        return os;
    }

    template<class T>
    std::istream &operator>>(std::istream &is, VectorAlgebraic <T> &vector) {
        std::string string1;
        std::getline(is, string1);                            //getting string which initializate vector
        std::string string2;
        std::unique_copy(string1.begin(), string1.end(), std::back_inserter(string2), // deleting repeating whitespaces
                         [](char c1, char c2) { return isspace(c1) and isspace(c2); });
        string2 = trim(
                string2);                                         //deleting whitespaces at begin, and end of string
        size_t size = 1;
        if (string2.empty()) {
            delete[] vector.Array;
            vector.Size = 0;
            return is;
        }
        for (char i : string2) {
            if (isspace(i)) {
                ++size;
            }
        }
        string1.erase();
        if (size != vector.size()) {
            delete[] vector.Array;
            vector.Array = new T[vector.Size = size];
        }
        size_t i = 0;
        for (size_t j = 0; j < vector.Size; ++j) {
            for (; i < string2.size(); ++i) {
                if (isspace(string2[i])) {
                    ++i;
                    break;
                }
                string1 += string2[i];
            }
            vector.Array[j] = std::stod(string1);
            string1.erase();
        }
        vector.Initialized = true;
        return is;
    }
}
#endif //CPP_OPP_VECTORALG_H