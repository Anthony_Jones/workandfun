/*
* Copyright © 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef CPP_OOP_MATRIXALGEBRAIC_H
#define CPP_OOP_MATRIXALGEBRAIC_H

#include "MatrixT.h"
#include "OOP Examples/VectorAlgebraic.h"

#include <iostream>
#include <cstddef>

/**
 * @brief Class template Algebraic Matrices
 * @author AnthonyJ , Alex Atamanenko
 * @tparam T Any type with arithmetic operations
 */
namespace cw {
    template<class T = double>
    class MatrixRectangle : public MatrixT<T> {

        static constexpr long double EPSILON = 1e-14; // comparison error
    public:

        MatrixRectangle();

        MatrixRectangle(size_t rows, size_t columns);

        MatrixRectangle(size_t rows, size_t columns, T number);

        MatrixRectangle(const MatrixRectangle<T> &other);

        virtual ~MatrixRectangle() = default;

        virtual MatrixRectangle<T> &operator=(const MatrixRectangle<T> &other);

        const MatrixRectangle<T> operator+(const MatrixRectangle<T> &other) const;

        const MatrixRectangle<T> operator-(const MatrixRectangle<T> &other) const;

        const MatrixRectangle<T> operator*(const MatrixRectangle<T> &other) const;


        const MatrixRectangle<T> operator*(const T &number) const;

        const MatrixRectangle<T> operator/(const T &number) const;

        const MatrixRectangle<T> &operator*=(const T &number) const;

        const MatrixRectangle<T> &operator/=(const T &number) const;


        template<class V>
        friend const MatrixRectangle<V> operator*(const V &number, const MatrixRectangle<V> &matrix);

        template<class V>
        friend const MatrixRectangle<V> operator/(const V &number, const MatrixRectangle<V> &matrix);

        const MatrixRectangle<T> &operator+=(const MatrixRectangle<T> &other) const;

        const MatrixRectangle<T> &operator-=(const MatrixRectangle<T> &other) const;

        const MatrixRectangle<T> &operator*=(const MatrixRectangle<T> &other);


        virtual const MatrixRectangle<T> getMinor(long long RowNumber, long long ColumnNumber) const;


        const bool operator==(const MatrixRectangle<T> &other) const;

        const bool operator!=(const MatrixRectangle<T> &other) const;

        const MatrixRectangle<T> transpose() const;

        const VectorAlgebraic<T> operator*(VectorAlgebraic<T> &vectorAlgebraic) const;

        MatrixRectangle(const VectorAlgebraic<T> &vector);

        MatrixRectangle<T> toIterationForm() const;

    };

/**
 * @author AntonyJ
 * @brief sum of two matrices with same Size
 * @throw std::domain_error When one of the matrices is degenerated(Row = 0)
 * @throw std::domain_error When matrices are not compatible(Rows != other.Rows or Columns != other.Columns)
 * @tparam T Algebraic type
 * @param other Second matrix
 * @return Matrix result of sum
 * @version 1.0
 */
    template<class T>
    const MatrixRectangle<T> MatrixRectangle<T>::operator+(const MatrixRectangle<T> &other) const {
        if (this->Rows * other.Rows == 0) {
            throw std::domain_error("Matrix is degenerated");
        } else if (this->Rows != other.Rows or this->Columns != other.Columns) {
            throw std::domain_error("Matrices are not compatible");
        }
        MatrixRectangle<T> result(this->Rows, this->Columns);
        for (size_t i = 0; i < this->Rows; ++i) {
            for (size_t j = 0; j < this->Columns; ++j) {
                result(i, j) = this->Array[i][j] + other.Array[i][j];
            }
        }
        return result;
    }

/**
 * @brief Subtraction os two matrices with same Size
 * @author AnthonyJ
 * @throw std::domain_error When one of the matrices is degenerated(Row = 0)
 * @throw std::domain_error When matrices are not compatible(Rows != other.Rows or Columns != other.Columns)
 * @tparam T Algebraic type
 * @param other Second matrix
 * @return Matrix result of subtraction
 * @version 1.0
 */
    template<class T>
    const MatrixRectangle<T> MatrixRectangle<T>::operator-(const MatrixRectangle<T> &other) const {
        if (this->Rows * other.Rows == 0) {
            throw std::domain_error("Matrix is degenerated");
        } else if (this->Rows != other.Rows or this->Columns != other.Columns) {
            throw std::domain_error("Matrices are not compatible");
        }
        MatrixRectangle<T> result(this->Rows, this->Columns);
        for (size_t i = 0; i < this->Rows; ++i) {
            for (size_t j = 0; j < this->Columns; ++j) {
                result(i, j) = this->Array[i][j] - other.Array[i][j];
            }
        }
        return result;
    }

/**
 * @brief Multiplication Matrix By some Number
 * @throw std::domain_error When The matrix is degenerated(Rows == 0)
 * @tparam T Algebraic type
 * @param number Number by which you want to multiply matrix
 * @return Matrix result of multiplication
 */
    template<class T>
    const MatrixRectangle<T> MatrixRectangle<T>::operator*(const T &number) const {
        if (this->Rows == 0) {
            throw std::domain_error("The Matrix is degenerated");
        }
        MatrixRectangle<T> result(this->Rows, this->Columns);
        for (size_t i = 0; i < this->Rows; ++i) {
            for (size_t j = 0; j < this->Columns; ++j) {
                result(i, j) = this->Array[i][j] * number;
            }
        }
        return result;
    }

/**
 * @brief Division matrix by some Number
 * @throw std::domain_error When The matrix is degenerated
 * @throw std::domain_error When number == T()(Zero)
 * @tparam T Algebraic type
 * @param number Number by which you want to multiply matrix
 * @return Matrix result of multiplication
 */
    template<class T>
    const MatrixRectangle<T> MatrixRectangle<T>::operator/(const T &number) const {
        if (this->Rows == 0) {
            throw std::domain_error("The Matrix is degenerated");
        } else if (number == T()) {
            throw std::domain_error("Division by zero");
        }
        MatrixRectangle<T> result(this->Rows, this->Columns);
        for (size_t i = 0; i < this->Rows; ++i) {
            for (size_t j = 0; j < this->Columns; ++j) {
                result.Array[i][j] = this->Array[i][j] / number;
            }
        }
        return result;
    }

/**
 * @brief Multiplication matrix by some number
 * @throw std::domain_error When Matrix if degenerated(Rows == 0 and Columns == 0)
 * @tparam T Algebraic type
 * @param number Some number
 * @param matrix
 * @return Matrix result of multiplication
 */
    template<class T>
    const MatrixRectangle<T> operator*(const T &number, const MatrixRectangle<T> &matrix) {
        if (matrix.Rows == 0) {
            throw std::domain_error("The Matrix is degenerated");
        }
        MatrixRectangle<T> result(matrix.Rows, matrix.Columns);
        for (size_t i = 0; i < matrix.Rows; ++i) {
            for (size_t j = 0; j < matrix.Columns; ++j) {
                result(i, j) = matrix.Array[i][j] / number;
            }
        }
        return result;
    }

/**
 * @brief Multiplication matrix by number with increasing
 * @tparam T Algebraic type
 * @throw std::domain_error When matrix if degenerated(Rows == 0)
 * @param number
 * @return *this result of multiplication
 */
    template<class T>
    const MatrixRectangle<T> &MatrixRectangle<T>::operator*=(const T &number) const {

        if (this->Rows == 0) {
            throw std::domain_error("The Matrix is degenerated");
        }
        for (size_t i = 0; i < this->Rows; ++i) {
            for (size_t j = 0; j < this->Columns; ++j) {
                this->Array[i][j] *= number;
            }
        }
        return *this;
    }

/**
 * @brief Division matrix by some number with increasing
 * @tparam T
 * @throw std::domain_error When matrix is degenerated (Rows == 0)
 * @throw std::domain_error When number == T()(Zero)
 * @param number
 * @return
 */
    template<class T>
    const MatrixRectangle<T> &MatrixRectangle<T>::operator/=(const T &number) const {
        if (this->Rows == 0) {
            throw std::domain_error("The Matrix is degenerated");
        } else if (number == T()) {
            throw std::domain_error("Division by zero");
        }
        for (size_t i = 0; i < this->Rows; ++i) {
            for (size_t j = 0; j < this->Columns; ++j) {
                this->Array[i][j] /= number;
            }
        }
        return *this;
    }

/**
 * @brief Division matrix by some number with increasing
 * @tparam T
 * @throw std::domain_error When matrix is degenerated (Rows == 0)
 * @throw std::domain_error When number == T()(Zero)
 * @param number
 * @return
 */
    template<class T>
    const MatrixRectangle<T> operator/(const T &number, const MatrixRectangle<T> &matrix) {
        if (matrix.Rows == 0) {
            throw std::domain_error("The Matrix is degenerated");
        } else if (number == T()) {
            throw std::domain_error("Division by zero");
        }
        MatrixRectangle<T> result(matrix.Rows, matrix.Columns);
        for (size_t i = 0; i < matrix.Rows; ++i) {
            for (size_t j = 0; j < matrix.Columns; ++j) {
                result.Array[i][j] = matrix.Array[i][j] / number;
            }
        }
        return result;
    }

/**
 * @brief Sum of wo Matrices with increasing
 * @tparam T
 * @throw std::domain_error When one of the Matrices is degenerated(Rows == 0)
 * @throw std::domain_error When matrices is compatible(Rows != other.Rows or Columns != other.Columns)
 * @param other Right Matrix
 * @return
 */
    template<class T>
    const MatrixRectangle<T> &MatrixRectangle<T>::operator+=(const MatrixRectangle<T> &other) const {
        if (this->Rows * other.Rows == 0) {
            throw std::domain_error("Matrix is degenerated");
        } else if (this->Rows != other.Rows or this->Columns != other.Columns) {
            throw std::domain_error("Matrices are not compatible");
        }

        for (size_t i = 0; i < this->Rows; ++i) {
            for (size_t j = 0; j < this->Columns; ++j) {
                this->Array[i][j] += other.Array[i][j];
            }
        }
        return *this;
    }

/**
 * @brief Subtraction of two matrices with increasing
 * @tparam T
 * @throw std::domain_error When one of the matrices is degenerated(Rows == 0)
 * @throw std::domain_error When matrices is not compatible(Rows != other.Rows or Columns != other.Columns)
 * @param other
 * @return
 */
    template<class T>
    const MatrixRectangle<T> &MatrixRectangle<T>::operator-=(const MatrixRectangle<T> &other) const {
        if (this->Rows * other.Rows == 0) {
            throw std::domain_error("Matrix is degenerated");
        } else if (this->Rows != other.Rows or this->Columns != other.Columns) {
            throw std::domain_error("Matrices are not compatible");
        }
        for (size_t i = 0; i < this->Rows; ++i) {
            for (size_t j = 0; j < this->Columns; ++j) {
                this->Array[i][j] -= other.Array[i][j];
            }
        }
        return *this;
    }

/**
 * @brief Operation of assignment Right Matrix to Left Matrix
 * @tparam T
 * @throw std::bad_alloc
 * @param other
 * @return
 */
    template<class T>
    MatrixRectangle<T> &MatrixRectangle<T>::operator=(const MatrixRectangle<T> &other) {
        if (*this == other) {
            return *this;
        } else if (this->Rows == other.Rows and this->Columns == other.Columns) {
            memcpy(this->Array[0], other.Array[0], sizeof(T) * this->Rows * this->Columns);
            for (size_t i = 1; i < this->Rows; ++i) {
                *(this->Array + i) = *(this->Array) + this->Columns * i;
            }
        } else {
            delete[] this->Array;
            this->Rows = other.Rows;
            this->Columns = other.Columns;
            try {
                this->Array = MatrixT<T>::createArray2d(this->Rows, this->Columns);
            } catch (std::bad_alloc &e) {
                throw e;
            }
            memcpy(this->Array[0], other.Array[0], sizeof(T) * this->Rows * this->Columns);
            for (size_t i = 1; i < this->Rows; ++i) {
                *(this->Array + i) = *(this->Array) + this->Columns * i;
            }
        }
        return *this;
    }

/**
 * @throw std::domain_error When matrices is not compatible(Columns != other.Rows)
 * @throw std::domain_error When one of the matrices is degenerated(Rows = 0)
 * @tparam T
 * @param other
 * @return
 */
    template<class T>
    const MatrixRectangle<T> MatrixRectangle<T>::operator*(const MatrixRectangle<T> &other) const {
        if (this->Columns != other.Rows) {
            throw std::domain_error("Matrices are not compatible");
        } else if (this->Rows * other.Rows == 0) {
            throw std::domain_error("Matrix is degenerated");
        }
        MatrixRectangle<T> result(this->Rows, other.Columns);
        T sum = 0;
        for (size_t i = 0; i < this->Rows; ++i) {
            for (size_t j = 0; j < other.Columns; ++j) {
                sum = 0;
                for (size_t k = 0; k < this->Columns; ++k) {
                    sum += this->Array[i][k] * other.Array[k][j];
                }
                result.Array[i][j] = sum;
            }
        }
        return result;
    }

/**
 * @throw std::domain_error when Columns of left Matrix does not equal Rows of right Matrix
 * @throw std::domain_error when one of the matrices is degenerated(Rows = 0)
 * @throw std::bad_alloc
 * @tparam T Algebraic type
 * @param other
 * @return
 */
    template<class T>
    const MatrixRectangle<T> &MatrixRectangle<T>::operator*=(const MatrixRectangle<T> &other) {
        if (this->Columns != other.Rows) {
            throw std::domain_error("Matrices are not compatible");
        } else if (this->Rows * other.Rows == 0) {
            throw std::domain_error("Matrix is degenerated");
        }
        T **result;
        try {
            result = MatrixRectangle<T>::createArray2d(this->Rows, other.Columns);
        } catch (std::bad_alloc &e) {
            throw e;
        }
        T sum = 0;
        for (size_t i = 0; i < this->Rows; ++i) {
            for (size_t j = 0; j < other.Columns; ++j) {
                sum = 0;
                for (size_t k = 0; k < this->Columns; ++k) {
                    sum += this->Array[i][k] * other.Array[k][j];
                }
                result[i][j] = sum;
            }
        }
        delete[] this->Array;
        this->Array = result;
        return *this;
    }


    template<class T>
    MatrixRectangle<T>::MatrixRectangle(): MatrixT<T>() {

    }

    template<class T>
    MatrixRectangle<T>::MatrixRectangle(size_t
                                        rows, size_t
                                        columns): MatrixT<T>(rows, columns) {

    }

    template<class T>
    MatrixRectangle<T>::MatrixRectangle(size_t
                                        rows, size_t
                                        columns, T
                                        number):MatrixT<T>(rows, columns, number) {

    }

    template<class T>
    MatrixRectangle<T>::MatrixRectangle(const MatrixRectangle<T> &other) : MatrixT<T>(other) {

    }


/**
 *
 * @tparam T
 * @details if RowNumber or ColumnsNumber == -1 Row or Columns do not deleting from result
 * @throw std::domain_error When Matrix is degenerated(Rows == 0)
 * @throw std::domain_error When Matrix is too small to have Minors( Rows == 1 or Columns == 1)
 * @param RowNumber Number of row which we don't want to see in minor
 * @param ColumnNumber Number of Columns which we don't want to see in minor
 * @return Minor with Rows == this->Rows - 1 and Columns == this->Columns - 1
 */
    template<class T>
    const MatrixRectangle<T> MatrixRectangle<T>::getMinor(long long
                                                          RowNumber,
                                                          long long
                                                          ColumnNumber) const {
        if (this->Rows == 0) {
            throw std::domain_error("Degenerated matrices does not have minors");
        } else if (this->Rows == 1 or this->Columns == 1) {
            throw std::domain_error("Matrix does not have minors(Size is to small)");
        }
        MatrixRectangle<T> result;
        if (RowNumber != -1 or ColumnNumber != -1) {
            size_t RowNum = size_t(RowNumber);
            size_t ColumnNum = size_t(ColumnNumber);
            result = MatrixRectangle<T>(this->Rows - 1, this->Columns - 1);
            for (size_t i = 0, k = 0; k < this->Rows - 1; ++i, ++k) {
                if (i == RowNum) ++i;
                for (size_t j = 0, l = 0; l < this->Columns - 1; ++j, ++l) {
                    if (j == ColumnNum) ++j;
                    result.Array[k][l] = this->Array[i][j];
                }
            }
        } else if (RowNumber == -1) {
            size_t ColumnNum = size_t(ColumnNumber);
            result = MatrixRectangle<T>(this->Rows, this->Columns - 1);
            for (size_t i = 0, k = 0; k < this->Rows; ++i, ++k) {
                for (size_t j = 0, l = 0; l < this->Columns - 1; ++j, ++l) {
                    if (j == ColumnNum) ++j;
                    result.Array[k][l] = this->Array[i][j];
                }
            }
        } else if (ColumnNumber == -1) {
            size_t RowNum = size_t(RowNumber);
            result = MatrixRectangle<T>(this->Rows - 1, this->Columns);
            for (size_t i = 0, k = 0; k < this->Rows - 1; ++i, ++k) {
                if (i == RowNum) ++i;
                for (size_t j = 0, l = 0; l < this->Columns; ++j, ++l) {
                    result.Array[k][l] = this->Array[i][j];
                }
            }
        } else {
            return *this;
        }
        return result;
    }


/**
 * @throw std::domain_error When matrix is degenerated(Rows * columns == 0)
 * @tparam T
 * @return Transposed matrix
 */
    template<class T>
    const MatrixRectangle<T> MatrixRectangle<T>::transpose() const {
        if (this->Rows * this->Columns == 0) {
            throw std::domain_error("Degenerated matrices does not have transposed matrix");
        } else if (this->Rows == 1 and this->Columns == 1) {
            return *this;
        }
        MatrixRectangle<T> result(this->Rows, this->Columns);
        for (size_t i = 0; i < this->Rows; ++i) {
            for (size_t j = 0; j < this->Columns; ++j) {
                result.Array[i][j] = this->Array[j][i];
            }
        }
        return result;
    }

/**
 * @brief Check equliaty with some comprasion error
 * @tparam T
 * @param other
 * @return
 */
    template<class T>
    const bool MatrixRectangle<T>::operator==(const MatrixRectangle<T> &other) const {
        if (this->Rows != other.Rows or this->Columns != other.Columns) {
            return false;
        } else {
            if (this->Rows * other.Rows * this->Columns * other.Columns == 0) {
                return false;
            }
            for (size_t i = 0; i < this->Rows; ++i) {
                for (size_t j = 0; j < this->Columns; ++j) {
                    if (fabs(this->Array[i][j] - other.Array[i][j]) > EPSILON) {
                        return false;
                    }
                }
            }
            return true;
        }
    }

    template<class T>
    const bool MatrixRectangle<T>::operator!=(const MatrixRectangle<T> &other) const {
        if (this->Rows != other.Rows or this->Columns != other.Columns) {
            return true;
        } else {
            if (this->Rows * other.Rows * this->Columns * other.Columns == 0) {
                return true;
            }
            for (size_t i = 0; i < this->Rows; ++i) {
                for (size_t j = 0; j < this->Columns; ++j) {
                    if (fabs(this->Array[i][j] - other.Array[i][j]) < EPSILON) {
                        return true;
                    }
                }
            }
            return false;
        }
    }

    /**
     *
     * @tparam T
     * @param vectorAlgebraic
     * @return
     */
    template<class T>
    const VectorAlgebraic<T> MatrixRectangle<T>::operator*(VectorAlgebraic<T> &vectorAlgebraic) const {
        if (this->Columns != vectorAlgebraic.size()) {
            throw std::domain_error("The Matrix and the vector are not compatible");
        } else {
            VectorAlgebraic<T> result(vectorAlgebraic.size());
            T sum = 0;
            for (size_t i = 0; i < this->Rows; ++i) {
                for (size_t j = 0; j < vectorAlgebraic.size(); ++j) {
                    sum = 0;
                    for (size_t k = 0; k < vectorAlgebraic.size(); ++k) {
                        sum += this->Array[i][k] * vectorAlgebraic[k];
                    }
                    result[i] = sum;
                }
            }
            return result;
        }
    }

    template<class T>
    MatrixRectangle<T>::MatrixRectangle(const VectorAlgebraic<T> &vectorAlgebraic) {
        if (vectorAlgebraic.size() == 0) {
            this->Rows = this->Columns = 0;
            this->Array = nullptr;
        } else {
            this->Rows = vectorAlgebraic.size();
            this->Columns = 1;
            this->Array = this->createArray2d(this->Rows, this->Columns);
            memccpy(this->Array[0], vectorAlgebraic.Array, this->Rows * sizeof(T));
        }
    }

    template<class T>
    MatrixRectangle<T> MatrixRectangle<T>::toIterationForm() const {
        T temp = 1;
        for (size_t l = 0; l < this->Rows; ++l) {
            temp *= this->Array[l][l];
        }
        if (temp < 1e-20) {
            throw std::domain_error("Zero at main diagonal");
        }
        MatrixRectangle<T> result(this->Rows, this->Columns);
        for (size_t i = 0; i < this->Rows; ++i) {
            result.Array[i][0] = 1;
            size_t k = 0; // Для нумерации по строке изначальной матрицы
            for (size_t j = 1; j < this->Columns - 1; ++j, ++k) {
                if (i == k) {
                    ++k; // Диагональные элементы равны 0, поэтому мы их пропускаем
                    if (k == this->Columns - 1) break;
                }
                result.Array[i][j] = -this->Array[i][k] / this->Array[i][i];
            }
            result.Array[i][this->Columns - 1] = this->Array[i][this->Columns - 1] / this->Array[i][i];
        }
        return result;
    }


}
#endif //CPP_OOP_MATRIXALGEBRAIC_H

