#include <gtest/gtest.h>
#include <OOP Examples/Vector.h>

using namespace cw;

TEST(Vector, copy) {
    Vector temp(2, 1);
    Vector temp2(temp);
    ASSERT_EQ(temp2, temp);
    ASSERT_EQ(Vector(2, 1) = temp, temp);
    ASSERT_EQ(Vector(2, 1) = Vector(10), Vector(10));
}

TEST(Vector, eq_nonEq) {
    Vector cv(2, 1);
    Vector cvw(2);
    ASSERT_TRUE(cvw == Vector(45));
    ASSERT_FALSE(cvw == Vector(45, 0));
    ASSERT_FALSE(Vector(3, 0) == Vector(4, 0));

    ASSERT_TRUE(cvw != Vector(45, 0));
    ASSERT_FALSE(cvw != Vector(45));
    ASSERT_TRUE(Vector(3, 0) != Vector(4, 0));

    ASSERT_TRUE(cv == cv);
    ASSERT_FALSE(cv != cv);
    ASSERT_TRUE(cv != Vector());
    ASSERT_FALSE(cv == Vector());
    ASSERT_TRUE(Vector(10) == Vector(20));
    ASSERT_FALSE(Vector(10) != Vector(3));

}

TEST(Vector, getByIndex) {
    Vector temp(3, 2);
    int tempi = 2;
    ASSERT_EQ(temp[0], tempi);
    temp[0] += 1;
    ++tempi;
    ASSERT_EQ(temp[0], tempi);
    ASSERT_EQ(Vector(10)[2], 0);
}
