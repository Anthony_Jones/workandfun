/*
* Copyright © 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once
#ifndef FIND_REAL_ROOTS_CPP
#define FIND_REAL_ROOTS_CPP

#include "OOP Examples/Polynomial.h"
#include "OOP Examples/VectorT.h"
#include <vector>
/**
 * @author Anton Ivaniv
 * @date February 2019
 *
 * By using this functions you can find ONLY Real Roots of Polynomials of degree(N) including multiple roots
 * Prefer use ChordMethod due to fact if Segment what contain only one root dont really small CombinationMethod
 * can find root from another Segment.
 *
 * Param step -  Step with which we move on segments.
 *
 * Roots come back in this form: [root1][The multiplicity of the root1][root2][The multiplicity of the root2] etc.
*/
////////////////////////////////////////////////////////////////////////////////////////////////////////<
namespace cw {


    /**
     * @warning THIS METHOD DOESN'T WORK RIGHT
     * @tparam T
     * @param polynomial
     * @param precision
     * @param step
     * @return
     */
    template<class T>
    const vector <T>
    FindRootsByCombinationMethod(const Polynomial <T> &polynomial, const T &precision, const T &step = 0.05) {
        vector <T> roots;
        VectorT<T> segment(polynomial.WhereRoots());
        VectorT<Polynomial<T>> derivative(polynomial.degree());
        for (size_t i = 1; i <= derivative.size(); ++i) {
            derivative[i - 1] = polynomial.GetOrdinalDerivative(static_cast<long long int>(i));
        }
        size_t countingRoots = 0;
        size_t multiplicityCounting = 0;
        if (countingRoots == polynomial.degree()) // Случай когда все корни на концах промежутка
        {
            return roots;
        } else {
            T leftEdg = segment[1] - step; // Смотрим правый отрезок на наличие корней
            T rightEdge = segment[1];
            while (leftEdg > segment[0]) {
                while (polynomial.AtPoint(leftEdg) * polynomial.AtPoint(rightEdge) > 0) {
                    leftEdg -= step;
                }
                T root = floor(CombinationMethod(leftEdg, rightEdge, polynomial, precision));
                bool flag = false;
                for (auto it : roots) {
                    if (it == root) flag = true;
                }
                if (!flag) {
                    roots.push_back(root);
                    ++countingRoots;
                    size_t i = 1;
                    bool TempFlagForCheckingMultiplity = true;
                    while (i <= polynomial.degree() - 1) { // Проверяем на кратность
                        if (fabs(derivative[i].AtPoint(root)) <
                                precision) {
                            ++countingRoots;
                            ++multiplicityCounting;
                        } else {
                            TempFlagForCheckingMultiplity = false;
                        }
                        if (countingRoots == polynomial.degree()) {
                            roots.push_back(T(multiplicityCounting));
                            return roots;
                        }
                        ++i;
                        if (!TempFlagForCheckingMultiplity) {
                            break;
                        }
                    }
                    roots.push_back(T(multiplicityCounting));
                }
                multiplicityCounting = 0;
                rightEdge = leftEdg - step;
            }
            multiplicityCounting = 0;
            leftEdg = -segment[0] - step; // Смотрим левый промежуток на наличие корней
            rightEdge = -segment[0];
            while (leftEdg < -segment[0]) {
                while (polynomial.AtPoint(leftEdg) * polynomial.AtPoint(rightEdge) > 0) {
                    leftEdg -= step;
                }
                T root = round(CombinationMethod(leftEdg, rightEdge, polynomial, precision));
                bool flag = false;
                for (auto it : roots) {
                    if (it == root) flag = true;
                }
                if (!flag) {
                    roots.push_back(root);
                    ++countingRoots;
                    size_t i = 1;
                    bool TempFlagForCheckingMultiplity = true;
                    while (i <= polynomial.degree() - 1) {
                        if (fabs(derivative[i].AtPoint(root)) <
                                precision) {
                            roots.push_back(root);
                            ++countingRoots;
                            ++multiplicityCounting;
                        } else {
                            TempFlagForCheckingMultiplity = false;
                        }
                        if (countingRoots == polynomial.degree()) {
                            roots.push_back(T(multiplicityCounting));
                            return roots;
                        }
                        ++i;
                        if (!TempFlagForCheckingMultiplity) {
                            break;
                        }
                    }
                    roots.push_back(T(multiplicityCounting));
                }
                multiplicityCounting = 0;
                rightEdge = leftEdg - step;
            }
        }
        return roots;
    }

    template<class T>
    const vector <T>
    FindRootsByChordMethod(const Polynomial <T> &polynomial, const T &precision, const T &step = 0.05) {
        vector <T> roots;
        VectorT<T> segment(polynomial.WhereRoots());
        segment[0] -= 0.25;
        segment[1] += 0.25;
        VectorT<Polynomial<T>> derivative(polynomial.degree());
        for (size_t i = 1; i <= derivative.size(); ++i) {
            derivative[i - 1] = polynomial.GetOrdinalDerivative(static_cast<long long int>(i));
        }
        size_t countingRoots = 0;
        size_t multiplicityCounting = 0;
        if (countingRoots == polynomial.degree()) // Случай когда все корни на концах промежутка
        {
            return roots;
        } else {
            T leftEdg = segment[1] - step; // Смотрим правый отрезок на наличие корней
            T rightEdge = segment[1];
            while (leftEdg > segment[0]) {
                while (polynomial.AtPoint(leftEdg) * polynomial.AtPoint(rightEdge) > 0) {
                    leftEdg -= step;
//                    if (leftEdg < segment[0]) break;
                }
//                if (leftEdg < segment[0]) break;
                T root = ChordMethod(leftEdg, rightEdge, polynomial, precision);
                bool flag = false;
                for (auto it : roots) {
                    if (fabs(it - root) < precision) flag = true;
                }
                if (flag) {
                    root = round(root);
                    for (auto it : roots) {
                        if (fabs(it - root) < precision) flag = true;
                    }
                }
                if (!flag) {
                    roots.push_back(root);
                    ++countingRoots;
                    bool TempFlagForCheckingMultiplity = true;
                    size_t i = 1;
                    while (i <= polynomial.degree() - 1) { // Проверяем на кратность
                        T derivativeAtRoot = fabs(
                                derivative[i - 1].AtPoint(root));
                        bool multi = derivativeAtRoot < 1e-30;
                        if (multi) {
                            ++countingRoots;
                            ++multiplicityCounting;
                        } else {
                            TempFlagForCheckingMultiplity = false;
                        }
                        if (countingRoots == polynomial.degree()) {
                            roots.push_back(T(multiplicityCounting));
                            return roots;
                        }
                        ++i;
                        if (!TempFlagForCheckingMultiplity) {
                            break;
                        }
                    }
                    roots.push_back(T(multiplicityCounting));
                }
                multiplicityCounting = 0;
                rightEdge = leftEdg - step;
            }
            multiplicityCounting = 0;
            leftEdg = -segment[0] - step; // Смотрим левый промежуток на наличие корней
            rightEdge = -segment[0];
            while (leftEdg > -segment[0]) {
                while (polynomial.AtPoint(leftEdg) * polynomial.AtPoint(rightEdge) > 0) {
                    leftEdg -= step;
//                    if(leftEdg < -segment[0]) break;
                }
//                if (leftEdg < -segment[0]) break;
                T root = ChordMethod(leftEdg, rightEdge, polynomial, precision);
                bool flag = false;
                for (auto it : roots) {
                    if (fabs(it - root) < precision) flag = true;
                }
                if (flag) {
                    root = round(root);
                    for (auto it : roots) {
                        if (fabs(it - root) < precision) flag = true;
                    }
                }
                if (!flag) {
                    roots.push_back(root);
                    ++countingRoots;
                    size_t i = 1;
                    bool TempFlagForCheckingMultiplity = true;
                    while (i <= polynomial.degree() - 1) {
                        if (fabs(derivative[i - 1].AtPoint(root)) <
                                precision) {
                            roots.push_back(root);
                            ++countingRoots;
                            ++multiplicityCounting;
                        } else {
                            TempFlagForCheckingMultiplity = false;
                        }
                        if (countingRoots == polynomial.degree()) {
                            roots.push_back(T(multiplicityCounting));
                            return roots;
                        }
                        ++i;
                        if (!TempFlagForCheckingMultiplity) {
                            break;
                        }
                    }
                    roots.push_back(T(multiplicityCounting));
                }
                multiplicityCounting = 0;
                rightEdge = leftEdg - step;
            }
        }
        if (roots.empty()) {
            throw std::domain_error("There is no Real Roots");
        }
        return roots;
    }

    template<class T>
    const T CombinationMethod(const T &a, const T &b, const Polynomial <T> &polynomial, const T &eps) {
        if (polynomial.AtPoint(a) * polynomial.AtPoint(b) > 0) {
            throw std::domain_error("There is no root");
        } else {
            T x0 = a;
            T _x0 = b;
            if (polynomial.GetOrdinalDerivative(2).AtPoint(a) * polynomial.AtPoint(b) > 0) {
                while (fabs(_x0 - x0) > eps) {
                    _x0 = _x0 + (x0 - _x0) / (1 - polynomial.AtPoint(x0) / polynomial.AtPoint(_x0));
                    x0 = x0 - polynomial.AtPoint(x0) / polynomial.GetOrdinalDerivative(1).AtPoint(x0);
                }
                return (x0 + _x0) / 2.0;
            } else if (polynomial.GetOrdinalDerivative(2).AtPoint(b) * polynomial.AtPoint(a) > 0) {
                while (fabs(_x0 - x0) > eps) {
                    x0 = x0 + (_x0 - x0) / (1 - polynomial.AtPoint(_x0) / polynomial.AtPoint(x0));
                    _x0 = _x0 - polynomial.AtPoint(_x0) / polynomial.GetOrdinalDerivative(1).AtPoint(_x0);
                }
                return (x0 + _x0) / 2.0;
            } else {
                throw std::domain_error("polynomial.Oftion is not monotonous on this segment");
            }
        }
    }

    template<class T>
    const T ChordMethod(const T &a, const T &b, const Polynomial <T> &polynomial, const T &eps) {
        if (polynomial.AtPoint(a) * polynomial.AtPoint(b) > 0) {
            throw std::domain_error("There is not root");
        } else {
            T tempA = a, tempB = b;
            while (fabs(tempB - tempA) > eps) {
                tempA = tempB - (tempB - tempA) * polynomial.AtPoint(tempB) /
                                        (polynomial.AtPoint(tempB) - polynomial.AtPoint(tempA));
                if (fabs(tempB - tempA) < eps) break;
                tempB = tempA + (tempA - tempB) * polynomial.AtPoint(tempA) /
                                        (polynomial.AtPoint(tempA) - polynomial.AtPoint(tempB));
            }
            return tempB;
        }
    }
}
#endif //FIND_REAL_ROOTS_CPP