/*
* Copyright © 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once
#ifndef CPP_OOP_1_VECTOR_CPP
#define CPP_OOP_1_VECTOR_CPP

#include <OOP Examples/Vector.h>
#include <iostream>
#include <cstring>
#include <cstdarg>
#include <regex>
#include <cmath>

namespace cw {
    size_t Vector::countObjects;

    /*
     * Constructors
     */
    Vector::Vector() : items(nullptr), buffered_size(0), size(0) {
        ++Vector::countObjects;
    }

    Vector::Vector(const Vector &vector) : buffered_size(vector.buffered_size), size(vector.size) {
        items = new double[vector.buffered_size];
        for (size_t i = 0; i < vector.size; ++i) items[i] = vector.items[i];
        ++Vector::countObjects;
    }


    Vector::Vector(size_t buffered_size) : items(new double[buffered_size]), buffered_size(buffered_size), size(0) {
        ++Vector::countObjects;
    }

    Vector::Vector(size_t size, double defaultValue) : buffered_size(size), size(size) {
        items = new double[size];
        for (size_t i = 0; i < size; ++i) {
            items[i] = defaultValue;
        }
        ++Vector::countObjects;
    }

    Vector::Vector(const double *array, size_t size_array) {
        buffered_size = size_array;
        size = size_array;
        items = new double[buffered_size];
        for (size_t i = 0; i < size_array; i++) {
            items[i] = array[i];
        }
        ++Vector::countObjects;
    }

    Vector::~Vector() {
        delete[] items;
        --Vector::countObjects;

    }

    /*
     * binary operators
     */
    const Vector Vector::operator+(const Vector &v) const {

        if (size != v.size) {
            size_t smax = (size > v.size) ? size : v.size;
            size_t smin = (size > v.size) ? v.size : size;
            Vector vector(smax);
            for (size_t i = 0; i < smin; ++i) {
                vector.items[i] = items[i] + v.items[i];
            }
            if (size > v.size) {
                for (size_t i = smin; i < smax; ++i) {
                    vector.items[i] = items[i];
                }
            } else {
                for (size_t i = smin; i < smax; ++i) {
                    vector.items[i] = v.items[i];
                }
            }
            vector.size = smax;
            return vector;
        } else {
            if (v.size == 0) return *this;
            Vector vector(size);
            for (size_t i = 0; i < size; ++i) {
                vector.items[i] = items[i] + v.items[i];
            }
            vector.size = size;

            return vector;
        }
    }

    const Vector Vector::operator-(const Vector &v) const {
        return Vector(*this + (-v));
    }

    double Vector::operator*(const Vector &v) const {
        double res = 0;
        size_t smin = (size > v.size) ? v.size : size;
        for (size_t i = 0; i < smin; ++i) {
            res += items[i] * v.items[i];
        }
        return res;
    }

    const Vector Vector::operator/(double n) const {
        return *this * (1 / n);
    }

    bool Vector::operator==(const Vector &rhs) const {
        if (size != rhs.size) return false;
        else {
            for (size_t i = 0; i < size; ++i) {
                if (items[i] != rhs.items[i]) return false;
            }
        }
        return true;
    }

    bool Vector::operator!=(const Vector &rhs) const {
        return !(rhs == *this);
    }

    const Vector Vector::operator*(double n) const {
        Vector vector(*this);
        for (size_t i = 0; i < vector.size; ++i) {
            vector.items[i] *= n;
        }
        return vector;
    }

    Vector &Vector::operator=(const Vector &vector) {
        buffered_size = vector.buffered_size;
        size = vector.size;
        if (items == nullptr) {
            items = new double[buffered_size];
        } else {
            delete[] items;
        }
        for (size_t i = 0; i < size; ++i) {
            items[i] = vector.items[i];
        }
        return *this;
    }

    const Vector &Vector::operator+=(const Vector &v) {
        if (v.size == 0) return *this;
        size_t smax = (size > v.size) ? size : v.size;
        size_t sbmax = (buffered_size > v.buffered_size) ? buffered_size : v.buffered_size;///max(buffer size)
        size_t smin = (size > v.size) ? v.size : size;
        if (buffered_size < smax || items == nullptr) {
            items = new double[sbmax];
        }
        for (size_t i = 0; i < smin - 1; ++i) {
            items[i] += v.items[i];
        }
        if (size < v.size) {
            for (size_t i = smin; i < smax; ++i) {
                items[i] = v.items[i];
            }
        }
        return *this;
    }

    const Vector &Vector::operator-=(const Vector &v) {
        return *this += -v;
    }

    const Vector &Vector::operator*=(double z) {
        for (size_t i = 0; i < size; ++i) {
            items[i] *= z;
        }
        return *this;
    }

    const Vector &Vector::operator/=(double z) {
        return *this *= 1 / z;
    }

    double &Vector::operator[](size_t index) {
        return this->items[index];
    }

    /*
     * Static binary operators
     */
    std::ostream &operator<<(std::ostream &os, const Vector &vector) {
        size_t sz = vector.size;
        os << "\n{\n\t\"Size\": " << sz << "\n\t\"items\": [";
        for (size_t i = 0; i < sz; ++i) {
            os << "\n\t\t" << vector.items[i];
            if (i != sz - 1)os << ',';
        }
        os << "\n\t]\n}\n";
        return os;
    }

    std::istream &operator>>(std::istream &is, Vector &v) {
        std::string s;
        getline(is, s);
        lineToVector(s, v);
        return is;
    }

    /*
     * Unary operators
     */
    Vector::operator double *() const {
        auto *vector = new double[size];
        for (size_t i = 0; i < size; ++i) {
            vector[i] = items[i];
        }
        return vector;
    }

    Vector &Vector::operator++() {
        auto *temp = new double[size + 1];

        size_t size1 = size / 2;
        for (size_t i = 0; i < size1; ++i) {
            temp[i] = items[i];
            temp[i + size1] = items[i + size1];
        }
        delete[] items;
//        items = new double[Size+1];
        size += 1;
        items = temp;
        return *this;
    }


    const Vector Vector::operator++(int) {
        const Vector &vect = Vector(*this);
        auto *temp = new double[size + 1];

        size_t size1 = size / 2;
        for (size_t i = 0; i < size1; ++i) {
            temp[i] = items[i];
            temp[i + size1] = items[i + size1];
        }
        delete[] items;
        size += 1;
        items = temp;

        return vect;
    }

    Vector Vector::operator-() const {
        const Vector vector(*this);
        for (size_t i = 0; i < size; ++i) {
            vector.items[i] = -items[i];
        }
        return vector;
    }

    const Vector &Vector::operator+() const {
        return *this;
    }


    const Vector Vector::seq(size_t size, ...) {
        va_list args;
        va_start(args, size);
        Vector v;
        while (size--) {
            v.add(va_arg(args, double));
        }
        va_end(args);
        return v;
    }


    const double Vector::len() const {
        double res = 0;
        for (size_t i = 0; i < size; ++i) {
            res += items[i] * items[i];
        }
        return std::sqrt(res);
    }

    const double Vector::angle(const Vector &&v) const {
        return acos((*this * v) / (len() * v.len()));
    }

    const Vector &Vector::add(double item) {
        if (items == nullptr) items = new double[BUFSIZ];
        else if (size == buffered_size) {
            delete[] items;
            items = new double[buffered_size += 128];
        }
        items[size] = item;
        size++;
        return *this;
    }

    const bool Vector::isEmpty() const {
        return size == 0;
    }


    const Vector &Vector::clear() {
        size = 0;
        return *this;
    }

    void lineToVector(std::string s, Vector &v) {
        v.size = 0;
        std::regex regexpr(R"(\d*\.?\d*)");
        std::cmatch result;
        std::regex_iterator <std::string::iterator> it(s.begin(), s.end(), regexpr);
        std::regex_iterator <std::string::iterator> end;
        for (; it != end; ++it) {
            const auto &str = it->str();
            if (!str.empty())
                v.add(std::stod(str));
        }
    }


}
#endif