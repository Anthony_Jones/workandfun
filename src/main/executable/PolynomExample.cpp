/*
* Copyright © 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#include "OOP Examples/Polynomial.h"
//#include <boost/multiprecision/cpp_dec_float.hpp>
#include "../source/Numerical methods/FindRealRoots.cpp"
//typedef boost::multiprecision::cpp_dec_float_50 cpp_dec_float_50;
using namespace cw;

int main() {
    Polynomial<double> poly(3, 'z');

    cin >> poly;
    VectorT<double> WhereRoots(poly.WhereRoots());
    cout << WhereRoots << std::endl;
//    cout << CombinationMethod(WhereRoots[0], WhereRoots[1],Polynomial(poly),1e-10) << endl;
//    auto roots =  FindRootsByCombiMethod(poly);
//    cout << poly.AtPoint(WhereRoots[1]);
//    cout << ChordMethod(0.95, 2.90, Polynomial(poly), 1e-20);
    auto roots = FindRootsByChordMethod(poly, 1e-30, 0.001);
    for (auto it: roots) {
        cout << it << endl;
    }
//    size_t i = 1;
//    while (i <= poly.degree() - 1) {
//        cout << (fabs(poly.GetOrdinalDerivative(static_cast<long long int>(i)).AtPoint(1)) < 1e-30 )<< endl;
//        ++i;
//    }
}
