/*
* Copyright © 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#include "../../source/ASD/Sorts/Sorts.cpp"
#include "../source/hptimer.h"

#include <iostream>
//#include <fstream>

int main() {
    DoubleArray A;
    HRTimer timer;
    for (size_t i = 1; i <= 23; ++i) {
        A.RandArray(i * 1000);
        timer.StartTimer();
        BubbleSort(A, true);
        auto time = timer.StopTimer();
        cout << time << endl;
    }


}
