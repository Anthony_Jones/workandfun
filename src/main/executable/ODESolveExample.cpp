/*
* Copyright © 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#include <cmath>
#include <iostream>

#include "../source/Numerical methods/EulerMethod.cpp"

using std::cout;

typedef double type;

/**
 *
 * @param x Value of X at ordinat
 * @param y Value of Y at absciss
 * @return Value at(x,y)
 */
type ODE(const type &x, const type &y) {
    return x - sin(y / type(3));
}

int main() {
    type step = 0.25; // Step
    type a = 1.6; // Begin of Segment
    type b = 2.6; // End of Segment

    type Cauchy = 4.6; // Cauchy Problem

    type eps = 1e-7; // Precision

    type result1 = 0, // temporary variables for double computation
            result2 = 1;
    while (fabs(result1 - result2) >= eps) {
        result1 = EulerMethod(ODE, Cauchy, a, b, step);
        step /= 2.;
        result2 = EulerMethod(ODE, Cauchy, a, b, step);
    }
    cout << result2 << " ";


}