/*
* Copyright © 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#include <Structs/Array.hxx>

#include "../utils/ArrayUtils.cpp"
#include "../../source/ASD/Sorts/Sorts.cpp"
#include "../utils/Compare.cpp"

using std::cout, std::endl;

int Compare1(const double &one, const double &another) {
    if (fabs(one - another) < 1e-10) {
        return 0;
    } else if (one < another) {
        return -1;
    } else {
        return 1;
    }
}

using namespace cw;
using utils::Compare, utils::shuffle;

int main() {
    size_t n = 10;
    Array<double> Test(n);
    for (size_t i = 0; i < Test.size(); ++i) {
        Test[i] = i;
    }
    for (size_t j = 0; j < Test.size(); ++j) {
        cout << Test[j] << ' ';
    }
    cout << endl;
    shuffle(Test, n, cw::random::lcg::rand);
    for (size_t j = 0; j < Test.size(); ++j) {
        cout << Test[j] << ' ';
    }
    cout << endl;
    HeapSort(Test, static_cast<ptrdiff_t >(10), Compare1);
//    utils::BuildHeap(Test, 10, Compare1);
    for (size_t j = 0; j < Test.size(); ++j) {
        cout << Test[j] << ' ';
    }
    return 0;
}