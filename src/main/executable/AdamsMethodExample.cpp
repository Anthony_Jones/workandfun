/*
* Copyright © 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

#include "../source/Numerical methods/AdamsMethod.cxx"

using namespace cw;

typedef double CType;

const CType rightPart(const CType &x, const CType &y) {
    return x / CType(2) + y / CType(2) * pow(M_E, -y);
}

const CType rightPart1(const CType &x, const CType &y) {
    return cos(x);
}


CType solu1(const CType &x) {
    return sin(x) + 1;
}

const CType rightPart2(const CType &x, const CType &y) {
    return -sin(x);
}


CType solu2(const CType &x) {
    return cos(x) + 2;
}

int main() {
    CType x0 = M_PI_2, xN = 3, y0 = 2;
    cout << "Метод Адамса: " << endl;
    CType result = AdamsMethod(x0, y0, xN, rightPart1);
    CType step = (xN - x0) / static_cast<CType>(4);
    cout << result << " " << endl;
    cout << "Аналитическое решение:" << endl;
    for (size_t i = 0; i <= 4; ++i) {
        cout << solu1(x0 + i * step) << " ";
    }
    cout << endl;
    cout << "Метод Адамса: " << endl;
    result = AdamsMethod(x0, y0, xN, rightPart2);
    cout << result << " " << endl;
    cout << "Аналитическое решение:" << endl;
    for (size_t i = 0; i <= 4; ++i) {
        cout << solu2(x0 + i * step) << " ";
    }
}