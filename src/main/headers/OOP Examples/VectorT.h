/*
* Copyright © 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef CPP_OOP_1_VECTORT_H
#define CPP_OOP_1_VECTORT_H

#include <iostream>
#include <string>
#include <cstddef>

namespace cw {
/**
  * @details Class template Vector
  * @author Antony_J 
  * @throws std::domain_error 
  * @date October 2018
 */
    template<typename T>
    class VectorT {
    public:
        typedef size_t size_type;

        class iterator {
        public:
            typedef iterator self_type;
            typedef T value_type;
            typedef T &reference;
            typedef T *pointer;
            typedef std::random_access_iterator_tag iterator_category;
            typedef const size_type difference_type;

            explicit iterator(pointer ptr) : ptr_(ptr) {}

            self_type &operator++() {
                ++ptr_;
                return *this;
            }

            self_type &operator--() {
                --ptr_;
                return *this;
            }

            iterator &operator=(const self_type &other) {
                ptr_ = other.ptr;
                return *this;
            };

            const self_type operator++(int junk) {
                self_type i = *this;
                ++ptr_;
                return i;
            }

            const self_type operator--(int junk) {
                self_type i = *this;
                --ptr_;
                return i;
            }

            value_type &operator*() const { return *ptr_; }

            value_type *operator->() const { return ptr_; }

            const bool operator==(const self_type &rhs) const { return ptr_ == rhs.ptr_; }

            const bool operator!=(const self_type &rhs) const { return ptr_ != rhs.ptr_; }

            self_type operator+(const size_type &n) const { return iterator(ptr_ + n); }

            self_type operator-(const size_type &n) const { return iterator(ptr_ - n); }

            friend self_type operator+(const size_type &n, const iterator &it) { return iterator(it.ptr_ + n); }

            friend self_type operator-(const size_type &n, const iterator &it) { return iterator(it.ptr_ - n); }

            self_type &operator+=(const size_type &n) {
                ptr_ += n;
                return *this;
            }

            self_type &operator-=(const size_type &n) {
                ptr_ -= n;
                return *this;
            }

            const bool operator<(const self_type &it) const { return ptr_ < it.ptr_; }

            const bool operator>(const self_type &it) const { return ptr_ > it.ptr_; }

            const bool operator>=(const self_type &it) const { return ptr_ >= it.ptr_; }

            const bool operator<=(const self_type &it) const { return ptr_ <= it.ptr_; }

            value_type &operator[](const size_type &index) const { return *(ptr_ + index); }

        private:
            pointer ptr_;
        };

        class const_iterator {
        public:
            typedef const_iterator self_type;
            typedef const T value_type;
            typedef const T &reference;
            typedef const T *pointer;
            typedef const size_type difference_type;
            typedef std::random_access_iterator_tag iterator_category;

            explicit const_iterator(pointer ptr) : ptr_(ptr) {}

            const_iterator &operator=(const self_type &other) {
                ptr_ = other.ptr;
                return *this;
            };

            const self_type &operator++() {
                ptr_++;
                return *this;
            }

            const self_type &operator--() {
                --ptr_;
                return *this;
            }

            const self_type operator++(int junk) {
                self_type i = *this;
                ++ptr_;
                return i;
            }

            const self_type operator--(int junk) {
                self_type i = *this;
                --ptr_;
                return i;
            }

            const value_type &operator*() const { return *ptr_; }

            const value_type *operator->() const { return ptr_; }

            const bool operator==(const self_type &rhs) const { return ptr_ == rhs.ptr_; }

            const bool operator!=(const self_type &rhs) const { return ptr_ != rhs.ptr_; }

            const self_type operator+(const size_type &n) const { return iterator(ptr_ + n); }

            const self_type operator-(const size_type &n) const { return iterator(ptr_ - n); }

            friend const self_type operator+(const size_type &n, const self_type &it) { return iterator(it.ptr_ + n); }

            friend const self_type operator-(const size_type &n, const self_type &it) { return iterator(it.ptr_ - n); }

            const bool operator<(const self_type &it) const { return ptr_ < it.ptr_; }

            const bool operator>(const self_type &it) const { return ptr_ > it.ptr_; }

            const bool operator>=(const self_type &it) const { return ptr_ >= it.ptr_; }

            const bool operator<=(const self_type &it) const { return ptr_ <= it.ptr_; }

            const self_type &operator+=(const size_type &n) {
                ptr_ += n;
                return *this;
            }

            const self_type &operator-=(const size_type &n) {
                ptr_ -= n;
                return *this;
            }

            value_type &operator[](const size_type &index) const { return *(ptr_ + index); }

        private:
            pointer ptr_;
        };

        /**
         * Return vectors' size
         * @return size_t Size of vector
         */
        size_t size() const;

        static std::string ElementSeparator;
        static size_t County;

        /**
         * Default constructor
         * 'Size' = 0
         * 'Array' = nullptr
         */
        VectorT();

        /**
         * Constructor which inizializing field 'Size' by 'size1', and 'Array' by nullptr
         * @param size1 Size of vector
         */
        [[deprecated]]
        explicit VectorT(size_t size);

        /**
         * Constructor which inizializing field 'Size' by 'size1', and
         * every element of 'Array' by number
         * @param size1 Size of vector
         * @param number Real number
         *
         */
        VectorT(size_t size, T number);

        /**
         * Copy constructor
         * @param vector Vector which you wanna copy
         */
        VectorT(const VectorT<T> &vector);

        virtual ~VectorT();

        /**
         * Initializing *this by 'vector'
         * @throw std::domain_error when one of the vector is degenerated
         * @param vector Vector by which we initializing
         * @param this Vector which we want to initialize
         * @return Vector Copy of 'vector'
         */
        VectorT<T> &operator=(const VectorT<T> &vector);

        const bool operator==(const VectorT<T> &vector) const;

        const bool operator!=(const VectorT<T> &vector) const;

        T &operator[](long long index);

        iterator begin();

        iterator end();

        const_iterator cbegin() const;

        const_iterator cend() const;

        T &back() const {
            return Array[Size - 1];
        }

        template<class V>
        friend std::ostream &operator<<(std::ostream &os, const VectorT<V> &vectorT);

        template<class V>
        friend std::istream &operator>>(std::istream &is, VectorT<V> &vectorT);

        void *operator new[](size_t lenght);

        void operator delete[](void *p);

    protected:

        size_t Size;//< field for containing vectors' dimensions quantity
        T *Array;
        bool Initialized = false; //< filed to watch is this the zero-vector
    };

}

#endif //CPP_OOP_1_VECTORT_H

#include "../../source/OPP Examples/VectorT.cpp"
