/*
* Copyright © 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#include <iostream>

#include <Structs/ArrayList.h>

using std::cout;

using namespace cw;

int main() {

    ArrayList<double> temp;
    for (size_t i = 1; i <= 10; ++i) {
        temp.add(i);
    }

    for (size_t j = 1; j <= temp.size(); ++j) {
        cout << temp[j] << " ";
    }
    cout << std::endl;
    temp.remove(1);
    for (size_t j = 1; j <= temp.size(); ++j) {
        cout << temp[j] << " ";
    }
    cout << std::endl;
    temp.remove(8);
    for (size_t j = 1; j <= temp.size(); ++j) {
        cout << temp[j] << " ";
    }
    cout << std::endl;
//    temp.remove(2);
    for (size_t j = 1; j <= temp.size(); ++j) {
        cout << temp[j] << " ";
    }
    cout << std::endl;
    temp.insert(1, 10);
    for (size_t j = 1; j <= temp.size(); ++j) {
        cout << temp[j] << " ";
    }
    cout << std::endl;
    temp.insert(5, 11);
    temp.insert(1, 99);
    for (size_t j = 1; j <= temp.size(); ++j) {
        cout << temp[j] << " ";
    }
    cout << std::endl;
    temp.insert(1, 12);
    for (size_t j = 1; j <= temp.size(); ++j) {
        cout << temp[j] << " ";
    }
    cout << std::endl;
    temp.clear();
    for (size_t j = 1; j <= temp.size(); ++j) {
        cout << temp[j] << " ";
    }
    return 0;
}