/*
* Copyright © 2019 Anton Ivaniv. All rights reserved.
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef LINEAR_ALGEBRA_CPP
#define LINEAR_ALGEBRA_CPP

#include <Structs/MatrixSquare.h>
#include <Structs/MatrixRectangle.h>
#include <OOP Examples/VectorAlgebraic.h>
#include <OOP Examples/Polynomial.h>
#include "FindRealRoots.cpp"

#include <vector>

namespace cw {

    template<class T>
    const VectorAlgebraic<T> SeidelsMethod(const MatrixRectangle<T> &SLAE, const T &EPSILON) {
        MatrixSquare<T> temp(SLAE.getRows(), (SLAE.getColumns() - 1));
        for (size_t i = 0; i < temp.getRows(); ++i) {
            for (size_t j = 0; j < temp.getColumns() - 1; ++j) {
                temp(i, j) = SLAE(i, j); //Формируем матрицу коэффициентов без столбца свободных членов
            }
        }
        if (fabs(temp.determinant()) < EPSILON) { //Система уравнений не имеет решений
            throw std::domain_error("The system is not compatible");
        }
        MatrixRectangle<T> result(SLAE.getRows(), SLAE.getColumns());
        try {
            result = SLAE.toIterationForm();
        } catch (std::domain_error &e) {
            throw std::domain_error("Cannot be converted to an iterative type: zero at main diagonal.");
        }
        cout << "Итерационный вид: " << endl << result << endl;
        T norm = 0;
        for (size_t l = 0; l < result.getRows(); ++l) {
            T s = 0;
            for (size_t i = 1; i < result.getColumns() - 1; ++i) {
                if (i == l) {
                    continue;
                } else {
                    s += fabs(result(l, i));
                }
                if (s > norm) {
                    norm = s;
                }
            }
        }
        if (norm > 1) {
            throw std::domain_error("Matrix norm greater than 1.The iteration process may not converge.");
        }
        VectorAlgebraic<T> roots(result.getRows());
        cout << "Итерация номер 0:" << endl;
        for (size_t k = 0; k < result.getRows(); ++k) {
            roots[k] = result(k, result.getColumns() - 1);  // Начальное приближение(Столбец свободных членов)
        }
        cout << roots;
        bool flag = true;
        size_t u = 0;
        VectorAlgebraic<double> prevRoot;
        while (flag) {
            ++u;
            flag = false;
            prevRoot = roots; // Сохраняем прошлые значения решения системы для контроля сходимости
            for (size_t k = 0; k < roots.size(); ++k) {
                roots[k] = result(k, result.getColumns() - 1); // Свободные члены
                for (size_t i = 1, j = 0; i < result.getColumns() - 1; ++i, ++j) {
                    if (k == j) {
                        ++j;
                        if (j == result.getColumns() - 1) break;
                    }
                    roots[k] += result(k, i) * roots[j]; // Вычисление решений
                }
                if (fabs(roots[k] - prevRoot[k]) > EPSILON) { // Контролируем сходимость
                    flag = true;
                }
            }
            cout << endl << "Итерация номер " << u << ":" << endl << roots;
        }
        return roots;
    }

    template<typename T>
    const VectorAlgebraic<T> IterationRoots(const MatrixRectangle<T> &SLAE, const long double EPSILON) {
        MatrixSquare<T> temp(SLAE.getRows(), (SLAE.getColumns() - 1));
        for (size_t i = 0; i < temp.getRows(); ++i) {
            for (size_t j = 0; j < temp.getColumns() - 1; ++j) {
                temp(i, j) = SLAE(i, j);
            }
        }

        if (temp.determinant() == T(0)) {
            throw std::domain_error("IterationsRoots : Система не совместна");
        }
        MatrixRectangle<T> result(SLAE.getRows(), SLAE.getColumns());
        try {
            result = SLAE.toIterationForm();
        } catch (std::domain_error &e) {
            throw e;
        }

        VectorAlgebraic<T> roots(result.getRows());
        for (size_t k = 0; k < result.getRows(); ++k) {
            roots[k] = result(k, result.getColumns() - 1);
        }
        bool flag = true;
        while (flag) {
            flag = false;
            VectorAlgebraic<double> prevRoot(roots);
            for (size_t k = 0; k < roots.size(); ++k) {
                roots[k] = result(k, result.getColumns() - 1);
                for (size_t i = 1, j = 0; i < result.getColumns() - 1; ++i, ++j) {
                    if (k == j) {
                        ++j;
                        if (j == result.getColumns() - 1) break;
                    }
                    roots[k] += result(k, i) * prevRoot[j];
                }
                if (fabs(roots[k] - prevRoot[k]) > EPSILON) {
                    flag = true;
                }
            }
        }
        return roots;
    }

//template<typename T>
//const VectorAlgebraic<T> Cramer(const MatrixRectangle<T>& SLAE) {
//    MatrixSquare<T> main = SLAE.getMinor(-1, static_cast<long long>(SLAE.getColumns() - 1)); // Главный минор
//    VectorAlgebraic<T> result(SLAE.getRows());
//    auto it = result.begin() + 1;
//    auto itE = result.end();
//    T mainDeterminant = main.determinant();
//    if(mainDeterminant == T()) {
//        throw std::domain_error("Determinant is Zero. System has no solutions");
//    }
//    MatrixSquare<T> temp;
//    for (size_t i = 0; i < SLAE.getColumns() - 1; ++i) {
//        temp = SLAE.swapLine(i, SLAE.getColumns() - 1);
//        temp.getMinor(-1, static_cast<long long>(SLAE.getColumns() - 1));
//        result[i] = temp.determinant();
//    }
//    return result / mainDeterminant;
//}

    template<typename T>
    const std::vector<T>
    EigenValues(const MatrixSquare<T> &matrix, VectorAlgebraic<VectorAlgebraic<T>> &vectorsC, const T &epsilon) {
        size_t Rows = matrix.getRows();
        size_t Columns = matrix.getColumns();
//        VectorAlgebraic<VectorAlgebraic<T>> vectorsC(Rows + 1); //> Generating a random system of vectors/
        if (vectorsC[0].size() != Rows) {
            throw std::domain_error("C0 must be compatible with given Matrix");
        }

        MatrixRectangle<T> SLAE(Rows, Columns + 1); // Система уравнений  p1*c2 + p2 *c1 + p3 *c0 = c3
        for (int l = 0; l < static_cast<int>(Rows); ++l) {
            for (int i = static_cast<int>(SLAE.getColumns() - 1) - 1; i >= 0; --i) {
                SLAE(l, SLAE.getColumns() - i - 2) = vectorsC[i][l];
            }
            SLAE(l, SLAE.getColumns() - 1) = vectorsC[vectorsC.size() - 1][l];
        }
//        std::cout << SLAE << std::endl;
        vector<T> coefficiantsOfCharactericticPolynomial;

        gauss(SLAE, coefficiantsOfCharactericticPolynomial, epsilon);

        VectorAlgebraic<T> temp(coefficiantsOfCharactericticPolynomial.size() + 1);
        temp[0] = 1;
        for (size_t j = 1; j < temp.size(); ++j) {
            temp[j] = pow(-1, matrix.getRows()) * coefficiantsOfCharactericticPolynomial[j - 1];
        }
//        cout << temp << endl;

        Polynomial<T> charactericticPolynomial(temp);

//        cout << charactericticPolynomial << endl;

        vector<T> EigenValues = FindRootsByChordMethod(charactericticPolynomial, 1e-10);

        return EigenValues;
    }

    template<class T>
    const VectorAlgebraic<VectorAlgebraic<T>>
    EigenVectors(const MatrixSquare<T> &matrix, VectorAlgebraic<VectorAlgebraic<T>> &vectorsC, const T &epsilon) {
        size_t Rows = matrix.getRows();
        size_t Columns = matrix.getColumns();
        MatrixRectangle<T> SLAE;
        if (vectorsC.size() == Rows + 1) {
            SLAE = MatrixRectangle<T>(Rows, Columns + 1); // Система уравнений  p1*c2 + p2 *c1 + p3 *c0 = c3
            for (int l = 0; l < static_cast<int>(Rows); ++l) {
                for (int i = static_cast<int>(SLAE.getColumns() - 1) - 1; i >= 0; --i) {
                    SLAE(l, SLAE.getColumns() - i - 2) = vectorsC[i][l];
                }
                SLAE(l, SLAE.getColumns() - 1) = vectorsC[vectorsC.size() - 1][l];
            }
        } else { // Если переданная система векторов имеет m - 1
            SLAE = MatrixRectangle<T>(Rows - 1, Columns);
            for (int l = 0; l < static_cast<int>(SLAE.getRows()); ++l) {
                for (int i = static_cast<int>(SLAE.getColumns() - 1) - 1; i >= 0; --i) {
                    SLAE(l, SLAE.getColumns() - i - 2) = vectorsC[i][l];
                }
                SLAE(l, SLAE.getColumns() - 1) = vectorsC[vectorsC.size() - 1][l];
            }
        }
        cout << "Система уравненией: " << endl;
        std::cout << SLAE << std::endl;

        vector<T> coefficiantsOfCharactericticPolynomial;

        gauss(SLAE, coefficiantsOfCharactericticPolynomial, epsilon);
        cout << "Корни системы:" << endl;
        for (auto i: coefficiantsOfCharactericticPolynomial) {
            cout << i << " ";
        }
        cout << endl;
        VectorAlgebraic<T> temp(coefficiantsOfCharactericticPolynomial.size() + 1);
        temp[0] = 1;
        for (size_t j = 1; j < temp.size(); ++j) {
            temp[j] = pow(-1, matrix.getRows()) * coefficiantsOfCharactericticPolynomial[j - 1];
        }
//        cout << temp << endl;
        Polynomial<T> charactericticPolynomial(temp);

        cout << "Характерестический полином матрицы по данной системе векторов:" << endl;
        cout << charactericticPolynomial << endl;

        vector<T> EigenValues = FindRootsByChordMethod(charactericticPolynomial, 1e-10);
//        for (auto it: EigenValues) {
//            cout << it << " ";
//        }
        cout << endl;
        size_t tempSize = EigenValues.size() / 2;
        VectorAlgebraic<VectorAlgebraic<T>> eigenVectors(tempSize);

        MatrixRectangle<T> eigenVectorsCoefficiant(tempSize, tempSize);

        for (size_t i = 0; i < eigenVectorsCoefficiant.getRows(); ++i) {
            auto temp1 = static_cast<int>(2 * i);
            eigenVectorsCoefficiant(i, 0) = 1;
            for (size_t j = 1; j < eigenVectorsCoefficiant.getColumns(); ++j) {
                eigenVectorsCoefficiant(i, j) =
                        EigenValues[temp1] * eigenVectorsCoefficiant(i, j - 1) -
                        coefficiantsOfCharactericticPolynomial[j - 1];
//                cout << roots[temp] << "*" << eigenVectorsCoefficiant(i, j - 1) << " - "
//                     << coefficiantsOfCharactericticPolynomial[j - 1]
//                     << endl;
            }
        }
        cout << "Матрица коэфициентов для формирования Собственных векторов: " << endl;
        std::cout << eigenVectorsCoefficiant << std::endl;
        for (size_t k = 0; k < eigenVectors.size(); ++k) {
            eigenVectors[k] = VectorAlgebraic<T>(tempSize, 0);
            for (size_t i = 0; i < tempSize; ++i) {
                eigenVectors[k] +=
                        vectorsC[tempSize - 1 - i] * eigenVectorsCoefficiant(k, i);
//                cout << vectorsC[tempSize - 1 - i] << " *" << eigenVectorsCoefficiant(k, i) << endl;
            }
//            cout << endl;
        }
        cout << "Проверка: " << endl;
        for (size_t m = 0; m < vectorsC.size() - 1; ++m) {
            size_t temp1 = 2 * m;
            cout << matrix * eigenVectors[m] << "  ==  " << EigenValues[temp1] * eigenVectors[m] << endl;
        }
        cout << endl;
        return eigenVectors;
    }

    /**
     * Gauss method to solving System of linear equations
     * @tparam T Type Parameter
     * @param SLAE System of linear equations
     * @param roots Vector of Roots
     * @param EPS Some calculation Error
     * @return
     */
    template<typename T>
    int gauss(const MatrixRectangle<T> &SLAE, std::vector<double> &roots, const T &EPS = MatrixRectangle<T>::EPSILON) {
        MatrixRectangle<T> temporaryMatrixForSolving = SLAE;
        auto n = (int) SLAE.getRows();
        auto m = (int) SLAE.getColumns() - 1;

        std::vector<int> where(m, -1);
        for (int col = 0, row = 0; col < m && row < n; ++col) {
            int sel = row;
            for (int i = row; i < n; ++i)
                if (fabs(temporaryMatrixForSolving(i, col)) > fabs(temporaryMatrixForSolving(sel, col)))
                    sel = i;
            if (fabs(temporaryMatrixForSolving(sel, col)) < EPS)
                continue;
            T temp;
            for (int i = col; i <= m; ++i) {
                temp = temporaryMatrixForSolving(sel, i);
                temporaryMatrixForSolving(sel, i) = temporaryMatrixForSolving(row, i);
                temporaryMatrixForSolving(row, i) = temp;
//                swap (SLAE[sel][i], SLAE[row][i]);
            }
            where[col] = row;
            for (int i = 0; i < n; ++i)
                if (i != row) {
                    T c = temporaryMatrixForSolving(i, col) / temporaryMatrixForSolving(row, col);
                    for (int j = col; j <= m; ++j)
                        temporaryMatrixForSolving(i, j) -= temporaryMatrixForSolving(row, j) * c;
                }
            ++row;
        }

        roots.assign(m, 0);
        for (int i = 0; i < m; ++i)
            if (where[i] != -1)
                roots[i] = temporaryMatrixForSolving(where[i], m) / temporaryMatrixForSolving(where[i], i);
        for (int i = 0; i < n; ++i) {
            T sum = 0;
            for (int j = 0; j < m; ++j)
                sum += roots[j] * temporaryMatrixForSolving(i, j);
            if (fabs(sum - temporaryMatrixForSolving(i, m)) > EPS)
                return 0;
        }

        for (int i = 0; i < m; ++i)
            if (where[i] == -1)
                return -1;
        return 1;
    }
}

#endif //LINEAR_ALGEBRA_CPP